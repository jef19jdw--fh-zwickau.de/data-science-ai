import os.path
import gzip
import numpy as np
import matplotlib.pyplot as plt

def load(path='', subset='train', as_list=False):
    ''' Read QMNIST data.
    
    Parmeters:
    - path (string, path to directory with QMNIST data files)
    - subset ('train' or 'test', subset of data to load)
    - as_list (False: return images as list of arrays,
               True: return images as large array, first dim is image index)
               
    Return values:
    - image list or array
    - 1d array of classes
    - 1d array of series IDs
    - 1d array of writer IDs
    '''
    
    # set paths
    image_path = os.path.join(path, 'qmnist-' + subset + '-images-idx3-ubyte.gz')
    meta_path = os.path.join(path, 'qmnist-' + subset + '-labels-idx2-int.gz')    

    # read image data
    with gzip.open(image_path) as f:

        magic_number = np.frombuffer(f.read(4), dtype='>u4', count=1)[0]
        if magic_number != 2051:
            print(f'ERROR: incorrect file format (wrong magic number: {magic_number})!')
            return None

        n = np.frombuffer(f.read(4), dtype='>u4', count=1)[0]
        width = np.frombuffer(f.read(4), dtype='>u4', count=1)[0]
        height = np.frombuffer(f.read(4), dtype='>u4', count=1)[0]
        
        data = np.frombuffer(f.read(n * width * height), dtype='u1')

    # adjust shape, scaling, type
    images = (data.reshape(n, width, height) / 255).astype(np.float16)
    del data
    if as_list:
        images = [images[k, :, :] for k in range(n)]

    # read metadata
    with gzip.open(meta_path) as f:
        
        magic_number = np.frombuffer(f.read(4), dtype='>u4', count=1)[0]
        if magic_number != 3074:
            print('ERROR: incorrect file format (wrong magic number: {})!'.format(magic_number))
            return None
        
        n = np.frombuffer(f.read(4), dtype='>u4', count=1)[0]
        cols = np.frombuffer(f.read(4), dtype='>u4', count=1)[0]
        
        data = np.frombuffer(f.read(n * cols * 4), dtype='>u4')
    
    # extract relevant columns
    data = data.reshape(n, cols)
    classes = data[:, 0].astype(np.uint8)
    series = data[:, 1].astype(np.uint8)
    writers = data[:, 2].astype(np.uint16)
    del data
        
    return images, classes, series, writers


def preprocess(images, steps, as_list=False):
    ''' Apply functions to all images (does not overwrite original images).
    
    Parameters:
    - images (large array with images of list of images)
    - step (list of functions mapping an image to an image)
    - as_list (False returns images as large array, True yields list of arrays)
    
    Return values:
    - large image array or list of images (depending on as_list parameter)
    '''
    
    # array to list
    if isinstance(images, np.ndarray):
        images = [images[k, :, :] for k in range(images.shape[0])]
    
    # process images
    new_images = []
    for image in images:
        new_images.append(image.copy())
        for step in steps:
            new_images[-1] = step(new_images[-1])
            
    # make array
    if not as_list:
        height = new_images[0].shape[0]
        width = new_images[0].shape[1]
        new_array = np.empty((len(new_images), height, width), dtype=new_images[0].dtype)
        for i in range(len(new_images)):
            if new_images[i].shape != (height, width):
                print('ERROR: dimensions of preprocessed images do not match!')
                return None
            new_array[i, :, :] = new_images[i]
        new_images = new_array

    return new_images
 
