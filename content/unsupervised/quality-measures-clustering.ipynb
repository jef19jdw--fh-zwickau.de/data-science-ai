{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "8b8c6154",
   "metadata": {
    "tags": [],
    "user_expressions": []
   },
   "source": [
    "(unsupervised:quality-measures-clustering)=\n",
    "# Quality Measures for Clustering\n",
    "\n",
    "Evaluating the quality of a clustering is not as simple as it looks at first glance. Almost always we do not have a ground truth at hand (external evaluation). Instead we can only look at size and shape of clusters themselves (internal evaluation). There exist lots of internal evaluation metrics. Below we only consider two examples. There is no best metric, because cluster evaluation heavily depends on the intended application. The only reliable evaluation metric is human inspection. But human inspection is restricted to visualizations, which are not available for high dimensional data sets. Dimensionality reduction techniques may help.\n",
    "\n",
    "In the following we represent clusters as subsets of our data set. In other words, a cluster $C$ is a subset of $\\{x_1,\\ldots,x_n\\}$.\n",
    "\n",
    "## Silhouette Score\n",
    "\n",
    "The silhouette score relates intra-cluster distances to inter-cluster distances. It is defined for each sample of a data set. The silhouette score of a whole data set then is the mean score of all samples.\n",
    "\n",
    "Given some distance measure $d:X\\times X\\rightarrow[0,\\infty)$ (usually Euclidean distance) the intra-cluster distance of a sample $x$ and the cluster $C$ the sample belongs to is the mean distance of the sample to all other samples in the cluster:\n",
    "\\begin{equation*}\n",
    "\\text{intra}(x, C):=\\frac{1}{|C|-1}\\,\\sum_{\\tilde{x}\\in C}d(x,\\tilde{x}).\n",
    "\\end{equation*}\n",
    "The inter-cluster distance of a sample $x$ to a cluster $\\tilde{C}$ the sample does not belong to is the mean distance of the sample to all samples in the cluster under consideration:\n",
    "\\begin{equation*}\n",
    "\\text{inter}(x, \\tilde{C}):=\\frac{1}{|\\tilde{C}|}\\,\\sum_{\\tilde{x}\\in\\tilde{C}}d(x,\\tilde{x}).\n",
    "\\end{equation*}\n",
    "Now the silhouette score of a sample $x$ is the ratio of the intra-cluster distance and the smallest inter-cluster-distance:\n",
    "\\begin{equation*}\n",
    "a:=\\text{intra}(x,C),\\quad b:=\\min_{\\tilde{C}\\neq C}\\text{inter}(x,\\tilde{C}),\\quad\n",
    "\\text{silhouette}(x):=\\frac{b-a}{\\max\\{a,b\\}}.\n",
    "\\end{equation*}\n",
    "If $x$ is the only element in $C$, then this formula does not work and one sets the silhouette score to zero.\n",
    "\n",
    "Silhouette score always lie in $[-1,1]$. It is the higher the lower the intra-cluster distance is and the higher the inter-cluster distance is. Thus, high silhouette score for a sample indicates that it belongs to a cluster well separated from all other clusters. Score close to 0 indicates that the sample belongs to overlapping clusters. A score close to -1 indicates a missclustering (sample is closer to other clusters than to its own cluster).\n",
    "\n",
    "Silhouette score of a data set represents the average clustering quality. Many missclustered samples result in negative silhouette score and so on. Note, that a silhouette score close to zero may indicate that half the samples have been missclustered as well as that there is no clustering (all clusters heavily overlap).\n",
    "\n",
    "```{figure} silhouette.svg\n",
    "---\n",
    "alt: different silhouette scores\n",
    "---\n",
    "Silhouette scores for different clustering results.\n",
    "```\n",
    "\n",
    "```{important}\n",
    "Silhouette score depends on the chosen distance and, thus, on scaling of each feature. If some feature has much higher numerical values than other features, then that feature will dominate the distances.\n",
    "```\n",
    "\n",
    "Another noteworthy point is that silhouette scores are more reliable if clusters are convex. Else inter-cluster distances might be smaller than intra-cluster distances although clusters were correctly identified.\n",
    "\n",
    "```{figure} nonconvex.svg\n",
    "---\n",
    "alt: nonconvex clusters\n",
    "---\n",
    "For non-convex clusters silhouette score may indicate bad clustering results although clusters have been identified correctly.\n",
    "```\n",
    "\n",
    "## Davies-Bouldin Index\n",
    "\n",
    "The Davies-Bouldin index relates cluster diameters to distances between clusters. It is defined for each pair of clusters. The Davies-Bouldin index of a single cluster is the worst Davies-Bouldin index of each pair containing the cluster under consideration. The Davies-Bouldin index of a whole clustering is the mean Davies-Bouldin index of all clusters.\n",
    "\n",
    "To define the Davies-Bouldin index we need the *centroid* of a cluster $C$. It's the coordinatewise arithmetic mean of all samples in the cluster:\n",
    "\\begin{equation*}\n",
    "\\text{cent}(C):=\\frac{1}{|C|}\\,\\sum_{x\\in C}x.\n",
    "\\end{equation*}\n",
    "The cluster radius can be defined as the mean distance of samples to the cluster's centroid:\n",
    "\\begin{equation*}\n",
    "r(C):=\\frac{1}{|C|},\\sum_{x\\in C}d\\bigl(x,\\text{cent}(C)\\bigr)\n",
    "\\end{equation*}\n",
    "with some distance measure $d$. Usually $d$ is the Euclidean distance, because the notion of centroid is based on considerations involving Euclidean distances. For other distance measures introducing a sensible notion of centroids is difficult.\n",
    "Given two clusters $C_1$ and $C_2$ we may define the cluster distance as the distance of their centroids:\n",
    "\\begin{equation*}\n",
    "\\text{dist}(C_1,C_2):=d\\bigl(\\text{cent}(C_1),\\text{cent}(C_2)\\bigr).\n",
    "\\end{equation*}\n",
    "\n",
    "The Davies-Bouldin index of two clusters $C_1$ and $C_2$ is\n",
    "\\begin{equation*}\n",
    "\\text{DB}(C_1,C_2):=\\frac{r(C_1)+r(C_2)}{\\text{dist}(C_1,C_2)}.\n",
    "\\end{equation*}\n",
    "It takes values in $[0,\\infty)$ and is the closer to zero the smaller the clusters are and the higher the distance between clusters is.\n",
    "\n",
    "```{figure} db2.svg\n",
    "---\n",
    "alt: Davies-Bouldin index of two clusters\n",
    "---\n",
    "Davies-Bouldin index relates distance between clusters to cluster diameters.\n",
    "```\n",
    "\n",
    "A Davies-Bouldin index above 1 indicates overlapping clusters (at least if the clusters' shapes are close to spheres). If clusters are not sphere shaped the Davies-Boulding index does not yield useful information.\n",
    "\n",
    "```{figure} dbsphere.svg\n",
    "---\n",
    "alt: Davies-Bouldin index for clusters not sphere shaped\n",
    "---\n",
    "If clusters are not sphere shaped Davies-Bouldin index may indicate bad clustering although clustering is correct.\n",
    "```\n",
    "\n",
    "Note that the Davies-Bouldin index of two clusters is symmetric, that is, does not depend on the ordering of the clusters. If there are more than two clusters, the Davies-Bouldin index of each cluster is\n",
    "\\begin{equation*}\n",
    "\\text{DB}(C):=\\max_{\\tilde{C}\\neq C}\\text{DB}(C,\\tilde{C})\n",
    "\\end{equation*}\n",
    "and the Davies-Bouldin index of the whole clustering is mean Davies-Bouldin index of all clusters."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
