{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "7a0450db",
   "metadata": {},
   "source": [
    "(managing-data:saving-loading-data)=\n",
    "# Saving and Loading Non-Standard Data\n",
    "\n",
    "There exist Python modules for almost all standard file formats. Readers and writers for several formats also are included in larger packages like `matplotlib`, `opencv`, `pandas`.\n",
    "To share data with others always use some standard file format (PNG or JPEG for images, CSV for tabulated data, and so one).\n",
    "\n",
    "For storing temporary data like interim results NumPy and the `pickle` module from Python's standard library provide very convenient quick-and-dirty functions. Next to those functions, in this chapter we also discuss how to read custom binary file formats.\n",
    "\n",
    "Related projects:\n",
    "* [](projects:mnist)\n",
    "  * [](projects:mnist:xmnist)\n",
    "  * [](projects:mnist:load-qmnist)\n",
    "\n",
    "## Saving and Loading NumPy Arrays\n",
    "\n",
    "NumPy provides functions for saving arrays to files and for loading arrays from files."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "bbe4d336",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "431c5dbd",
   "metadata": {},
   "source": [
    "### One Array per File\n",
    "\n",
    "With [`np.save`](https://numpy.org/doc/stable/reference/generated/numpy.save.html) we can write one array to a file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "bbcfa087",
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.array([1, 2, 3])\n",
    "\n",
    "np.save('some_array.npy', a)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cca4c15d",
   "metadata": {},
   "source": [
    "The [`np.load`](https://numpy.org/doc/stable/reference/generated/numpy.load.html) functions reads an array from a file written with `np.save`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "0c882fa6",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1 2 3]\n"
     ]
    }
   ],
   "source": [
    "a = np.load('some_array.npy')\n",
    "\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3700b697",
   "metadata": {},
   "source": [
    "### Multiple Arrays\n",
    "\n",
    "To save multiple arrays to one file use [`np.savez`](https://numpy.org/doc/stable/reference/generated/numpy.savez.html) and provide each array as a keyword argument. The result is the same as calling `save` and creating an uncompressed (!) ZIP archive containing all files. File names in the ZIP archive correspond to keyword argument names."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "1fcf7572",
   "metadata": {},
   "outputs": [],
   "source": [
    "a = np.array([1, 2, 3])\n",
    "b = np.array([4, 5])\n",
    "\n",
    "np.savez('many_arrays.npz', a=a, b=b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6a8dcff1",
   "metadata": {},
   "source": [
    "Use [`np.load`](https://numpy.org/doc/stable/reference/generated/numpy.load.html) to load multiple arrays written with `savez`.\n",
    "The returned object is dict-like, that is, it behaves like a dictionary, but isn't of type `dict`. Conversion to `dict` works as expected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "7f5a1e40",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1 2 3]\n",
      "[4 5]\n"
     ]
    }
   ],
   "source": [
    "with np.load('many_arrays.npz') as data:    # data is dict-like\n",
    "    a = data['a']\n",
    "    b = data['b']\n",
    "    \n",
    "print(a)\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "96689362",
   "metadata": {},
   "source": [
    "To get a compressed ZIP archive use [`np.savez_compressed`](https://numpy.org/doc/stable/reference/generated/numpy.savez_compressed.html).\n",
    "\n",
    "## Saving and Loading Arbitrary Python Objects\n",
    "\n",
    "The `pickle` module provides functions for *pickling* (saving) and *unpickling* (loading) almost arbitrary Python objects to and from files, respectively. For details on what objects are picklable see [documentation of the `pickle` module](https://docs.python.org/3/library/pickle.html#what-can-be-pickled-and-unpickled)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "dec9f27d",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pickle"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fbfbd34f",
   "metadata": {},
   "source": [
    "There exist two interfaces: either use the functions `dump` and `load` or create a `Pickler` and an `Unpickler` object. Here we only discuss the former variant. For the latter see [`pickle` module](https://docs.python.org/3/library/pickle.html) in Python's documentation.\n",
    "\n",
    "### Pickling\n",
    "\n",
    "Steps for pickling are:\n",
    "1. Open a file for writing in binary mode.\n",
    "2. Call [`dump`](https://docs.python.org/3/library/pickle.html#pickle.dump) for each object to pickle.\n",
    "3. Close the file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "804bc4d1",
   "metadata": {},
   "outputs": [],
   "source": [
    "some_object = [1, 2, 3, 4]\n",
    "another_object = 'I\\'m a string.'\n",
    "\n",
    "with open('test.pkl', 'wb') as f:\n",
    "    pickle.dump(some_object, f)\n",
    "    pickle.dump(another_object, f)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e332efce",
   "metadata": {},
   "source": [
    "### Unpickling\n",
    "\n",
    "Steps for unpickling are:\n",
    "1. Open the file for reading in binary mode.\n",
    "2. Call [`load`](https://docs.python.org/3/library/pickle.html#pickle.load) for each object to unpickle.\n",
    "3. Close the file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "ac1fb7c2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 2, 3, 4]\n",
      "I'm a string.\n"
     ]
    }
   ],
   "source": [
    "with open('test.pkl', 'rb') as f:\n",
    "    some_object = pickle.load(f)\n",
    "    another_object = pickle.load(f)\n",
    "\n",
    "print(some_object)\n",
    "print(another_object)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1b9f54ac",
   "metadata": {},
   "source": [
    "Unpickling objects from unknown sources is a **security risk**. See [`pickle`'s documentation](https://docs.python.org/3/library/pickle.html).\n",
    "\n",
    "### (Un)Pickling many Objects\n",
    "\n",
    "If you have many objects to pickle, create a list of all objects and pickle the list.\n",
    "The advantage is, that for unpickling you do not have to remember how many objects you have pickled.\n",
    "Simply unpickle the list and look at its length.\n",
    "\n",
    "## Reading Custom Binary File Formats\n",
    "\n",
    "Sometimes data comes in custom binary formats for which no library functions exist. To read data from binary files we have to know how to interpret the data. Which bytes represent text? Which bytes represent numbers? And so on. Without format specification binary files are almost useless.\n",
    "\n",
    "### Viewing Binary Files\n",
    "\n",
    "To view binary files use a hex editor. A hex editor shows a file byte by byte, where each byte is shown as two hexadecimal digits. If you do not have a hex editor installed, try [wxHexEditor](https://www.wxhexeditor.org).\n",
    "\n",
    "```{figure} hexedit.png\n",
    "---\n",
    "alt: screenshot of a typical hex editor\n",
    "figclass: bordered\n",
    "---\n",
    "A hex editor shows file contents in hexadecimal notation and as ASCII characters (right column) together with common interpretations (lower panel).\n",
    "```\n",
    "\n",
    "Most binary files are composed of strings, bit masks, integers, floats, and padding bytes. The hex editor shows common interpretations of bytes at current cursor position.\n",
    "\n",
    "### Reading Strings\n",
    "\n",
    "We already discussed decoding binary data to strings in the chapter on [](python:accessing-data:text-files). The only question is how to find the end of a string. This question should be answered in the format specification. Usually string data is terminated by a byte with value 0.\n",
    "\n",
    "### Reading Bit Masks\n",
    "\n",
    "Bit masks are bytes in which each bit describes a truth value. To extract a bit from a byte all programming languages provide bitwise operators. Here we interpret a byte as sequence of 8 bits. Following bitwise operations can be used:\n",
    "* `a & b` returns 1 at a bit position if and only if `a` and `b` are both 1 at this position (*bitwise and*).\n",
    "* `a | b` returns 1 at a bit position if and only if at least one of `a` and `b` is 1 at this position (*bitwise or*)\n",
    "* `a ^ b` returns 1 at a bit position if and only if exactly one of `a` and `b` is 1 at this position (*bitwise exclusive or*)\n",
    "* `~a` returns 1 at a bit position if and only if `a` is 0 at this position (*bitwise not*)\n",
    "\n",
    "Python implements these bitwise operators for signed integers, which results in somewhat unexpected results (but it's the only way since Python has no unsigned integers). Thus, better use NumPy's types.\n",
    "\n",
    "To read the third bit use `& 0b00100000`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "82c2d337",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# some integer to be interpreted as bit mask (prefix 0b indicates binary notation)\n",
    "bit_mask = np.uint8(0b10111100)\n",
    "\n",
    "# get bit and convert result from int to bool\n",
    "third_bit = bool(bit_mask & np.uint8(0b00100000))\n",
    "\n",
    "third_bit"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c53a5cf9",
   "metadata": {},
   "source": [
    "To set the third bit to 1 (when writing binary files) use `| 0b00100000`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "dbcde012",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'0b10111100'"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# some integer to be interpreted as bit mask (prefix 0b indicates binary notation)\n",
    "bit_mask = np.uint8(0b10011100)\n",
    "\n",
    "# update bit mask (set third bit without modifying others)\n",
    "bit_mask = bit_mask | np.uint8(0b00100000)\n",
    "\n",
    "bin(bit_mask)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b5dfae03",
   "metadata": {},
   "source": [
    "To set the third bit to 0 (when writing binary files) use `& ~0b00100000`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "af688d2d",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'0b10011100'"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# some integer to be interpreted as bit mask (prefix 0b indicates binary notation)\n",
    "bit_mask = np.uint8(0b10111100)\n",
    "\n",
    "# update bit mask (set third bit without modifying others)\n",
    "bit_mask = bit_mask & ~np.uint8(0b00100000)\n",
    "\n",
    "bin(bit_mask)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6bc4b251",
   "metadata": {},
   "source": [
    "### Reading Integers\n",
    "\n",
    "Integer values in a binary file may have different lengths, starting from 1 byte upto 8 byte. Reading a 1-byte-integer is very simple. Just read the byte. For two-byte integers things become more involved. There is a first (closer to begin of file) and a second byte and there is no universally accepted rule for converting two bytes to an integer. Denoting the first byte by $a$ and the second by $b$ there are two possibilities:\n",
    "* $a+256\\,b\\quad$ (least significant byte first, *little endian*, Intel format)\n",
    "* $256\\,a+b\\quad$ (most significant byte first, *big endian*, Motorola format)\n",
    "\n",
    "If we have 4-byte integers, the problem persists. With bytes $a$, $b$, $c$, $d$ we have\n",
    "* $a+256\\,b+256^2\\,c+256^3\\,d\\quad$ (little endian)\n",
    "* $256^3\\,a+256^2\\,b+256\\,c+d\\quad$ (big endian)\n",
    "\n",
    "Analogously for 8-byte integers.\n",
    "\n",
    "NumPy provides the [`fromfile`](https://numpy.org/doc/stable/reference/generated/numpy.fromfile.html) function to read integers and other numeric data from binary files. Next to `offset` (starting position) and `count` (number of items to read) it has a `dtype` keyword argument. Usual Python and NumPy types are allowed, but more detailed type control is possible by providing a string consisting of:\n",
    "* `'<'` (little endian) or `'>'` (big endian) and\n",
    "* `'i'` (signed integer) or `'u'` (unsigned integer) and\n",
    "* length of item in bytes.\n",
    "\n",
    "Reading unsigned 32-bit integers in little endian notation would require `'<u4'`, for instance.\n",
    "\n",
    "If data is already in memory, use [`frombuffer`](https://numpy.org/doc/stable/reference/generated/numpy.frombuffer.html) instead of `fromfile`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "253e3ac9",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[200   3   4   5]\n",
      "[-56   3   4   5]\n",
      "[ 968 1284]\n",
      "[51203  1029]\n",
      "[3355640837]\n",
      "[-939326459]\n"
     ]
    }
   ],
   "source": [
    "data = bytes([200, 3, 4, 5])\n",
    "\n",
    "# 4 unsigned 8-bit integers\n",
    "a = np.frombuffer(data, 'u1')\n",
    "print(a)\n",
    "\n",
    "# 4 signed 8-bit integers\n",
    "a = np.frombuffer(data, 'i1')\n",
    "print(a)\n",
    "\n",
    "# 2 unsigned 16-bit integers (little endian)\n",
    "a = np.frombuffer(data, '<u2')\n",
    "print(a)\n",
    "\n",
    "# 2 unsigned 16-bit integers (big endian)\n",
    "a = np.frombuffer(data, '>u2')\n",
    "print(a)\n",
    "\n",
    "# 1 unsigned 32-bit integer (big endian)\n",
    "a = np.frombuffer(data, '>u4')\n",
    "print(a)\n",
    "\n",
    "# 1 signed 32-bit integer (big endian)\n",
    "a = np.frombuffer(data, '>i4')\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cd8192bf",
   "metadata": {},
   "source": [
    "See [Byte-swapping](https://numpy.org/doc/stable/user/byteswapping.html) for more detailes on NumPy's support of endianess."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
