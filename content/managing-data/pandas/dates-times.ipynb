{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "1afa23e7",
   "metadata": {},
   "source": [
    "(managing-data:pandas:dates-times)=\n",
    "# Dates and Times\n",
    "\n",
    "We already met the `datetime` module in [](python:accessing-data:web-access) for handling points of time and time durations. Pandas extends those capabilities by introducing time periods (durations associated with a point) and more advanced calendar arithmetics.\n",
    "\n",
    "Pandas also provides date and time related index objects to easily index time series data: `DatetimeIndex`, `TimedeltaIndex`, `PeriodIndex`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "50e9bc8b",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d09c1d7",
   "metadata": {},
   "source": [
    "## Time Stamps\n",
    "\n",
    "The basic data structure for representing points in time are [`Timestamp`](https://pandas.pydata.org/docs/reference/api/pandas.Timestamp.html) objects. They provide lots of useful methods for conversion from and to other date and time formats."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "76a0eff9",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Timestamp('2020-02-15 12:34:00')"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "some_day = pd.Timestamp(year=2020, month=2, day=15, hour=12, minute=34)\n",
    "some_day"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12b6ed28",
   "metadata": {},
   "source": [
    "## Time Deltas\n",
    "\n",
    "The basic data structure for representing durations are [`Timedelta`](https://pandas.pydata.org/docs/reference/api/pandas.Timedelta.html) objects. They can be used in their own or to shift time stamps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "129eb5d2",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "Timedelta('10000 days 01:40:00')"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a_long_time = pd.Timedelta(days=10000, minutes=100)\n",
    "a_long_time"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d79d9e28",
   "metadata": {},
   "source": [
    "## Periods\n",
    "\n",
    "A period in Pandas is a time interval paired with a time stamp. Interpretation is as follows:\n",
    "* The interval is one of several preset intervals, like a calendar month or a week from Monday till Sunday. See [Offset aliases](https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#offset-aliases) and [Anchored aliases](https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#anchored-offsets) for available intervals.\n",
    "* The time stamp selects a concrete interval, the month or the week containing the time stamp, for instance.\n",
    "\n",
    "The basic data structure for representing periods are [`Period`](https://pandas.pydata.org/docs/reference/api/pandas.Period.html) objects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "d23028fb",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2010-01-01 00:00:00\n",
      "2010-12-31 23:59:59.999999999\n"
     ]
    }
   ],
   "source": [
    "year2010 = pd.Period('1/1/2010', freq='A')\n",
    "print(year2010.start_time)\n",
    "print(year2010.end_time)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "5c620396",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2009-11-01 00:00:00\n",
      "2010-10-31 23:59:59.999999999\n"
     ]
    }
   ],
   "source": [
    "year2010oct = pd.Period('1/1/2010', freq='A-OCT')\n",
    "print(year2010oct.start_time)\n",
    "print(year2010oct.end_time)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a99927c9",
   "metadata": {},
   "source": [
    "## Time Stamp Indices\n",
    "\n",
    "Using time stamps for indexing offers lots of nice features in Pandas, because Pandas originally has been developed for handling time series.\n",
    "\n",
    "### Creating Time Stamp Indices\n",
    "\n",
    "The constructor for [`DatetimeIndex`](https://pandas.pydata.org/docs/reference/api/pandas.DatetimeIndex.html) objects takes a list of time stamps and an optional frequency. Frequency has to match the passed time stamps. If there is no common frequency in the data, the frequency is `None` (default).\n",
    "\n",
    "A more convenient method is [`pd.date_range`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.date_range.html):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "969b0a0d",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "DatetimeIndex(['2018-03-14 00:00:00', '2018-03-16 12:00:00',\n",
       "               '2018-03-19 00:00:00', '2018-03-21 12:00:00',\n",
       "               '2018-03-24 00:00:00', '2018-03-26 12:00:00',\n",
       "               '2018-03-29 00:00:00', '2018-03-31 12:00:00',\n",
       "               '2018-04-03 00:00:00', '2018-04-05 12:00:00'],\n",
       "              dtype='datetime64[ns]', freq='60H')"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "index = pd.date_range(start='2018-03-14', freq='2D12H', periods=10)\n",
    "index"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "fa99ae03",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "DatetimeIndex(['2018-03-14', '2018-03-15', '2018-03-16', '2018-03-17',\n",
       "               '2018-03-18', '2018-03-19', '2018-03-20'],\n",
       "              dtype='datetime64[ns]', freq='D')"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "index = pd.date_range(start='2018-03-14', end='2018-03-20', freq='D')\n",
    "index"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bb1e0d14",
   "metadata": {},
   "source": [
    "We may also use columns of an existing data frame to create a `DatetimeIndex`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "323e97c8",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>day</th>\n",
       "      <th>month</th>\n",
       "      <th>year</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>1</td>\n",
       "      <td>4</td>\n",
       "      <td>2018</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>2</td>\n",
       "      <td>6</td>\n",
       "      <td>2018</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>3</td>\n",
       "      <td>9</td>\n",
       "      <td>2020</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>4</td>\n",
       "      <td>9</td>\n",
       "      <td>2020</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   day  month  year\n",
       "0    1      4  2018\n",
       "1    2      6  2018\n",
       "2    3      9  2020\n",
       "3    4      9  2020"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "0   2018-04-01\n",
       "1   2018-06-02\n",
       "2   2020-09-03\n",
       "3   2020-09-04\n",
       "dtype: datetime64[ns]"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "df = pd.DataFrame({'day': [1, 2, 3, 4], 'month': [4, 6, 9, 9], 'year': [2018, 2018, 2020, 2020]})\n",
    "display(df)\n",
    "\n",
    "pd.to_datetime(df)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "696034ef",
   "metadata": {},
   "source": [
    "The [`to_datetime`](https://pandas.pydata.org/docs/reference/api/pandas.to_datetime.html) function expects that columns are named `'day'`, `'month'`, `'year'`. It returns a series of type `datetime64` which may be converted to a `DatetimeIndex`.\n",
    "\n",
    "### Indexing\n",
    "\n",
    "#### Exact Indexing\n",
    "\n",
    "Using `Timestamp` objects for label based indexing yields items with the corresponding time stamp, if there are any. Slicing works as usual."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "192ab9e0",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2018-03-14     1\n",
      "2018-03-15     2\n",
      "2018-03-16     3\n",
      "2018-03-17     4\n",
      "2018-03-18     5\n",
      "2018-03-19     6\n",
      "2018-03-20     7\n",
      "2018-03-21     8\n",
      "2018-03-22     9\n",
      "2018-03-23    10\n",
      "Freq: D, dtype: int64 \n",
      "\n",
      "3 \n",
      "\n",
      "3 \n",
      "\n",
      "2018-03-16    3\n",
      "2018-03-17    4\n",
      "2018-03-18    5\n",
      "2018-03-19    6\n",
      "2018-03-20    7\n",
      "Freq: D, dtype: int64\n"
     ]
    }
   ],
   "source": [
    "index = pd.date_range(start='2018-03-14', freq='D', periods=10)\n",
    "s = pd.Series([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], index=index)\n",
    "print(s, '\\n')\n",
    "\n",
    "print(s.loc[pd.Timestamp('2018-3-16')], '\\n')\n",
    "#print(s.loc[pd.Timestamp('2018-3-16 10:00')], '\\n')    # KeyError\n",
    "print(s.loc[pd.Timestamp('2018-3-16 00:00')], '\\n')\n",
    "print(s.loc[pd.Timestamp('2018-3-16'):pd.Timestamp('2018-3-20')])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "166a9ea7",
   "metadata": {},
   "source": [
    "#### Inexact Indexing\n",
    "\n",
    "Passing strings containing partial dates/times selects time ranges. This technique is referred to as *partial string indexing*. Slicing is allowed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "e21b518d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2018-03-14      1\n",
      "2018-03-15      2\n",
      "2018-03-16      3\n",
      "2018-03-17      4\n",
      "2018-03-18      5\n",
      "             ... \n",
      "2018-06-17     96\n",
      "2018-06-18     97\n",
      "2018-06-19     98\n",
      "2018-06-20     99\n",
      "2018-06-21    100\n",
      "Freq: D, Length: 100, dtype: int64 \n",
      "\n",
      "2018-03-14     1\n",
      "2018-03-15     2\n",
      "2018-03-16     3\n",
      "2018-03-17     4\n",
      "2018-03-18     5\n",
      "2018-03-19     6\n",
      "2018-03-20     7\n",
      "2018-03-21     8\n",
      "2018-03-22     9\n",
      "2018-03-23    10\n",
      "2018-03-24    11\n",
      "2018-03-25    12\n",
      "2018-03-26    13\n",
      "2018-03-27    14\n",
      "2018-03-28    15\n",
      "2018-03-29    16\n",
      "2018-03-30    17\n",
      "2018-03-31    18\n",
      "Freq: D, dtype: int64 \n",
      "\n",
      "2018-03-14     1\n",
      "2018-03-15     2\n",
      "2018-03-16     3\n",
      "2018-03-17     4\n",
      "2018-03-18     5\n",
      "2018-03-19     6\n",
      "2018-03-20     7\n",
      "2018-03-21     8\n",
      "2018-03-22     9\n",
      "2018-03-23    10\n",
      "2018-03-24    11\n",
      "2018-03-25    12\n",
      "2018-03-26    13\n",
      "2018-03-27    14\n",
      "2018-03-28    15\n",
      "2018-03-29    16\n",
      "2018-03-30    17\n",
      "2018-03-31    18\n",
      "2018-04-01    19\n",
      "2018-04-02    20\n",
      "2018-04-03    21\n",
      "2018-04-04    22\n",
      "2018-04-05    23\n",
      "2018-04-06    24\n",
      "2018-04-07    25\n",
      "2018-04-08    26\n",
      "2018-04-09    27\n",
      "2018-04-10    28\n",
      "2018-04-11    29\n",
      "2018-04-12    30\n",
      "2018-04-13    31\n",
      "2018-04-14    32\n",
      "2018-04-15    33\n",
      "2018-04-16    34\n",
      "2018-04-17    35\n",
      "2018-04-18    36\n",
      "2018-04-19    37\n",
      "2018-04-20    38\n",
      "2018-04-21    39\n",
      "2018-04-22    40\n",
      "2018-04-23    41\n",
      "2018-04-24    42\n",
      "2018-04-25    43\n",
      "2018-04-26    44\n",
      "2018-04-27    45\n",
      "2018-04-28    46\n",
      "2018-04-29    47\n",
      "2018-04-30    48\n",
      "Freq: D, dtype: int64\n"
     ]
    }
   ],
   "source": [
    "index = pd.date_range(start='2018-03-14', freq='D', periods=100)\n",
    "s = pd.Series(range(1, len(index) + 1), index=index)\n",
    "print(s, '\\n')\n",
    "\n",
    "print(s.loc['2018-3'], '\\n')\n",
    "print(s.loc['2018-3':'2018-4'])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ede3d45",
   "metadata": {},
   "source": [
    "Inexact indexing has some pitfalls, which are described in [Partial string indexing](https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#partial-string-indexing) and [Slice vs. exact match](https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#slice-vs-exact-match) of the Pandas user guide.\n",
    "\n",
    "### Useful Functions for Time Stamp Indexed Data\n",
    "\n",
    "Pandas provides lots of functions for working with time stamp indices. Some are:\n",
    "* [`asfreq`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.asfreq.html) (upsampling with fill values or filling logic)\n",
    "* [`shift`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.shift.html) (shift index or data by some time period)\n",
    "* [`resample`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.resample.html) (downsampling with aggregation, see below)\n",
    "\n",
    "The `resample` method returns a [`Resampler`](https://pandas.pydata.org/pandas-docs/stable/reference/resampling.html) object, which provides several methods for calculating data values at the new time stamps. Examples are `sum`, `mean`, `min`, `max`. All these methods return a series or a data frame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "bd6411fa",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2018-03-14     15\n",
       "2018-03-19     40\n",
       "2018-03-24     65\n",
       "2018-03-29     90\n",
       "2018-04-03    115\n",
       "2018-04-08    140\n",
       "2018-04-13    165\n",
       "2018-04-18    190\n",
       "2018-04-23    215\n",
       "2018-04-28    240\n",
       "2018-05-03    265\n",
       "2018-05-08    290\n",
       "2018-05-13    315\n",
       "2018-05-18    340\n",
       "2018-05-23    365\n",
       "2018-05-28    390\n",
       "2018-06-02    415\n",
       "2018-06-07    440\n",
       "2018-06-12    465\n",
       "2018-06-17    490\n",
       "Freq: 5D, dtype: int64"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "index = pd.date_range(start='2018-03-14', freq='D', periods=100)\n",
    "s = pd.Series(range(1, len(index) + 1), index=index)\n",
    "\n",
    "s2 = s.resample('5D').sum()\n",
    "s2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b6175ede",
   "metadata": {},
   "source": [
    "## Period indices\n",
    "\n",
    "Period indices work analogously to time stamp indices. Corresponding class is [`PeriodIndex`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.PeriodIndex.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "f997bc0c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "PeriodIndex(['2018-03-14', '2018-03-15', '2018-03-16', '2018-03-17',\n",
      "             '2018-03-18', '2018-03-19', '2018-03-20', '2018-03-21',\n",
      "             '2018-03-22', '2018-03-23'],\n",
      "            dtype='period[D]')\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "2018-03-14     1\n",
       "2018-03-15     2\n",
       "2018-03-16     3\n",
       "2018-03-17     4\n",
       "2018-03-18     5\n",
       "2018-03-19     6\n",
       "2018-03-20     7\n",
       "2018-03-21     8\n",
       "2018-03-22     9\n",
       "2018-03-23    10\n",
       "Freq: D, dtype: int64"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "index = pd.period_range(start='2018-03-14', freq='D', periods=10)\n",
    "print(index)\n",
    "\n",
    "s = pd.Series(range(1, len(index) + 1), index=index)\n",
    "s"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "677124b1",
   "metadata": {},
   "source": [
    "Indexing with time stamps selects the appropriate period, like with `IntervalIndex` objects:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "0a9ff0b8",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s.loc[pd.Timestamp('2018-03-15 12:34')]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ee594644",
   "metadata": {},
   "source": [
    "With time stamp index the above line would lead to a `KeyError`. But for periods it's interpreted as: select the period containing the time stamp.\n",
    "\n",
    "Similar is possible with slicing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "9d438864",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "2018-03-15    2\n",
       "2018-03-16    3\n",
       "2018-03-17    4\n",
       "2018-03-18    5\n",
       "Freq: D, dtype: int64"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s.loc[pd.Timestamp('2018-03-15 12:34'):pd.Timestamp('2018-03-18 23:45')]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "52a2028c",
   "metadata": {},
   "source": [
    "Methods `asfreq`, `shift`, `resample` also work for periods indices."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
