{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f11fd33a",
   "metadata": {},
   "source": [
    "(managing-data:pandas:categorical-data)=\n",
    "# Categorical Data\n",
    "\n",
    "Next to numerical and string data one frequently encounters *categorical data*. That is data of whatever type with finite range. Admissible values are called *categories*. There are two kinds of categorical data:\n",
    "* *nominal data* (finitely many different values without any order)\n",
    "* *ordinal data* (finitely many different values with linear order)\n",
    "\n",
    "Examples:\n",
    "* colors `red`, `blue`, `green`, `yellow` (nominal)\n",
    "* business days `Monday`, `Tuesday`, `Wednesday`, `Thursday`, `Friday` (ordnial)\n",
    "\n",
    "Pandas provides explicit support for categorical data and indices. Major advantages of categorical data compared to string data are lower memory consumption and more meaningful source code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "e96bb674",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3191b94d",
   "metadata": {},
   "source": [
    "## Creating Categorical Data\n",
    "\n",
    "Pandas has a class `Categorical` to hold a list of categorical data with (ordinal) or without (nominal) ordering. Such `Categorical` objects can directly be converted to series or columns of a data frame. Almost always category labels are strings, but any other data type is allowed, too."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "c7a98bbe",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0      red\n",
       "1    green\n",
       "2     blue\n",
       "3    green\n",
       "4    green\n",
       "dtype: category\n",
       "Categories (3, object): ['red', 'green', 'blue']"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "cat_data = pd.Categorical(['red', 'green', 'blue', 'green', 'green'],\n",
    "                          categories=['red', 'green', 'blue'], ordered=False)\n",
    "\n",
    "s = pd.Series(cat_data)\n",
    "s"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cd19719d",
   "metadata": {},
   "source": [
    "Passing `dtype='category'` to series or data frame constructors works, too. Categories then are determined automatically."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "2d34b0d4",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0      red\n",
       "1    green\n",
       "2     blue\n",
       "3    green\n",
       "4    green\n",
       "dtype: category\n",
       "Categories (3, object): ['blue', 'green', 'red']"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s = pd.Series(['red', 'green', 'blue', 'green', 'green'], dtype='category')\n",
    "s"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "56e47c7c",
   "metadata": {},
   "source": [
    "Or we may convert an existing series or data frame column to categorical type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "6e6af646",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0      red\n",
       "1    green\n",
       "2     blue\n",
       "3    green\n",
       "4    green\n",
       "dtype: category\n",
       "Categories (3, object): ['blue', 'green', 'red']"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "s = pd.Series(['red', 'green', 'blue', 'green', 'green'])\n",
    "s = s.astype('category')\n",
    "s"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27abb113",
   "metadata": {},
   "source": [
    "Automatically determined categories always are unordered (nominal).\n",
    "\n",
    "Advantage of ordered categories is that we may use `min` and `max` functions for corresponding data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "b4deb39a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "poor\n",
      "excellent\n"
     ]
    }
   ],
   "source": [
    "quality = pd.Series(pd.Categorical(['poor', 'good', 'excellent', 'good', 'very good', 'poor'],\n",
    "                                   categories=['very poor', 'poor', 'good', 'very good', 'excellent'],\n",
    "                                   ordered=True))\n",
    "\n",
    "print(quality.min())\n",
    "print(quality.max())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0cfc53bd",
   "metadata": {},
   "source": [
    "## Custom Categorical Types\n",
    "\n",
    "Instead of using general `categorical` data type we may define new categorical types. Strictly speaking `categorical` isn't a well defined type because we have to provide the category labels to obtain a full-fledged data type. A more natural way for using categories is to define a data type for each set of categories via [`CategoricalDtype`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.CategoricalDtype.html).\n",
    "\n",
    "A further advantage is that the same set of categories can be used for several series and data frames simultaneously."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "708192e4",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0     red\n",
       "1     red\n",
       "2     NaN\n",
       "3    blue\n",
       "dtype: category\n",
       "Categories (4, object): ['red', 'green', 'blue', 'yellow']"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "colors = pd.CategoricalDtype(['red', 'green', 'blue', 'yellow'], ordered=False)\n",
    "\n",
    "s = pd.Series(['red', 'red', 'black', 'blue'], dtype=colors)\n",
    "s"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a06cd471",
   "metadata": {},
   "source": [
    "Values not covered by the categorical type are set to `NaN`.\n",
    "\n",
    "## Encoding Categorical Data for Machine Learning\n",
    "\n",
    "Most machine learning algorithms expect numerical input. Thus, categorical data has to be converted to numerical data first.\n",
    "\n",
    "For ordinal data one might use numbers 1, 2, 3,... instead of the original category labels. But for nominal data the natural ordering of integers adds artificial structure to the data, which might affect an algorithm's behavior. Thus, *one hot encoding* usually is used for converting nominal data to numerical data.\n",
    "\n",
    "The idea is to replace a variable holding one of $n$ categories by $n$ boolean variables. Each new variable corresponds to one category. Exactly one variable is set to `True`. Pandas supports this conversion via [`get_dummies`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.get_dummies.html) function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "c3999a68",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0      red\n",
      "1      red\n",
      "2    green\n",
      "3     blue\n",
      "dtype: category\n",
      "Categories (4, object): ['red', 'green', 'blue', 'yellow']\n"
     ]
    },
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>red</th>\n",
       "      <th>green</th>\n",
       "      <th>blue</th>\n",
       "      <th>yellow</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>0</th>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>1</th>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>2</th>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>3</th>\n",
       "      <td>0</td>\n",
       "      <td>0</td>\n",
       "      <td>1</td>\n",
       "      <td>0</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "   red  green  blue  yellow\n",
       "0    1      0     0       0\n",
       "1    1      0     0       0\n",
       "2    0      1     0       0\n",
       "3    0      0     1       0"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "colors = pd.CategoricalDtype(['red', 'green', 'blue', 'yellow'], ordered=False)\n",
    "\n",
    "s = pd.Series(['red', 'red', 'green', 'blue'], dtype=colors)\n",
    "print(s)\n",
    "\n",
    "df = pd.get_dummies(s)\n",
    "df"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "774615fd",
   "metadata": {},
   "source": [
    "## Modifying Categories\n",
    "\n",
    "Series or data frame columns with categorical data have a `cat` member providing access to the set of categories. Some member functions are:\n",
    "* [`rename_categories`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.cat.rename_categories.html) (modify category labels),\n",
    "* [`add_categories`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.cat.add_categories.html) (add category; at the highest position, if ordinal),\n",
    "* [`remove_categories`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.Series.cat.remove_categories.html) (remove category, replacing corresponding items by `nan`),\n",
    "* [`union_categories`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.api.types.union_categoricals.html) (join sets of categories).\n",
    "\n",
    "## Categorical Data and CSV Files\n",
    "\n",
    "Information about categories cannot be stored in CSV files. Instead, category labels are written to the CSV file in their native data type. When reading CSV data to a data frame, columns have to be converted to categorical types again, if desired.\n",
    "\n",
    "## Categorical Indices\n",
    "\n",
    "Pandas supports categorical indices via `CategoricalIndex` objects. Simply pass a `Categorical` object as index when creating a series or a data frame."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "2fd31d9c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "poor          3\n",
      "good          4\n",
      "excellent     2\n",
      "good         23\n",
      "very good    41\n",
      "poor          5\n",
      "dtype: int64 \n",
      "\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "poor          3\n",
       "poor          5\n",
       "good          4\n",
       "good         23\n",
       "very good    41\n",
       "excellent     2\n",
       "dtype: int64"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "quality = pd.Categorical(['poor', 'good', 'excellent', 'good', 'very good', 'poor'],\n",
    "                         categories=['very poor', 'poor', 'good', 'very good', 'excellent'],\n",
    "                         ordered=True)\n",
    "s = pd.Series([3, 4, 2, 23, 41, 5], index=quality)\n",
    "print(s, '\\n')\n",
    "\n",
    "s = s.sort_index()\n",
    "s"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c0655df3",
   "metadata": {},
   "source": [
    "Data access works as usual."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "94558106",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "poor    3\n",
      "poor    5\n",
      "dtype: int64 \n",
      "\n",
      "poor          3\n",
      "poor          5\n",
      "good          4\n",
      "good         23\n",
      "very good    41\n",
      "dtype: int64\n"
     ]
    }
   ],
   "source": [
    "print(s.loc['poor'], '\\n')\n",
    "print(s.loc['poor':'very good'])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ee318a45",
   "metadata": {},
   "source": [
    "## Categories by Binning\n",
    "\n",
    "Continuous data or discrete data with too large range can be converted to categories by providing a list of intervals (bins) in which items shall be placed. Each bin can be regarded as a category. Binning is important for machine learning tasks which require discrete data. The [`pd.cut`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.cut.html) function implements binning."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
