{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "4731f98e",
   "metadata": {},
   "source": [
    "(managing-data:numpy:copies-views)=\n",
    "# Copies and Views\n",
    "\n",
    "NumPy arrays may be very large. Thus, having too many copies of one and the same array (or subarrays) is expensive. NumPy implements a mechanism to avoid copying arrays if not necessary by sharing data between arrays. The programmer has to take care of which arrays share data und which arrays are independent from others."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "f8499d11",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26440b15",
   "metadata": {},
   "source": [
    "## Views\n",
    "\n",
    "A *view* of a NumPy array is a usual `ndarray` object, that shares data with another array. The other array is called the *base* array of the view.\n",
    "\n",
    "Views can be created with an array's `view` method. The base object is accessible through a view's `base` member variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "6f29b5e8",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "id of a: 139825110801392\n",
      "id of b: 139825110801776\n",
      "base of a: None\n",
      "id of base of b: 139825110801392\n"
     ]
    }
   ],
   "source": [
    "a = np.ones((100, 100))\n",
    "b = a.view()\n",
    "\n",
    "print('id of a:', id(a))\n",
    "print('id of b:', id(b))\n",
    "print('base of a:', a.base)\n",
    "print('id of base of b:', id(b.base))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "65936342",
   "metadata": {},
   "source": [
    "The `view` method is rarely called directly (might be used for type conversions without copying), but views frequently originate from calling shape manipulation functions like `reshape` or `fliplr`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "86c35243",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(100, 100)\n",
      "(10, 1000) True\n",
      "(100, 100) True\n"
     ]
    }
   ],
   "source": [
    "b = a.reshape(10, 1000)\n",
    "c = np.fliplr(a)\n",
    "\n",
    "print(a.shape)\n",
    "print(b.shape, b.base is a)\n",
    "print(c.shape, c.base is a)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "03c6be83",
   "metadata": {},
   "source": [
    "Operations on views alter the base array's (and other view's) data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "0cbd77c6",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "5.0 5.0 5.0\n"
     ]
    }
   ],
   "source": [
    "b[0, 0] = 5\n",
    "\n",
    "print(a[0, 0], b[0, 0], c[0, -1])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "600943d4",
   "metadata": {},
   "source": [
    "```{important}\n",
    "Writing data to views modifies the base array! This is a common source of errors, which are very hard to track down. Always keep track of which of your arrays are views!\n",
    "```\n",
    "\n",
    "## Slicing Creates Views\n",
    "\n",
    "Views may be smaller than the original array. Such views of subarrays originate from slicing operations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "57e3f9c3",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(100, 100)\n",
      "(6, 100) True\n",
      "(100,) True\n"
     ]
    }
   ],
   "source": [
    "a = np.ones((100, 100))\n",
    "b = a[4:10, :]\n",
    "c = a[5]\n",
    "\n",
    "print(a.shape)\n",
    "print(b.shape, b.base is a)\n",
    "print(c.shape, c.base is a)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c438c3df",
   "metadata": {},
   "source": [
    "Again modifying a view alters the original array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "dfc211ee",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "5.0 5.0 5.0\n"
     ]
    }
   ],
   "source": [
    "b[1, 0] = 5\n",
    "\n",
    "print(a[5, 0], b[1, 0], c[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7de770dd",
   "metadata": {},
   "source": [
    "## Copies\n",
    "\n",
    "A NumPy array's `copy` method yields a (deep) copy of an array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "9ab7c6ff",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.0 5.0\n",
      "None\n"
     ]
    }
   ],
   "source": [
    "a = np.ones((100, 100))\n",
    "b = a.copy()\n",
    "\n",
    "b[0, 0] = 5\n",
    "\n",
    "print(a[0, 0], b[0, 0])\n",
    "print(b.base)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "241e4963",
   "metadata": {},
   "source": [
    "```{hint}\n",
    "NumPy arrays are mutable objects. Thus, assigning a new name to an array or passing an array to a function does not copy the array. Keeping this in mind is very important because functions you call in your code may alter you arrays. The other way round, writing functions other people might use, clearly indicate in the documentation if your function modifies arrays passed as parameters. If in doubt, use `copy`.\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
