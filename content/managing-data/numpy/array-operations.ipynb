{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "18a78fe6",
   "metadata": {},
   "source": [
    "(managing-data:numpy:array-operations)=\n",
    "# Array Operations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "4d25dd40",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f061fa69",
   "metadata": {},
   "source": [
    "All Python operators can be applied to NumPy arrays, where **all operations work elementwise**.\n",
    "\n",
    "## Mathematical Operations\n",
    "\n",
    "For instance, we can easily add two vectors or two matrices by using the `+` operator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "55e8b556",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[10 10 10]\n"
     ]
    }
   ],
   "source": [
    "a = np.array([1, 2, 3])\n",
    "b = np.array([9, 8, 7])\n",
    "\n",
    "c = a + b\n",
    "\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "93db1b9e",
   "metadata": {},
   "source": [
    "```{important}\n",
    "Because all operations work elementwise, the `*` operator on two-dimensional arrays does NOT multiply the two matrices in the mathematical sense, but simply yields the matrix of elementwise products. Mathematical matrix multiplication will be discussed in [](managing-data:numpy:linear-algebra-functions).\n",
    "```\n",
    "\n",
    "NumPy reimplements almost all mathematical functions, like sine, cosine and so on. NumPy's functions take arrays as arguments and apply the mathematical functions elementwise. Have a look at [Mathematical functions](https://numpy.org/doc/stable/reference/routines.math.html) in NumPy's documentation for a list of available functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "630b76f2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[0.84147098 0.90929743 0.14112001]\n"
     ]
    }
   ],
   "source": [
    "a = np.array([1, 2 , 3])\n",
    "\n",
    "b = np.sin(a)\n",
    "\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df2a9607",
   "metadata": {},
   "source": [
    "```{hint}\n",
    "Functions `min` and `amin` are equivalent. The `amin` variant exists to avoid confusion and name conflicts with Python's built-in function `min`. Writing `np.min` is okay.\n",
    "```\n",
    "\n",
    "```{important}\n",
    "Functions `np.min` and `np.minimum` do different things. With `min` we get the smallest value in an array, whereas `minimum` yields the elementwise minimum of two equally sized arrays. With `np.argmin` we get the index (not the value) of the minimal element of an array.\n",
    "```\n",
    "\n",
    "## Comparing Arrays\n",
    "\n",
    "Comparisons work elementwise, too."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "4a7a778b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ True False False]\n"
     ]
    }
   ],
   "source": [
    "a = np.array([1, 2, 3])\n",
    "b = np.array([-1, 3, 3])\n",
    "\n",
    "print(a > b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "578c4fd6",
   "metadata": {},
   "source": [
    "Comparisons result in NumPy arrays of data type `bool`.\n",
    "The function `np.any` returns `True` if and only if at least one item of the argument is `True`.\n",
    "The function `np.all` returns `True` if and only if all items of the argument are `True`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "d038a6c1",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n",
      "False\n"
     ]
    }
   ],
   "source": [
    "a = np.array([True, True, False])\n",
    "\n",
    "print(np.any(a))\n",
    "print(np.all(a))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30c54bf3",
   "metadata": {},
   "source": [
    "Some of NumPy's functions also are accessible as methods of `ndarray` objects. Examples are `any` and `all`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "9633c026",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n",
      "False\n"
     ]
    }
   ],
   "source": [
    "a = np.array([True, True, False])\n",
    "\n",
    "print(a.any())\n",
    "print(a.all())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8eed45f3",
   "metadata": {},
   "source": [
    "```{margin}\n",
    "Due to Python's internal workings for processing logical expressions efficiently it's not possible to redefine `and` and friends via dunder methods. Thus, there's no chance for NumPy to define it's own variant of `and`. There is a dunder method `__and__`, but that implements bitwise 'and' (Python operator `&`), which is something different than logical 'and'.\n",
    "```\n",
    "\n",
    "To combine several conditions you might use [`logical_and`](https://numpy.org/doc/stable/reference/generated/numpy.logical_and.html) and friends. Using Python's `and` (and friends) results in an error, because Python tries to convert a NumPy array to a `bool` value and it's not clear how to do this (any or all?).\n",
    "\n",
    "\n",
    "## Broadcasting\n",
    "\n",
    "If dimensions of the operands of a binary operation do not fit (short vector plus long vector, for instance) an exception is raised.\n",
    "But in some cases NumPy uses a technique called *broadcasting* to make dimensions fit by cloning suitable subarrays. Examples:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "d9e08d4f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1 2 3]] \n",
      "\n",
      "[[1. 1. 1.]\n",
      " [1. 1. 1.]\n",
      " [1. 1. 1.]\n",
      " [1. 1. 1.]] \n",
      "\n",
      "[[2. 3. 4.]\n",
      " [2. 3. 4.]\n",
      " [2. 3. 4.]\n",
      " [2. 3. 4.]]\n",
      "(4, 3)\n"
     ]
    }
   ],
   "source": [
    "a = np.array([[1, 2, 3]])    # 1 x 3\n",
    "b = np.ones((4, 3))          # 4 x 3\n",
    "\n",
    "c = a + b\n",
    "\n",
    "print(a, '\\n')\n",
    "print(b, '\\n')\n",
    "print(c)\n",
    "print(c.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "5af538ca",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1 2 3]] \n",
      "\n",
      "[[1]\n",
      " [2]\n",
      " [3]\n",
      " [4]] \n",
      "\n",
      "[[2 3 4]\n",
      " [3 4 5]\n",
      " [4 5 6]\n",
      " [5 6 7]] \n",
      "\n",
      "(4, 3)\n"
     ]
    }
   ],
   "source": [
    "a = np.array([[1, 2, 3]])              # 1 x 3\n",
    "b = np.array([[1], [2], [3], [4]])     # 4 x 1\n",
    "\n",
    "c = a + b\n",
    "\n",
    "print(a, '\\n')\n",
    "print(b, '\\n')\n",
    "print(c, '\\n')\n",
    "print(c.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "09008b6c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1 2 3] \n",
      "\n",
      "[[1. 1. 1.]\n",
      " [1. 1. 1.]\n",
      " [1. 1. 1.]\n",
      " [1. 1. 1.]] \n",
      "\n",
      "[[2. 3. 4.]\n",
      " [2. 3. 4.]\n",
      " [2. 3. 4.]\n",
      " [2. 3. 4.]]\n",
      "(4, 3)\n"
     ]
    }
   ],
   "source": [
    "a = np.array([1, 2, 3])    #     3 (one-dimensional)\n",
    "b = np.ones((4, 3))        # 4 x 3\n",
    "\n",
    "c = a + b\n",
    "\n",
    "print(a, '\\n')\n",
    "print(b, '\\n')\n",
    "print(c)\n",
    "print(c.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ab87d239",
   "metadata": {},
   "source": [
    "Broadcasting follows two simple rules:\n",
    "* If one array has fewer dimensions than the other, add dimensions of size 1 till dimensions are equal.\n",
    "* Compare array sizes in each dimension. If they equal, do nothing. If they differ and one array has size 1 in the dimension under consideration, clone the array with size 1 sufficiently often to fit the other array's size.\n",
    "\n",
    "Broadcasting makes life much easier. On the one hand it allows for operations where one operand is a scalar:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "d41a33ad",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1 2 3]\n",
      " [4 5 6]] \n",
      "\n",
      "7 \n",
      "\n",
      "[[ 8  9 10]\n",
      " [11 12 13]]\n",
      "(2, 3)\n"
     ]
    }
   ],
   "source": [
    "a = np.array([[1, 2, 3], [4, 5, 6]])    # 2 x 3\n",
    "b = 7                                   #     1 (one-dimensional)\n",
    "\n",
    "c = a + b\n",
    "\n",
    "print(a, '\\n')\n",
    "print(b, '\\n')\n",
    "print(c)\n",
    "print(c.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6de71895",
   "metadata": {},
   "source": [
    "On the other hand broadcasting allows for efficient column or row operations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "153dc7fa",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1 2 3]\n",
      " [4 5 6]] \n",
      "\n",
      "[[0.5]\n",
      " [2. ]] \n",
      "\n",
      "[[ 0.5  1.   1.5]\n",
      " [ 8.  10.  12. ]]\n",
      "(2, 3)\n"
     ]
    }
   ],
   "source": [
    "# multiply columns by different values\n",
    "\n",
    "a = np.array([[1, 2, 3], [4, 5, 6]])    # 2 x 3\n",
    "b = np.array([[0.5], [2]])              # 2 x 1\n",
    "\n",
    "c = a * b\n",
    "\n",
    "print(a, '\\n')\n",
    "print(b, '\\n')\n",
    "print(c)\n",
    "print(c.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d8d09735",
   "metadata": {},
   "source": [
    "For higher-dimensional examples have a look at [Broadcasting](https://numpy.org/doc/stable/user/basics.broadcasting.html) in NumPy's documentation."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
