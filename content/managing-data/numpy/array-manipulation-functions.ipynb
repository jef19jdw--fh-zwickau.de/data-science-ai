{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "9cc258c9",
   "metadata": {},
   "source": [
    "(managing-data:numpy:array-manipulation-functions)=\n",
    "# Array Manipulation Functions\n",
    "\n",
    "NumPy comes with lots of functions for manipulating arrays. Some of them are needed more often, others almost never.\n",
    "A comprehensive list is provided in [array manipulation routines](https://numpy.org/doc/stable/reference/routines.array-manipulation.html).\n",
    "Here we only mention some of the more important ones."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "b6f501cb",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a9c80038",
   "metadata": {},
   "source": [
    "## Modifying Shape with `reshape`\n",
    "\n",
    "A NumPy array's [`reshape`](https://numpy.org/doc/stable/reference/generated/numpy.reshape.html) method yields an array of different shape, but with identical data. The new array has to have the same number of elements as the old one."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "1b866f57",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1. 1. 1. 1. 1.] \n",
      "\n",
      "[[1. 1. 1. 1. 1.]] \n",
      "\n",
      "[[1.]\n",
      " [1.]\n",
      " [1.]\n",
      " [1.]\n",
      " [1.]]\n"
     ]
    }
   ],
   "source": [
    "a = np.ones(5)         # 1d (vector)\n",
    "b = a.reshape(1, 5)    # 2d (row matrix)\n",
    "c = a.reshape(5, 1)    # 2d (column matrix)\n",
    "\n",
    "print(a, '\\n')\n",
    "print(b, '\\n')\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "56e29a16",
   "metadata": {},
   "source": [
    "One dimension may be replaced by `-1` indicating that the size of this dimension shall be computed by NumPy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "4978849d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(8, 8) (4, 16)\n"
     ]
    }
   ],
   "source": [
    "a = np.ones((8, 8))\n",
    "b = a.reshape(4, -1)\n",
    "\n",
    "print(a.shape, b.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ed52c8a8",
   "metadata": {},
   "source": [
    "## Mirrowing with `fliplr` and `flipud`\n",
    "\n",
    "To mirrow a 2d array on its vertical or horizontal axis use [`fliplr`](https://numpy.org/doc/stable/reference/generated/numpy.fliplr.html) and [`flipud`](https://numpy.org/doc/stable/reference/generated/numpy.flipud.html), respectively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "dd2c770a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1 2 3]\n",
      " [4 5 6]] \n",
      "\n",
      "[[3 2 1]\n",
      " [6 5 4]]\n"
     ]
    }
   ],
   "source": [
    "a = np.array([[1, 2, 3], [4, 5, 6]])\n",
    "b = np.fliplr(a)\n",
    "\n",
    "print(a, '\\n')\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "697d81d4",
   "metadata": {},
   "source": [
    "## Joining Arrays with `concatenate` and `stack`\n",
    "\n",
    "Arrays of identical shape (except for one axis) may be joined along an existing axis to one large array with [`concatenate`](https://numpy.org/doc/stable/reference/generated/numpy.concatenate.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "c11d2875",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1. 1. 1. 0. 0. 0. 0. 0. 5. 5.]\n",
      " [1. 1. 1. 0. 0. 0. 0. 0. 5. 5.]]\n"
     ]
    }
   ],
   "source": [
    "a = np.ones((2, 3))\n",
    "b = np.zeros((2, 5))\n",
    "c = np.full((2, 2), 5)\n",
    "d = np.concatenate((a, b, c), axis=1)\n",
    "\n",
    "print(d)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f7337f3b",
   "metadata": {},
   "source": [
    "If identically shaped array shall be joined along a new axis, use [`stack`](https://numpy.org/doc/stable/reference/generated/numpy.stack.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "cb41695b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1. 0. 5.]\n",
      " [1. 0. 5.]]\n"
     ]
    }
   ],
   "source": [
    "a = np.ones(2)\n",
    "b = np.zeros(2)\n",
    "c = np.full(2, 5)\n",
    "d = np.stack((a, b, c), axis=1)\n",
    "\n",
    "print(d)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bbf3e5d5",
   "metadata": {},
   "source": [
    "## Appending Data with `append`\n",
    "\n",
    "Like Python lists NumPy arrays may be extended by appending further data. The [`append`](https://numpy.org/doc/stable/reference/generated/numpy.append.html) method takes the original array and the new data and returns the extended array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "3e14fb59",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1. 1. 1.]\n",
      " [1. 1. 1.]\n",
      " [1. 1. 1.]\n",
      " [1. 2. 3.]]\n"
     ]
    }
   ],
   "source": [
    "a = np.ones((3, 3))\n",
    "b = np.append(a, [[1, 2, 3]], axis=0)\n",
    "\n",
    "print(b)"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
