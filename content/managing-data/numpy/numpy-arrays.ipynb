{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "021458ba",
   "metadata": {},
   "source": [
    "(managing-data:numpy:numpy-arrays)=\n",
    "# NumPy Arrays\n",
    "\n",
    "NumPy provides two fundamental tools for data science purposes:\n",
    "* a data type for storing tabular numerical data,\n",
    "* very efficient functions for computations on large amounts of numerical data.\n",
    "\n",
    "NumPy's basic data type is called `ndarray` (n-dimensional array), often called *NumPy array*.\n",
    "\n",
    "NumPy's standard abbreviation is `np`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "555afd4c",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "03887eda",
   "metadata": {},
   "source": [
    "## Python Lists versus NumPy Arrays\n",
    "\n",
    "From mathematics we know [](math:linalg:vectors) and [](math:linalg:matrices). A vector is a (one-dimensional) list of numbers. A matrix is a (two-dimensional) field of numbers. Vectors could be represented by lists in Python, whereas a matrix would be a list of lists (a list of rows or a list of columns).\n",
    "\n",
    "Using Python lists for representing large vectors and matrices is very inefficient. Each item of a Python list has its own location somewhere in memory. When reading a whole list, to multiply a vector by some number, for instance, Python reads the first list item, then looks for the memory location of the second, then reads the second, and so on. A lot of memory management is involved.\n",
    "\n",
    "To significantly improve performance, NumPy provides the `ndarray` data type. The most important property of an `ndarray` is its dimension. A one-dimensional array stores a vector. A two-dimensional array stores a matrix. Zero-dimensional arrays store nothing, but are valid Python objects. Visualization of arrays with dimension above two is somewhat difficult. A three-dimensional array can be visualized as cuboid of numbers, each number described by three indices (row, column, depth level). We will meet dimensions of three and above almost every day when diving into machine learning. One example are color images: two-dimensions for pixel positions, one dimension for color channels (red, green, blue, transparency).\n",
    "\n",
    "Why are NumPy arrays more efficient?\n",
    "* All items of a NumPy array have to have **identical data type**, mostly float or integer. This saves time and memory for handling different types and type conversions.\n",
    "* All items of a NumPy array are stored in a **well-structured contiguous block of memory**. To find the next item or to copy a whole array or part of it much less memory management operations are required.\n",
    "* NumPy provides **optimized mathematical operations for vectors and matrices**. Instead of processing arrays item by item, NumPy functions take the whole array and process it in compiled C code. Thus, the item-by-item part is not done by the (slow) Python interpreter, but by (very fast) compiled code.\n",
    "\n",
    "## Creating NumPy Arrays\n",
    "\n",
    "### Converting Python Lists to Arrays\n",
    "\n",
    "There are several ways to create NumPy arrays. We start with conversion of Python lists or tuples by NumPy's `array` function.\n",
    "\n",
    "Passing a list or a tuple to `array` yields a one-dimensional `ndarray`. The data type is determined by NumPy to be the simplest type which can hold all objects in the list or tuple."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "ad28c2f1",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[23 42  7  4 -2]\n",
      "int64\n"
     ]
    }
   ],
   "source": [
    "a = np.array([23, 42, 7, 4, -2])\n",
    "\n",
    "print(a)\n",
    "print(a.dtype)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c4c4af3",
   "metadata": {},
   "source": [
    "The member variable `ndarray.dtype` contains the array's data type. Here NumPy decided to use `int64`, that is, integers of length 8 byte. Available types will be discussed below. An example with floats:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "43512cf5",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ 2.3  4.2  7.   4.  -2. ]\n",
      "float64\n"
     ]
    }
   ],
   "source": [
    "b = np.array([2.3, 4.2, 7, 4, -2])\n",
    "\n",
    "print(b)\n",
    "print(b.dtype)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41e59bc6",
   "metadata": {},
   "source": [
    "```{important}\n",
    "NumPy ships with its own data types for numbers to allow for more efficient storage and computations. Python's `int` type allows for arbitrarily large numbers, whereas NumPy has different types for integers with different (and finite!) numerical ranges. NumPy also knows several types of floats differing in precision (number of decimal places) and range. Wherever possible conversion between Python types and NumPy types is done automatically.\n",
    "```\n",
    "\n",
    "### Higher-Dimensional Arrays from Lists\n",
    "\n",
    "To get higher-dimensional arrays use nested lists:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "9287abfa",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1 2 3]\n",
      " [4 5 6]\n",
      " [7 8 9]]\n"
     ]
    }
   ],
   "source": [
    "c = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])\n",
    "\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4f668f25",
   "metadata": {},
   "source": [
    "### New Arrays as Return Values\n",
    "\n",
    "Next to explicit creation, NumPy arrays may be the result of mathematical operations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "e35aaf01",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'numpy.ndarray'>\n"
     ]
    }
   ],
   "source": [
    "d = a + b\n",
    "\n",
    "print(type(d))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c18f8f8",
   "metadata": {},
   "source": [
    "To see that `d` is indeed a new `ndarray` and not an in-place modified `a` or `b`, we might look at the object ids, which are all different:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "79320f7f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "140549050909872 140549364309744 140549050912656\n"
     ]
    }
   ],
   "source": [
    "print(id(a), id(b), id(d))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39398923",
   "metadata": {},
   "source": [
    "### Functions for Creating Special Arrays\n",
    "\n",
    "A third way for creating NumPy arrays is to call specific NumPy functions returning new arrays. From `np.zeros` we get an array of zeros. From `np.ones` we get an array of ones. There are much more functions like `zeros` and `ones`, see [Array creation routines](https://numpy.org/doc/stable/reference/routines.array-creation.html#routines-array-creation) in Numpy's documentation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "8d6d61dc",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[0. 0. 0. 0. 0.] \n",
      "\n",
      "[[1. 1. 1.]\n",
      " [1. 1. 1.]]\n"
     ]
    }
   ],
   "source": [
    "a = np.zeros(5)\n",
    "b = np.ones((2, 3))\n",
    "\n",
    "print(a, '\\n')\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1bca6ce7",
   "metadata": {},
   "source": [
    "NumPy almost always defaults to floats if no data type is explicitly provided.\n",
    "\n",
    "## Properties of NumPy Arrays\n",
    "\n",
    "Objects of type `ndarray` have several member variables containing important information about the array:\n",
    "* `ndim`: number of dimensions,\n",
    "* `shape`: tuple of length `ndim` with array size in each dimension,\n",
    "* `size`: total number of elements,\n",
    "* `nbytes`: number of bytes occupied by the array elements,\n",
    "* `dtype`: the array's data type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "cd640c9e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2\n",
      "(4, 3)\n",
      "12\n",
      "96\n",
      "float64\n"
     ]
    }
   ],
   "source": [
    "a = np.zeros((4, 3))\n",
    "\n",
    "print(a.ndim)\n",
    "print(a.shape)\n",
    "print(a.size)     # 4 * 3\n",
    "print(a.nbytes)   # 4 * 3 * 8\n",
    "print(a.dtype)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "441fdd38",
   "metadata": {},
   "source": [
    "It's important to know that `shape` matters. In mathematics almost always we identify vectors with matrices having only one column. But in NumPy these are two different things. A vector has shape `(n, )`, that is `ndim` is 1, whereas a one-column matrix has shape `(n, 1)` with `ndim` of 2. Consequently, a vector neither is a row nor a column in NumPy. It's simply a list of numbers, nothing more."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "9e7f8459",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[0. 0. 0. 0. 0.] \n",
      "\n",
      "[[0.]\n",
      " [0.]\n",
      " [0.]\n",
      " [0.]\n",
      " [0.]] \n",
      "\n",
      "[[0. 0. 0. 0. 0.]]\n"
     ]
    }
   ],
   "source": [
    "a = np.zeros(5)\n",
    "b = np.zeros((5, 1))\n",
    "c = np.zeros((1, 5))\n",
    "\n",
    "print(a, '\\n')\n",
    "print(b, '\\n')\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d4a2edb",
   "metadata": {},
   "source": [
    "## List-Like Indexing\n",
    "\n",
    "Elements of NumPy arrays can be accessed similarly to items of Python lists. That is, the first item in a one-dimensional `ndarray` has index 0 and the last one has index `ndarray.size`. Slicing is allowed, too."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "0c517202",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "23 \n",
      "\n",
      "[42 -7] \n",
      "\n",
      "[23 -7 10]\n"
     ]
    }
   ],
   "source": [
    "a = np.array([23, 42, -7, 3, 10])\n",
    "\n",
    "print(a[0], '\\n')\n",
    "print(a[1:3], '\\n')\n",
    "print(a[::2])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23a593dc",
   "metadata": {},
   "source": [
    "In case of multi-dimensional arrays we have to provide an index for each dimension. Slicing is done per dimension."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "f4b46d5c",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[1 2 3]\n",
      " [4 5 6]\n",
      " [7 8 9]] \n",
      "\n",
      "2 \n",
      "\n",
      "[[4 5]\n",
      " [7 8]] \n",
      "\n",
      "[[1 3]\n",
      " [7 9]] \n",
      "\n",
      "[4 5 6]\n"
     ]
    }
   ],
   "source": [
    "a = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])\n",
    "print(a, '\\n')\n",
    "\n",
    "print(a[0, 1], '\\n')\n",
    "print(a[1:3, 0:2], '\\n')\n",
    "print(a[::2, ::2], '\\n')\n",
    "print(a[1, :])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ebb2cf1",
   "metadata": {},
   "source": [
    "Here `:` stands for 'all indices of the dimension'.\n",
    "\n",
    "```{note}\n",
    "Selecting all elements in the last dimensions like in `a[1, :]` can be abbreviated to `a[1]`. Same holds for higher dimensions: `a[1, 3, :, :]` is equivalent to `a[1, 3]`. The drawback is that one doesn't see immediately the array's dimensionality.\n",
    "```\n",
    "\n",
    "## Data Types\n",
    "\n",
    "NumPy knows many different numerical data types. Often we do not have to care about types (NumPy will choose suitable ones), but sometimes we have to specify data types explicitly (see examples below).\n",
    "\n",
    "Almost all NumPy functions accept the keyword argument `dtype` to specify the data type of the function's return value. Either pass a string with the desired type's name or pass a `type` object. Passing Python types like `int` makes NumPy choose the most appropriate NumPy type (here, `np.int64` or the string `'int64'`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "79ec9e53",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[0. 0. 0.]\n",
      " [0. 0. 0.]] \n",
      "\n",
      "[[0 0 0]\n",
      " [0 0 0]]\n"
     ]
    }
   ],
   "source": [
    "a = np.zeros((2, 3))\n",
    "b = np.zeros((2, 3), dtype=np.int64)\n",
    "\n",
    "print(a, '\\n')\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a958e867",
   "metadata": {},
   "source": [
    "NumPy types for integers:\n",
    "* `np.int8`, `np.int16`, `np.int32`, `np.int64` (signed integers of different range),\n",
    "* `np.uint8`, `np.uint16`, `np.uint32`, `np.uint64` (unsigned integers of different range),\n",
    "\n",
    "NumPy types for floats:\n",
    "* `np.float16`, `np.float32`, `np.float64` (different precision and range)\n",
    "\n",
    "For booleans there is `np.bool8`, which is very similar to Pythons `bool` (both using 8 times as much memory as required).\n",
    "\n",
    "Types for complex numbers are available, too. See [Built-in scalar types](https://numpy.org/doc/stable/reference/arrays.scalars.html#built-in-scalar-types) in NumPy's documentation for details.\n",
    "\n",
    "```{hint}\n",
    "The `dtype` member of `ndarray`s and the `dtype` argument to NumPy functions carry more information than the bare type (e.g., 'signed integer of length 64 bits'). They also contain information about how data is organized in memory. This is important for efficient import of data from external sources. Details will be discussed in [](managing-data:saving-loading-data).\n",
    "```\n",
    "\n",
    "### Example: Saving Memory by Manually Choosing Types\n",
    "\n",
    "Working with large NumPy arrays we have to save memory wherever possible. One important ingredient for memory efficiency is choosing small types, that is, types with small range. Often we work with arrays of zeros and ones or of small integers only. Then we should choose the smallest integer type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "344d0701",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "a has 8000 bytes\n",
      "b has 1000 bytes\n"
     ]
    }
   ],
   "source": [
    "a = np.ones(1000)    # defaults to np.int64\n",
    "b = np.ones(1000, dtype=np.int8)\n",
    "\n",
    "print(f'a has {a.nbytes} bytes')\n",
    "print(f'b has {b.nbytes} bytes')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "afdab13e",
   "metadata": {},
   "source": [
    "Having a data set with one billion numbers choosing the correct type decides about requiring 1 GB or 8 GB of memory!\n",
    "\n",
    "### Example: Unsuitable Default Type\n",
    "\n",
    "Creating an array without explicitly providing a data type makes NumPy choose `np.int64` or `np.float64` depending on the presence of floats. This may lead to hard to find errors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "8226f876",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1 4 6 0]\n"
     ]
    }
   ],
   "source": [
    "a = np.array([2, 4, 6, 1])    # defaults to np.int64\n",
    "a[0] = 1.23\n",
    "a[3] = 0.99\n",
    "\n",
    "print(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f69bdd0c",
   "metadata": {},
   "source": [
    "Modifying values in integer arrays converts the new values to the array's data type, even if information will be lost.\n",
    "To avoid such errors always specify types if working with floats!"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
