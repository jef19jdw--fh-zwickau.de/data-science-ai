{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "495299c4",
   "metadata": {},
   "source": [
    "(managing-data:numpy:efficiency-considerations)=\n",
    "# Efficiency Considerations\n",
    "\n",
    "When working with large arrays the most expensive operation (longest execution time) is copying arrays. Thus, we should avoid making copies of arrays. But there are some more things to consider when optimizing execution time and memory consumption."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "a975f256",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-13T14:16:10.004240Z",
     "iopub.status.busy": "2024-12-13T14:16:10.004142Z",
     "iopub.status.idle": "2024-12-13T14:16:10.049632Z",
     "shell.execute_reply": "2024-12-13T14:16:10.049296Z",
     "shell.execute_reply.started": "2024-12-13T14:16:10.004229Z"
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8c7c9975",
   "metadata": {},
   "source": [
    "## Don't Use `append` in Loops\n",
    "\n",
    "Sometimes data comes in in chunks and we have to build a large array step by step. We could start with an empty array and append each new chunk of data.\n",
    "If incoming chunks are single numbers, code could look as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "9425d587",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-13T14:16:10.054283Z",
     "iopub.status.busy": "2024-12-13T14:16:10.053062Z",
     "iopub.status.idle": "2024-12-13T14:16:19.433596Z",
     "shell.execute_reply": "2024-12-13T14:16:19.432828Z",
     "shell.execute_reply.started": "2024-12-13T14:16:10.054260Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "115 μs ± 1.02 μs per loop (mean ± std. dev. of 7 runs, 10,000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%%timeit\n",
    "\n",
    "a = np.array([], dtype=np.int64)\n",
    "\n",
    "for k in range(0, 100):\n",
    "    a = np.append(a, k)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2de8b04",
   "metadata": {},
   "source": [
    "Each call to `append` creates a new (larger) array and copies the existing one into the new one. In the end we made 100 expensive copy operations.\n",
    "\n",
    "If we know the final size of our array in advance, then we should create an array of final size before filling it with data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "f588ee5d",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-13T14:16:19.434511Z",
     "iopub.status.busy": "2024-12-13T14:16:19.434379Z",
     "iopub.status.idle": "2024-12-13T14:16:22.846048Z",
     "shell.execute_reply": "2024-12-13T14:16:22.845321Z",
     "shell.execute_reply.started": "2024-12-13T14:16:19.434500Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "4.19 μs ± 18.9 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%%timeit\n",
    "\n",
    "a = np.empty(100, dtype=np.int64)\n",
    "\n",
    "for k in range(0, 100):\n",
    "    a[k] = k"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "614a6e7b",
   "metadata": {},
   "source": [
    "We get a speed-up of factor 30, because repeated copying is avoided.\n",
    "\n",
    "## Append to Lists Instead of Arrays\n",
    "\n",
    "If data comes in in chunks and we do not know the final array's size in advance, we should use a Python list for temporarily storing data. Appending to a Python list is cheap, because existing list data won't be copied. Each list item has its own (more or less random) location in memory. If data is complete, we create a NumPy array of correct size and copy the list's items to the array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "dccaf658",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-13T14:16:22.846509Z",
     "iopub.status.busy": "2024-12-13T14:16:22.846403Z",
     "iopub.status.idle": "2024-12-13T14:16:25.966425Z",
     "shell.execute_reply": "2024-12-13T14:16:25.965891Z",
     "shell.execute_reply.started": "2024-12-13T14:16:22.846498Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3.84 μs ± 11.6 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%%timeit\n",
    "\n",
    "a = []\n",
    "for k in range(0, 100):\n",
    "    a.append(k)\n",
    "\n",
    "b = np.array(a, dtype=np.int64)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d9ef0d4b",
   "metadata": {},
   "source": [
    "Speed-up compared to `np.append` is factor 30.\n",
    "\n",
    "## Use Multidimensional Indices\n",
    "\n",
    "For multidimensional arrays we have two indexing variants:\n",
    "* multidimensional indexing (e.g., `a[0, 0, 0]`),\n",
    "* repeated onedimensional indexing (e.g., `a[0][0][0]`).\n",
    "\n",
    "The latter creates a lower-dimensional slice `a[0]`, then indexes this slice, creating another slice, and so on. This process is less efficient than using multidimensional indices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "5d3535d4-f851-4060-bfd9-533e8149c21b",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-13T14:16:25.966862Z",
     "iopub.status.busy": "2024-12-13T14:16:25.966754Z",
     "iopub.status.idle": "2024-12-13T14:16:25.970506Z",
     "shell.execute_reply": "2024-12-13T14:16:25.969991Z",
     "shell.execute_reply.started": "2024-12-13T14:16:25.966851Z"
    }
   },
   "outputs": [],
   "source": [
    "a = np.ones((100, 100, 100))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "f329068b",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-13T14:16:25.970933Z",
     "iopub.status.busy": "2024-12-13T14:16:25.970823Z",
     "iopub.status.idle": "2024-12-13T14:16:40.254118Z",
     "shell.execute_reply": "2024-12-13T14:16:40.253503Z",
     "shell.execute_reply.started": "2024-12-13T14:16:25.970922Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "17.5 μs ± 592 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%%timeit\n",
    "\n",
    "for k in range(0, 100):\n",
    "    b = a[k][k][k]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "2cb5fb04",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-13T14:16:40.254542Z",
     "iopub.status.busy": "2024-12-13T14:16:40.254444Z",
     "iopub.status.idle": "2024-12-13T14:16:45.801156Z",
     "shell.execute_reply": "2024-12-13T14:16:45.800574Z",
     "shell.execute_reply.started": "2024-12-13T14:16:40.254533Z"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "6.82 μs ± 11.6 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%%timeit\n",
    "\n",
    "for k in range(0, 100):\n",
    "    b = a[k, k, k]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bdf2d881",
   "metadata": {},
   "source": [
    "Speed-up is almost factor 2 to 3.\n",
    "\n",
    "## Remove Unused Arrays from Memory\n",
    "\n",
    "Working with large arrays we should free memory as soon as possible (use `del`). A non-obvious situation, where memory can be freed, is when using small views of large arrays. Consider the following code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "4f09ac89",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-13T14:16:45.801572Z",
     "iopub.status.busy": "2024-12-13T14:16:45.801473Z",
     "iopub.status.idle": "2024-12-13T14:16:45.804553Z",
     "shell.execute_reply": "2024-12-13T14:16:45.804082Z",
     "shell.execute_reply.started": "2024-12-13T14:16:45.801562Z"
    }
   },
   "outputs": [],
   "source": [
    "a = np.ones((100, 100))    # large array resulting from some computation\n",
    "b = a[0, :]    # we only need the first row\n",
    "del a"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "06eeaf89",
   "metadata": {},
   "source": [
    "Here the large array remains in memory although we only need the first row. Because the view `b` is based on the array object `a`, `del a` only removes the name `a`, but garbage collection cannot remove the array object. More efficient code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "b0f981b6",
   "metadata": {
    "execution": {
     "iopub.execute_input": "2024-12-13T14:16:45.805408Z",
     "iopub.status.busy": "2024-12-13T14:16:45.805200Z",
     "iopub.status.idle": "2024-12-13T14:16:45.831484Z",
     "shell.execute_reply": "2024-12-13T14:16:45.830209Z",
     "shell.execute_reply.started": "2024-12-13T14:16:45.805388Z"
    }
   },
   "outputs": [],
   "source": [
    "a = np.ones((100, 100))    # large array resulting from some computation\n",
    "b = a[0, :].copy()    # we only need the first row, make a copy\n",
    "del a    # remove large array from memory"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "341218e4",
   "metadata": {},
   "source": [
    "Here only the first (copied) row remains in memory. The original large array will be removed from memory by Python's garbage collection as soon as possible."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
