{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "efe728e6",
   "metadata": {},
   "source": [
    "(managing-data:numpy:vectorization)=\n",
    "# Vectorization\n",
    "\n",
    "NumPy is very fast if operations are applied to whole arrays instead of element-by-element. Thus, we should try to avoid iterating through array elements and processing single elements. This idea is known as *vectorization*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "740b618f",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42ab2865",
   "metadata": {},
   "source": [
    "## Example: Vectorization via Indexing\n",
    "\n",
    "Imagine we have a vector of length $n$, where $n$ is even. We would like to interchange each number at an even index with its successor. The result shall be stored in a new array.\n",
    "\n",
    "Here is the code based on a loop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "bb94fab0",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2 1 4 3 6 5 8 7]\n"
     ]
    }
   ],
   "source": [
    "def interchange_loop(a):\n",
    "    result = np.empty_like(a)\n",
    "    for k in range(0, int(a.size / 2)):\n",
    "        result[2 * k] = a[2 * k + 1]\n",
    "        result[2 * k + 1] = a[2 * k]\n",
    "    return result\n",
    "\n",
    "print(interchange_loop(np.array([1, 2, 3, 4, 5, 6, 7, 8])))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7a5965bc",
   "metadata": {},
   "source": [
    "And here with vectorization:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "ff15cedc",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2 1 4 3 6 5 8 7]\n"
     ]
    }
   ],
   "source": [
    "def interchange_vectorized(a):\n",
    "    result = np.empty_like(a)\n",
    "    result[0::2] = a[1::2]\n",
    "    result[1::2] = a[0::2]\n",
    "    return result\n",
    "\n",
    "print(interchange_vectorized(np.array([1, 2, 3, 4, 5, 6, 7, 8])))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4668e55e",
   "metadata": {},
   "source": [
    "Now let's look at the execution times:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "4b95ab52",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "178 µs ± 5.66 µs per loop (mean ± std. dev. of 7 runs, 10,000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%%timeit\n",
    "interchange_loop(np.zeros(1000))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "3ecc4da2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2.96 µs ± 266 ns per loop (mean ± std. dev. of 7 runs, 100,000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%%timeit\n",
    "interchange_vectorized(np.zeros(1000))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50a0fb87",
   "metadata": {},
   "source": [
    "The speed-up is of a factor about 60 for $n=1000$ and becomes much better if $n$ grows.\n",
    "\n",
    "## Example: Vectorization via Broadcasting\n",
    "\n",
    "Given two lists of numbers we want to have a two-dimensional array containing all products from these numbers.\n",
    "\n",
    "Here is the code based on a loop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "be0ed93f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[0.00000e+00 0.00000e+00 0.00000e+00 ... 0.00000e+00 0.00000e+00\n",
      "  0.00000e+00]\n",
      " [0.00000e+00 1.00000e+00 2.00000e+00 ... 9.97000e+02 9.98000e+02\n",
      "  9.99000e+02]\n",
      " [0.00000e+00 2.00000e+00 4.00000e+00 ... 1.99400e+03 1.99600e+03\n",
      "  1.99800e+03]\n",
      " ...\n",
      " [0.00000e+00 9.97000e+02 1.99400e+03 ... 9.94009e+05 9.95006e+05\n",
      "  9.96003e+05]\n",
      " [0.00000e+00 9.98000e+02 1.99600e+03 ... 9.95006e+05 9.96004e+05\n",
      "  9.97002e+05]\n",
      " [0.00000e+00 9.99000e+02 1.99800e+03 ... 9.96003e+05 9.97002e+05\n",
      "  9.98001e+05]]\n"
     ]
    }
   ],
   "source": [
    "def products_loop(a, b):\n",
    "    result = np.empty((len(a), len(b)))\n",
    "    for i in range(0, len(a)):\n",
    "        for j in range(0, len(b)):\n",
    "            result[i, j] = a[i] * b[j]\n",
    "    return result\n",
    "\n",
    "print(products_loop(range(1000), range(1000)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d0a56a8",
   "metadata": {},
   "source": [
    "And here with vectorization:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "a2f3404a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[     0      0      0 ...      0      0      0]\n",
      " [     0      1      2 ...    997    998    999]\n",
      " [     0      2      4 ...   1994   1996   1998]\n",
      " ...\n",
      " [     0    997   1994 ... 994009 995006 996003]\n",
      " [     0    998   1996 ... 995006 996004 997002]\n",
      " [     0    999   1998 ... 996003 997002 998001]]\n"
     ]
    }
   ],
   "source": [
    "def products_vectorized(a, b):\n",
    "    result = np.array([a]).T * np.array([b])\n",
    "    return result\n",
    "\n",
    "print(products_vectorized(range(1000), range(1000)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10a9b27a",
   "metadata": {},
   "source": [
    "```{hint}\n",
    "The `T` member variable of a NumPy array provides the transposed array. It's not a copy (expensive) but a view (cheap). For detail see [](managing-data:numpy:linear-algebra-functions).\n",
    "```\n",
    "\n",
    "Execution times:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "2cea10b2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "248 ms ± 3.36 ms per loop (mean ± std. dev. of 7 runs, 1 loop each)\n"
     ]
    }
   ],
   "source": [
    "%%timeit\n",
    "products_loop(range(1000), range(1000))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "a4e5d974",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.25 ms ± 11.4 µs per loop (mean ± std. dev. of 7 runs, 1,000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%%timeit\n",
    "products_vectorized(range(1000), range(1000))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "74f52526",
   "metadata": {},
   "source": [
    "Speed-up is factor 200 for lists of lenth 1000.\n",
    "\n",
    "## Important\n",
    "\n",
    "Whenever you see a loop in numerical routines, spend some time to vectorize it. Almost always that's possible. Often vectorization also increases readability of the code."
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "content_md///md:myst,content///ipynb"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
