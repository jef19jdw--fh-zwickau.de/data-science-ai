---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(python:further-python-features)=
# Further Python Features

Python provides much more features than we need. Here we list some of them, which might be of interest either because they simplify some coding tasks or because they frequently occur in other people's code.

## Doing Nothing With `pass`

Python does not allow emtpy functions, classes, loops and so on. But there is a 'do nothing' command, the [`pass`](https://docs.python.org/3/reference/simple_stmts.html#the-pass-statement) keyword.

```{code-cell} ipython3
def do_nothing():

    pass
    
    
do_nothing()
```

## Checking Conditions With `assert`

Especially for debugging purposes placing multiple `if` statements can be avoided by using [`assert`](https://docs.python.org/3/reference/simple_stmts.html#the-assert-statement) instead. The condition following `assert` is evaluated. If the result is `False` an `AssertionError` is raised, else nothing happens. An optional error message is possible, too.

```{code-cell} ipython3
:tags: [raises-exception]

a = 0   # some number from somewhere (e.g., user input)

assert a != 0, 'Zero is not allowed!'
    
b = 1 / 0
```

## Structural Pattern Matching

Python 3.10 introduces two new keywords: `match` and `case`. In the simplest case they can be used to replace `if...elif...elif...else` constructs for discrimating many cases. But in addition they provide a powerful pattern matching mechanism. See [PEP 636 - Structural Pattern Matching: Tutorial](https://peps.python.org/pep-0636/) for an introduction.

## The `set` Type

Python knows a data type for representing (mathematical) sets. Its name is `set`. Typical operations like unions and intersections of sets are supported. The official [Python Tutorial](https://docs.python.org/3/tutorial/datastructures.html#sets) shows basic usage.

## Function Decorators

Function decorators are *syntactic suggar* (that is, not really needed), but very common. They precede a function definition and consist of an `@` character and a function name. Function decorators are used to modify a function by applying another function to it. The following two code cells are more or less equivalent:

```{code-cell} ipython3
def double(func):
    return lambda x: 2 * func(x)

@double
def calc_something(x):
    return x * x
```

```{code-cell} ipython3
def double(func):
    return lambda x: 2 * func(x)

def calc_something(x):
    return x * x
    
calc_something = double(calc_something)
```

See [Function definitions](https://docs.python.org/3/reference/compound_stmts.html#function-definitions) in Python's documention for details.

## The `copy` Module

The [`copy` module](https://docs.python.org/3/library/copy.html) provides functions for shallow and deep copying of objects. For discussion of the copy problem in the context of lists see [](python:lists-friends:lists:copies).

## Multitasking

Reading in large data files or downloading data from some server may take a while. During this time the CPU is more or less idle and could do some heavy computations without slowing down data transfer. This a typical situation where one wants to have two Python programs or two parts of one and the same program running in parallel, possibly communicating with each other.

Python has the [`threading`](https://docs.python.org/3/library/threading.html) module for real multitasking on operating system level. The [`asynio`](https://docs.python.org/3/library/asyncio.html) module in combination with the `async` and `await` keywords provides a simpler multithreading approach completely controlled by the Python interpreter.

## Graphical User Interfaces (GUIs)

The Python ecosystem provides lots of packages for creating and controlling graphical user interfaces (GUIs). Here are some widely used ones:
* [`tkinter`](https://docs.python.org/3/library/tkinter.html) in Python's standard library provides support for classical desktop applications with a main window, subwindows, buttons, text fields, and so on.
* [`ipywidgets`](https://ipywidgets.readthedocs.io) is a very easy to use package for creating graphical user interfaces in Jupyter notebooks. For instance a slider widget could control some parameter of an algorithm.
* [`flask`](https://flask.palletsprojects.com) is a package for building web apps with Python, that is, the user interacts via a website with the Python program running on a server.
