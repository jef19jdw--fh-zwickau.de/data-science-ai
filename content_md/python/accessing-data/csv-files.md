---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(python:accessing-data:csv-files)=
# CSV Files

The simplest form for storing spreadsheet data are *comma separated values* (CSV) in a text file. Each line of a CSV file contains one row of the spreadsheet. The columns are separated by commas and sometimes by another symbol. CSV files may contain column headers in their first line(s).

A typical CSV file looks like that:

```
first_name,last_name,town
John,Miller,Atown
Ann,Abor,Betown
Bob,Builder,Cetown
Nina,Morning,Detown
```

CSV files are not standardized. Thus, there might be cumbersome deviations from what one expects to be a simple CSV file. The CSV format is used to move data between different sources which cannot read each others native file formats.

In Python we can use the module `csv` for reading data from CSV files. It provides the class `csv.reader`. When creating a `csv.reader` object we have to pass a file object of the CSV file as parameter. The `csv.reader` object then is an iterator object. It yields one line of the CSV file per iteration. More precisely, it yields a list of strings. Each string contains the data from the corresponding column.

See [documentation of `csv` module](https://docs.python.org/3/library/csv.html) for details.
