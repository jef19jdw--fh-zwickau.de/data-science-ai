---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(python:crash)=
# Crash Course

This chapter provides an overview of everyday Python features and their basic usage.

```{tableofcontents}
```

Related exercises:
* [](exercises:python:finding-errors)
* [](exercises:python:basics)
* [](projects:python:simple-list)
* [](exercises:python:more-basics)
* [](projects:python:geometric-objects)
