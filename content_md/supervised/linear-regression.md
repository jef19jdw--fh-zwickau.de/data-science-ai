---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(supervised:linear-regression)=
# Linear Regression

Linear regression denotes a class of relatively simple yet powerful regression methods. In this chapter we study the basics as well as advanced techniques and special cases.

```{tableofcontents}
```

Related projects:
* [](projects:house-prices:gui)
