---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(supervised:trees)=
# Decision Trees

Decision trees are a class of relatively simple yet powerful machine learning methods suited for both regression and classification.

```{tableofcontents}
```

Related projects:
* [](projects:forged-banknotes)
  * [](projects:forged-banknotes:tree) (project)
