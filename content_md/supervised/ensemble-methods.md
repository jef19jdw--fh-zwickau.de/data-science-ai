---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(supervised:ensemble-methods)=
# Ensemble Methods

To increase overall prediction quality we could train several different models and somehow aggregate their prediction results. There are many different ways to realize this idea. Machine learning methods exploiting more than one trained model are called *ensemble methods*. Here we consider three classes of ensemble methods: *stacking*, *bootstrap aggregation (bagging)*, *boosting*.

```{tableofcontents}
```

Related projects:
* [](projects:forged-banknotes)
  * [](projects:forged-banknotes:random-forest)
* [](projects:house-prices)
  * [](projects:house-prices:random-forest)
