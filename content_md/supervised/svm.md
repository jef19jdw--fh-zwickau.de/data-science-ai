---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(supervised:svm)=
# Support-Vector Machines

The term support-vector machine (SVM) is used for a combination of certain machine learning models and corresponding training algorithms. SVMs are restricted to binary classification tasks, but can be applied to multiclass tasks via one-versus-rest or one-versus-one approach.

SVMs yield a separating hyperplane with maximum distance to both classes. Extensions to nonlinear separation will be discussed, too (kernel SVMs). Linear SVMs come in two flavors: hard margin SVMs and soft margin SVMs.

SVMs are state-of-art techniques for classification. Necessary computations can be implemented very efficiently and resulting models are very robust to changes in the training data. SVMs are well suited for very high dimensional data sets. A trained SVM model requires only very few memory (in constrast to ANNs) and predictions can be computed very efficiently.

```{tableofcontents}
```

Related projects:
* [](projects:forged-banknotes)
  * [](projects:forged-banknotes:svm)
* [](projects:mnist)
  * [](projects:mnist:qmnist-svm)
