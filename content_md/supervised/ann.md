---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"tags": [], "user_expressions": []}

(supervised:ann)=
# Artificial Neural Networks

Artificial Neural Networks (ANNs) are a fundamental technique in modern machine learning and artificial intelligence. The buzzword *deep learning* is used for artificial neural networks with many layers of neurons. In this chapter we consider layered ANNs and the important special case *convolutional ANNs (CNNs)*.

```{tableofcontents}
```

Related projects:
* [](projects:install:long-running)
* [](projects:house-prices:ann)
* [](projects:mnist:qmnist-pca-ann)
* [](projects:mnist:qmnist-cnn)
* [](projects:mnist:qmnist-cnn-analysis)
* [](projects:cats-dogs-hyperparameters)
