---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(supervised:general)=
# General Considerations

Before we consider concrete classes of supervised learning methods, we have a look at basic principles common to (almost) all methods.

```{tableofcontents}
```

Related projects:
* [](projects:forged-banknotes)
  * [](projects:forged-banknotes:knn)
  * [](projects:forged-banknotes:quality)
  * [](projects:forged-banknotes:hyperparameter-optimization)
* [](projects:mnist:qmnist-feature-reduction)
