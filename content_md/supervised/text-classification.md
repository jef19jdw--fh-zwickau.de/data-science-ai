---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(supervised:text-classification)=
# Text Classification

Up to now we only considered classification tasks with numerical features. But the very important field of text analysis and classification lacks such numerical features. Here we discuss relevant points to consider when using texts as model inputs. To avoid too much theory we immediately apply everything to a real-world example.

```{tableofcontents}
```

Related projects:
* [](projects:blogs)
  * [](projects:blogs:author-classification-training)
  * [](projects:blogs:author-classification-test)
