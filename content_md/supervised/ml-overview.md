---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(supervised:ml-overview)=
# Machine Learning Overview

Machine learning is the core technique behind data science in general and todays artificial intelligence. Revisit [](warm-up:data-science) for details. There are three major types of machine learning:
* supervised learning,
* unsupervised learning,
* reinforcement learning.

**Supervised learning** is also known as *learning by examples*. The task is to predict properties of input data. For instance, input data could be pet images and the computer shall decide whether there is a dog or a cat in the image. Predicting values of a discrete variable is called *classification*. If we want to predict continuous variables, then it's a *regression problem*. To *train* a supervised learning method, we need lots of training examples, that is, input data with corresponding outputs. The computer then gathers knowledge from the examples and tries to generalize that knowledge to input data not contained in the training data set.

**Unsupervised learning** tries to find structures in large data sets without the need for examples. It's mostly concerned with *clustering*. Given a data set, find subsets (clusters) of data points with common properties. In some sense it's also about classification, but with classes not known in advance. A typical example is the identification of different customer types in e-commerce. Unsupervised learning also covers techniques for anomaly detection and generative learning (create new data with same properties like exisiting data).

**Reinforcement learning** is learning by *trial and error*. Thus, it's very similar to a child's learning process. Again, no examples of how to do it right are required. The *agent* (the learning algorithm) can choose between several actions. Depending on the outcome of it's actions, the agent changes its behavior until the correct outcome is observed. Examples are mobile robots finding their way through a building, but also artificial intelligences playing classical board games (see [Google's AlphaGo Zero](https://en.wikipedia.org/wiki/AlphaGo_Zero) for a prominent example).

There exist less prominent subclasses of machine learning, like **semi-supervised learning**, mixing techniques from supervised learning and unsupervised learning.

Today's most advanced artificial intelligence products like [ChatGPT](https://en.wikipedia.org/wiki/ChatGPT) or [Stable Diffusion](https://en.wikipedia.org/wiki/Stable_Diffusion) use a complex mix of many different techniques. To understand such complex products we have to understand the basic methods and algorithms first. Only then we may go on designing more complex systems.
