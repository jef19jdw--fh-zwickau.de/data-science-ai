---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"tags": [], "user_expressions": []}

(projects:cart-pole:environment)=
# The Cart Pole Environment

In this project we learn how to use `gymnasium`'s [Cart Pole](https://gymnasium.farama.org/environments/classic_control/cart_pole/). The cart pole environment simulates a pole mounted on top of a car. If the car moves (and even if it does not move), the pole most likely will fall down. But if we control the car's movements in the correct way, then we might be able to balance the pole.

**Task:** Read about the cart pole environment. Describe the set of states and the set of actions and the set of rewards. When does an episode end?

**Solution:**

```{code-cell} ipython3
:tags: []

# your answer
```

+++ {"tags": [], "user_expressions": []}

**Task:** Create a cart pole environment object and run one episode by choosing actions randomly. Display episode length. Don't try to render the environment. Rendering will be discussed below.

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"tags": [], "user_expressions": []}

Rendering via `Env.render()` does not work in Jupyter. But we may record a video and then show the video in the notebook. This workflow requires [`pygame`](https://www.pygame.org) and [`moviepy`](https://zulko.github.io/moviepy/) to be installed.

**Task:**  Have a look at Gymnasium doc's [Recording Agents](https://gymnasium.farama.org/main/introduction/record_agent/) and at [`IPython.display.Video`](https://ipython.readthedocs.io/en/stable/api/generated/IPython.display.html#IPython.display.Video). Then record one episode and show the video in the notebook.

**Solution:**

```{code-cell} ipython3
:tags: []

# your solution
```
