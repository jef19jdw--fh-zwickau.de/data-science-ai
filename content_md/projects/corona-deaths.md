---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:corona-deaths)=
# Corona Deaths

In this project we collect and/or compute death rates before and during the Corona pandemic in Germany. You should read [](managing-data:pandas:dates-times) before you start.

## Get some Data

We would like to have *monthly* death rates for an as long as possible period of time including very recent data.

**Task:** Download relevant data from [Federal Statistical Office (Statistisches Bundesamt)](https://www.destatis.de) in CSV format:
* [Destatis, table 12613-0006](https://www-genesis.destatis.de/datenbank/online/table/12613-0006)
* [Destatis, table 12411-0001](https://www-genesis.destatis.de/datenbank/online/table/12411-0001)
* [Destatis, Sonderreihe mit Beiträgen für das Gebiet der ehemaligen DDR, Heft 3](https://www.statistischebibliothek.de/mir/servlets/MCRFileNodeServlet/DEMonografie_derivate_00000961/Heft_3.pdf)
* [Destatis, table 12411-0020](https://www-genesis.destatis.de/datenbank/online/table/12411-0020)

For each file write a short note on its content.

**Solution:**

```{code-cell} ipython3
# your notes
```

**Task:** Use your favorit spreadsheet tool to compile following CSV files from the downloaded files:
* `inhabitants-yearly.csv` with columns `year`, `FRG` (inhabitants FRG), `GDR` (inhabitants GDR, 0 from 1990 on)
* `inhabitants-quarterly.csv` with colums `date`, `inhabitants`
* `deaths-monthly.csv` with columns `year`, `months` (numeric 1...12), `men`, `women`

## Load Data

We want to use dates as index for data frames. Numbers of inhabitants are related to precise timestamps (end of year or quarter). Numbers of deaths are related to periods (month).

**Task:** Read in the three CSV files. Use `DatetimeIndex` and `PeriodIndex` for data frames and series. In the end you should have two series:
* `inhabitants` with index `date` (timestamp of last day in year or quarter),
* `deaths` with index `date` (monthly period aligned at last day of month).

**Solution:**

```{code-cell} ipython3
# your solution
```

## Death Rates

For calculating monthly death rates we have to get the number of inhabitants on a monthly basis, i.e., the mean number of inhabitants per month. If we would have daily values for the number of inhabitants we could simply calculate the mean. But resolution is much coarser. Thus, we have to use (linear) interpolation. A good replacement for the monthly mean is the (interpolated) value at the 15th of the month.

**Task:** Use resampling to get interpolated number of inhabitants at the 15th of each month. From these values construct a series with period index (in analogy to the `deaths` series' index). Hint: instead of (integer) index based linear interpolation you may want to use timestamp based interpolation (see docs).

```{code-cell} ipython3
# your solution
```

**Task:** Calculate monthly death rates and plot results with `Series.plot()`.

```{code-cell} ipython3
# your solution
```
