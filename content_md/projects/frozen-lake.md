---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:frozen-lake)=
# Frozen Lake

[Frozen Lake](https://gymnasium.farama.org/environments/toy_text/frozen_lake/) is a simple grid world simulator contained in [Gymnasium](https://gymnasium.farama.org/), an open source project for standardizing environment simulators.

```{tableofcontents}
```
