---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:forged-banknotes)=
# Forged Banknotes

In this series of projects we apply several machine learning concepts and methods to automatically identify forged banknotes.

```{tableofcontents}
```
