---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:cart-pole)=
# Cart Pole

[Cart Pole](https://gymnasium.farama.org/environments/classic_control/cart_pole/) is a physics simulator contained in [Gymnasium](https://gymnasium.farama.org/), an open source project for standardizing environment simulators. It simulates movement of a pole mounted on top of a moving car.

```{tableofcontents}
```
