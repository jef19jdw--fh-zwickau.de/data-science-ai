---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"user_expressions": []}

(projects:blogs:author-classification-test)=
# Blog Author Classification (Test)

We want to write a script which takes a list of URLs to blog posts and yields predictions for gender, age and industry of the blog author. For this purpose we have to load our trained models from project [](projects:blogs:author-classification-training) and we have to apply all the necessary preprocessing steps to the downloaded posts.

## Getting some Blog Posts

**Task:** Collect URLs of posts of some blog in a list. Take a blog for which you know gender, age and industry of the author. So we will see whether our models yield good predictions.

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

**Task:** Download all webpages in the list. Strip HTML tags with Beautiful Soup's [`get_text`](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#get-text) and join all posts to one string.

**Solution:**

```{code-cell} ipython3
:tags: []

# your solution
```

+++ {"user_expressions": []}

## Preprocessing

**Task:** Repeat all preprocessing steps from part 1 of the text processing chapter (remove punctuation, tokenize, lemmatize).

**Solution:**

```{code-cell} ipython3
:tags: []

# your solution
```

+++ {"user_expressions": []}

**Task:** Load the three label lists and the vectorizer.


**Solution:**

```{code-cell} ipython3
:tags: []

# your solution
```

+++ {"user_expressions": []}

**Task:** Vectorize the lemmatized text.

**Solution:**

```{code-cell} ipython3
:tags: []

# your solution
```

+++ {"user_expressions": []}

## Prediction

**Task:** Load the three saved SVC models.

**Solution:**

```{code-cell} ipython3
:tags: []

# your solution
```

+++ {"user_expressions": []}

**Task:** Predict the blog author's gender, age and industry. Provide the result in human readable form.

**Solution:**

```{code-cell} ipython3
:tags: []

# your solution
```
