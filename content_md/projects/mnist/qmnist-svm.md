---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"user_expressions": []}

(projects:mnist:qmnist-svm)=
# SVM for QMNIST

We want to classify QMNIST images with an SVM.

## Load Data Set

**Task:** Load QMNIST training and test data to NumPy arrays as required by Scikit-Learn. Center bounding boxes of all images and crop images to 20x20 pixels.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Linear SVM

The data set is relatively small (60000 training samples) compared to the space dimension (400). So there is some chance that classes can be separated by hyperplanes instead of nonlinear hypersurfaces.

**Task:** Train and evaluate a linear SVM on the QMNIST training samples with Scikit-Learn's [`LinearSVC`](https://scikit-learn.org/stable/modules/generated/sklearn.svm.LinearSVC.html) and again with [`SGDClassifier`](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.SGDClassifier.html).

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

## Nonlinear SVM

To reduce computation time we should apply PCA to our data set. Thus, inner products are computed in, say, $\mathbb{R}^{15}$ instead of in $\mathbb{R}^{400}$.

**Task:** Apply PCA transform with 15 components to the data. Then train a kernel SVM with rbf kernel.

**Solution:**

```{code-cell} ipython3
# your solution
```

**Task:** Visualize some support vectors (original images, not PCA coefficients).

**Solution:**

```{code-cell} ipython3
# your solution
```
