---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"user_expressions": [], "tags": []}

(projects:mnist:generate-digits)=
# Generating Handwritten Digits

Gaussian mixture models are generative models, that is, we may use a trained model to generate new samples looking similar to the training samples. If we train a Gaussian mixture model on QMNIST, then it should be possible to generate images of handwritten digits, which are not exactly equal to one of the QMNIST images.

## Loading and Preprocessing Data

**Task:** Load QMNIST training images, labels and writer IDs. Center bounding boxes and crop images to 20x20 pixels.

**Solution:**

```{code-cell} ipython3
# your solution
```

**Task:** Use PCA with 50 components to reduce dimensionality of the data space. Else training the model will take too long. Show some image together with its projection onto the 50 dimensional subspace constructed by PCA.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Training the Model

**Task:** Fit a Gaussian mixture model to the data. What's a sensible number of clusters.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Generating Images

**Task:** Use [GaussianMixture.sample](https://scikit-learn.org/stable/modules/generated/sklearn.mixture.GaussianMixture.html#sklearn.mixture.GaussianMixture.sample) to generate 100 new images showing handwritten digits. Show all images.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Gaussian Mixture Unmixed

If we already know the clusters and only want to generate new images, we may fit a Gaussian distribution to each cluster manually. That is, for each cluster we compute mean vector and covariance matrix.

**Task:** Find the writer with the highest number of images available. Show all images for this writer.

**Solution:**

```{code-cell} ipython3
# your solution
```

**Task:** Get means and covariances for the ten classes of digitis written by the writer.

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

**Task:** Use NumPy's random number generator to generate 10 new images per class.

**Solution:**

```{code-cell} ipython3
# your solution
```
