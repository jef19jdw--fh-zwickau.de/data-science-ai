---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"tags": [], "user_expressions": []}

(projects:mnist:qmnist-cnn)=
# CNN for QMNIST

In this project we train a CNN for handwritten digit recognition. Read [](supervised:ann:cnn) and [](supervised:ann:cnn-keras) before you start.

You may reuse code from [](projects:mnist:load-qmnist) and [](exercises:managing-data:image-processing).

**Task:** Solve the QMNIST digit recognition task with a CNN. Try to get 99 percent correct classifications on the test set (without using the test set for training, of course). Remember that convolutions give more weight to image center. Thus, do not crop QMNIST images. Use 28x28 images with centered bounding boxes. Save your model to a file.

**Solution:**

```{code-cell} ipython3
# your solution
```
