---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"user_expressions": []}

(projects:mnist:qmnist-cnn-analysis)=
# CNN Analysis for QMNIST

In this project we analyse the CNN trained in [](projects:mnist:qmnist-cnn).

You may reuse code from [](supervised:ann:cnn-learn).

**Task:** Load the CNN trained and saved in [](projects:mnist:qmnist-cnn).

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

**Task:** Visualize filters for the first convolutional layer in your CNN and try to interpret some of them (what features do they detect?).

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

**Task:** Take an image correctly classified to show a zero and modify it slightly to make you CNN 'think' that it's a five although human eye clearly sees a zero.

**Solution:**

```{code-cell} ipython3
# your solution
```
