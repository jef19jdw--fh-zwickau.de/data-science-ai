---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"user_expressions": []}

(projects:mnist:qmnist-feature-reduction)=
# QMNIST Feature Reduction

In this project we apply PCA to QMNIST data. Read about [](supervised:general:feature-reduction) before you start.

## Preprocessing

**Task:** Use the Python module developed in [](projects:mnist:load-qmnist) to load the first 5000 QMNIST training images.

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

**Task:** Use the above module to apply the following preprocessing steps from [](exercises:managing-data:image-processing)
* auto crop,
* center in 20x20 image

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"tags": [], "user_expressions": []}

## Relevant Components

**Task:** Perform a full PCA (no feature reduction) and plot standard deviations (square roots of variances) for all principal components.

**Solution:**

```{code-cell} ipython3
:tags: []

# your solution
```

+++ {"tags": [], "user_expressions": []}

**Task:** Show the data set's mean and the first 100 principal components as images. Scale principal components by corresponding standard deviation and use same color map for all images.

**Solution:**

```{code-cell} ipython3
:tags: []

# your solution
```

+++ {"user_expressions": []}

**Task:** Transform all images by PCA with 15 components. For one images plot original and transformed image side by side (you may use [`PCA.inverse_transform`](https://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html#sklearn.decomposition.PCA.inverse_transform) or implement calculations manually).

**Solution:**

```{code-cell} ipython3
:tags: []

# your solution
```

+++ {"user_expressions": []}

## Visualization

We may use feature reduction techniques to visualize high-dimensional data sets.

**Task:** Use PCA to reduce the data set to 3 features. Plot the now 3d data set. Color classes differently.

```{code-cell} ipython3
# your solution
```

**Task:** Use PCA to reduce the data set to 4 features. Make pair plots for combinations of two features (use same coloring as above).

```{code-cell} ipython3
# your solution
```
