---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"tags": [], "user_expressions": []}

(projects:mnist:qmnist-tsne)=
# t-SNE for QMNIST

Many projects ago we used PCA to get 2d and 3d visualization of QMNIST training images. Now we have more powerful dimensionality reduction techniques at hand. Let's try t-SNE.

**Task:** Load QMNIST training images, center bounding boxes, and crop images to 20x20.

**Solution:**

```{code-cell} ipython3
# your solution
```

**Task:** Use t-SNE to get a 2d visualization of QMNIST images.

**Solution:**

```{code-cell} ipython3
# your solution
```

**Task:** Load QMNIST training labels and color the 2d plot according to the labels.

**Solution:**

```{code-cell} ipython3
# your solution
```
