---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"user_expressions": []}

(projects:mnist:qmnist-pca-ann)=
# PCA and ANN for QMNIST

In this project we train a small ANN for handwritten digit recognition. Using PCA for preprocessing allows to reduce the ANN's size compared to the ANN in [](supervised:ann:ann-keras).

You may reuse code from [](projects:mnist:load-qmnist) and [](exercises:managing-data:image-processing).

**Task:** Solve the QMNIST digit recognition task with a layered feedforward ANN and prior PCA feature reduction to 15 features. Try to get at least 90 percent correct classifications on the test set (without using the test set for training, of course).

**Solution:**

```{code-cell} ipython3
# your solution
```
