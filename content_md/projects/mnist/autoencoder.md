---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"user_expressions": [], "tags": []}

(projects:mnist:autoencoder)=
# Autoencoder for QMNIST

When clustering QMNIST images with Gaussian mixtures we had to apply PCA to get acceptable computation times. Now we have a nonlinear dimensionality reduction technique at hand which might yield lower quality loss than PCA.

**Task:** Load QMNIST training images (without further preprocessing).

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"tags": [], "user_expressions": []}

## Autoencoder Training

We use the following encoder model:

```{code-cell} ipython3
# disable GPU if TensorFlow with GPU causes problems
#import os
#os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

import tensorflow.keras as keras
```

```{code-cell} ipython3
encoder = keras.Sequential(name='encoder')

encoder.add(keras.Input(shape=(28, 28, 1)))
encoder.add(keras.layers.Conv2D(4, 3, activation='relu', name='conv1'))
encoder.add(keras.layers.Conv2D(4, 3, activation='relu', name='conv2'))
encoder.add(keras.layers.MaxPooling2D(name='pool1'))
encoder.add(keras.layers.Conv2D(8, 3, activation='relu', name='conv3'))
encoder.add(keras.layers.Conv2D(8, 3, activation='relu', name='conv4'))
encoder.add(keras.layers.MaxPooling2D(name='pool2'))
encoder.add(keras.layers.Conv2D(12, 3, activation='relu', name='conv5'))

encoder.summary()
```

+++ {"user_expressions": []}

**Task:** Create a symmetric decoder and the autoencoder model.

**Solution:**

```{code-cell} ipython3
# your solution
```

**Task:** Train the autoencoder.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Evaluation

Code space has 48 dimensions. So we could compare results to PCA with 48 components to see whether the autoencoder yields better results than PCA.

**Task:** Transform all images with PCA with 48 components. Visualize for some samples original image, autoencoder reconstruction and PCA transformed image. Compute RMSE for autoencoder and PCA results.

**Solution:**

```{code-cell} ipython3
# your solution
```

Visualizing codes provides some information on how well clusters can be separated by looking at the codes.

**Task:** Load training labels and visualize 300 codes for each digit.

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

## Anomaly Detection

Looking at RMSE for each image we may identify images not similar to most other images. Removing such outliers from the data set could increase training success for supervised learning methods.

**Task:** Calculate RMSE for each image (autoencoder only) and plot images with highest RMSE.

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

## Generating New Images

**Task:** Generate images from random codes.

**Solution:**

```{code-cell} ipython3
# your solution
```

**Task:** Generate images from random perturbations of a training image's code.

**Solution:**

```{code-cell} ipython3
# your solution
```

**Task:** Generate a transition from one training image to another training image
* by interpolating images directly and
* by interpolating their codes.

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

**Task:** Create an animation of the code based transition.

**Solution:**

```{code-cell} ipython3
# your solution
```
