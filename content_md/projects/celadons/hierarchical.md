---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:celadons:hierarchical)=
# Hierarchical Clustering

We want to find clusters in a set of celadons (early porcelains). A data set with material properties of a number of celadons found in China is available from [UCI Machine Learning Repository](https://archive.ics.uci.edu/ml/datasets/Chemical+Composition+of+Ceramic+Samples). The data set originates from research work published in [Data-driven research on chemical features of Jingdezhen and Longquan celadon by energy dispersive X-ray fluorescence](https://www.sciencedirect.com/science/article/pii/S0272884215023135) by Ziyang He, Maolin Zhang, Haozhe Zhang. A [free preprint PDF file](https://arxiv.org/pdf/1511.07825.pdf) is available, too.

**Task:** Read (at least) the last paragraph of section 1 and section 2 of the afore mentioned preprint. How many celadon sample do you expect in the data set after reading?

**Solution:**

```{code-cell} ipython3
# your answer
```

## Understanding the data

Data comes as a CSV file.

**Task:** Load the data to a data frame. Why are there so many samples?

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

**Task:** Create a NumPy array holding the data with one row per sample and one column per feature.

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

**Task:** Create a list of sample names.

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

## Preprocessing

Units of measurement are weight per cent for body features and parts per million for glaze features. Since all features are equally important for finding similar celadons we should standardize features independently. Maybe the assumption of equal importance is not correct, but without further domain knowledge we cannot do better.

**Task:** Standardize all features.

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

## Hierarchical Clustering

**Task:** Plot a dendrogram and determine a sensible number of clusters.

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

**Task:** Cluster data into the chosen number of clusters.

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

## $k$-Means Clustering

**Task:** Use $k$-means for clustering. Determine a good $k$.

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

**Task:** Compare results from hierarchical and $k$-means clustering.

**Solution:**

```{code-cell} ipython3
# your solution
```
