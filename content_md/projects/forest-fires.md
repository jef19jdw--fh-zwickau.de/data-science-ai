---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"tags": [], "user_expressions": []}

(projects:forest-fires)=
# Forest Fires

In this project we want to obtain some insight into different types of forest fires in a Portuguese national park. Data is available at the [UCI Machine Learning Repository](http://archive.ics.uci.edu/dataset/162/forest+fires). It covers forest fires from January 2000 till December 2003 and provides several numerical features of soil moisture and weather.

**Task:** Get the data and have a look at [Data Mining Approach to Predict Forest Firesusing Meteorological Data](https://core.ac.uk/download/pdf/55609027.pdf). What are FFMC, DMC, DC, ISI?

**Solution:**

```{code-cell} ipython3
# your answer
```

**Task:** Load the data. Remove outliers. Scale data where necessary. Create a NumPy array containing for all samples FFMC, DMC, DC, ISI.

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": [], "tags": []}

**Task:** Visualize data with 2d Isomap. Interpret the 2d embedding. Use visualizations of all the other features (not only FFMC, DMC, DC, ISI). Can you identify clusters or any other useful structure? Describe different types of forest fires.

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

**Task:** Use PCA to project data into 2 dimensions. Can you see different fire types here, too?

**Solution:**

```{code-cell} ipython3
# your solution
```
