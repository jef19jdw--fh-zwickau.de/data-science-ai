---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:weather:weather-animation)=
# Weather Animation

Based on the data collected in the [](projects:weather:climate-change) project here we want to create an animation showing air temperature (or other measurements) over a period of time for all weather stations on a map of Germany.

Before you start read [](visualization:matplotlib:animations).

Following features should be implemented:
* simple (non-interacitve) map of Germany showing at least the borders,
* color-coded temperatures at weather stations (scatter plot),
* one animation frame per day,
* time stamp of currently shown data in each frame.

## Prepare Data

For each frame we need following data:
* time stamp string to show during animation
* coordinates for all relevant weather stations,
* temperature for all stations.

Use data collected in [](projects:weather:climate-change).

**Task:** Decide for a time period of several months to show in the animation. Collect required data. Create a list with one item per frame. Each item shall be a dictionary with following keys:
* `'text'` (time stamp string for frame),
* `'x'` (NumPy array with longitudes of all stations to show in the frame),
* `'y'` (NumPy array with latitudes of all stations to show in the frame),
* `'val'` (NumPy array with temperature for all stations).

Save the list with `pickle` to a file.

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

## Visualize Data

**Task:** Load the data saved above and create a map of Germany with Cartopy. Plot with [`GeoAxes.scatter`](https://scitools.org.uk/cartopy/docs/latest/reference/generated/cartopy.mpl.geoaxes.GeoAxes.html#cartopy.mpl.geoaxes.GeoAxes.scatter) and use Matplotlib's `animation` module to animate the arrows. Don't forget to show time stamps in the animation. Save the animation to an MP4 video file.

```{hint}
To avoid troubles with Matplotlib consider the following:
* At the moment Matplotlib does not support blitting when saving animations, but redrawing the whole map for every frame is too slow. Thus, when updating the drawing remove artists not needed anymore by calling the artists [`remove`](https://matplotlib.org/stable/api/_as_gen/matplotlib.artist.Artist.remove.html) function and then create new artists. This way the background (map) is kept. It's kind of manual blitting.
* If your animation looks like the scatter dots are covered by parts of the map, then pass `zorder=100` (or some other high value) to `scatter`. This tells Matplotlib to draw the scatter object on top of all other objects (if all other objects have lower z-order).
```

**Solution:**

```{code-cell} ipython3
# your solution
```
