---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:house-prices:gui)=
# House Prices GUI

The aim of this project is to create a graphical user interface (GUI) for house price predictions based on the model trained in [](supervised:linear-regression:example2).

## The Model

In [](supervised:linear-regression:example2) we saw that not all features or of importance for price prediction. Thus, here we restrict our attention to the important features only.

**Task:** Train a Ridge regression model for house price prediction based on following features:
* region's average land prices and income,
* lot size and living space,
* build and renovation year
* number of rooms and bath rooms,
* building type (duplex, villa, other).

Find optimal hyperparameters.

**Solution:**

```{code-cell} ipython3
# your solution
```

**Task:** Train the model with optimal hyperparameters on the full data set. Save the trained model to a file (use `pickle` module).

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

## GUI

We use the [`ipywidgets`](https://ipywidgets.readthedocs.io/en/stable/) module to create a GUI in Jupyter Lab. Have a look at the following documentation pages:
* [Simple Widget Introduction](https://ipywidgets.readthedocs.io/en/stable/examples/Widget%20Basics.html)
* [Text](https://ipywidgets.readthedocs.io/en/stable/examples/Widget%20List.html#text)
* [Combobox](https://ipywidgets.readthedocs.io/en/stable/examples/Widget%20List.html#combobox)
* [RadioButtons](https://ipywidgets.readthedocs.io/en/stable/examples/Widget%20List.html#radiobuttons)
* [Button](https://ipywidgets.readthedocs.io/en/stable/examples/Widget%20List.html#button)
* [Output](https://ipywidgets.readthedocs.io/en/stable/examples/Widget%20List.html#output)

**Task:** Load the model saved above. Create all required input fields and a button which starts the prediction process. After clicking the button the user should see the predicted house price (in an output widget). To simplify input of the region's land prices and income you could use a combobox for selecting a region.

**Solution:**

```{code-cell} ipython3
# your solution
```
