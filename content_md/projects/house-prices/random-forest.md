---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:house-prices:random-forest)=
# A Random Forest for House Prices

In this project we solve the house price prediction problem from [](supervised:linear-regression:example2) with a random forest. Read [](supervised:ensemble-methods:bagging) before you start.

**Task:** Use the extended and preprocessed German housing data set to predict house prices. Train a random forest regressor with Scikit-Learn. Try to get similar or better prediction quality as for Ridge regression.

```{code-cell} ipython3
# your solution
```

**Task:** Show feature importances based on `RandomForestRegressor.feature_importances_`.

```{code-cell} ipython3
# your solution
```

**Task:** Visualize one of the trees in the forest. Only show the first few depth levels.

```{code-cell} ipython3
# your solution
```
