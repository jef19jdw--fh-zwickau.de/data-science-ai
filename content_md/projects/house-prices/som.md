---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"tags": [], "user_expressions": []}

(projects:house-prices:som)=
# House Prices SOM

We already used regression techniques to predict house prices. Now it's time to visualize the underlying data set using SOMs.

**Task:** Load the preprocessed German housing data set generated in [](supervised:linear-regression:example1). Convert categorical features to numeric features. For one hot encoding use as many code features as there are categories (do not drop one of them). Why?

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

**Task:** Create a list of feature names, which we will use below to label plots. Create a NumPy array holding the data set.

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

**Task:** Train a SOM on the data. Don't forget to standardize data.

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

**Task:** Plot the U-matrix.

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

**Task:** Visualize each (high dimensional) feature in 2d. Arrange all plots in a 4 by 6 grid.

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

**Task:** Create a new sample and get its position (best matching unit) in the SOM. Show corresponding activation map and mark the best matching unit in the map.

```{code-cell} ipython3
# your solution
```
