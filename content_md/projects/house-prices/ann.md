---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:house-prices:ann)=
# House Prices ANN

In this project we solve the house price prediction problem from [](supervised:linear-regression:example2) with an ANN. Read [](supervised:ann:basics) and [](supervised:ann:training) before your start.

## The ANN

In [](supervised:linear-regression:example2) we saw that not all features or of importance for price prediction. Thus, here we restrict our attention to the important features only.

**Task:** Train an ANN for house price prediction based on following features:
* region's average land prices and income,
* lot size and living space,
* build and renovation year
* number of rooms and bath rooms,
* building type (duplex, villa, other).

Apply regularization (parameter choice via Scikit-Learn's hyperparameter optimization methods) and try different ANN sizes (number of layers, number of neurons per layer) as well as different activation functions.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Some Theory

**Task:** Think about implications of very small batch sizes in case of noisy data. Are small batches good or bad here?

**Solution:**

```{code-cell} ipython3
# your notes
```
