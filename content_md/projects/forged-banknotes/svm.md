---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:forged-banknotes:svm)=
# Support-Vector Machine

We want to train a kernel SVM for banknote authentication. Without kernel the decision surface will be a hyperplane more or less identical to the hyperplane obtained from logistic regression. Separation by a hyperplane works, but for both classes there are samples very close to the hyperplane. Looking for nonlinear separation should yield fewer ambiguous samples.

## Load Data Set

**Task:** Load the banknotes data set (cf. [](projects:forged-banknotes:knn)). Drop the entropy column.

```{code-cell} ipython3
# your solution
```

## SVM

**Task:** Use Scikit-Learns's [`SVC`](https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html) to create a model for banknote authentication. Try different kernels (RBF, polynomials of different degrees). Use hyperparameter optimization for choosing the parameter `C`.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Decision Surface

**Task:** Train a second model based on variance and skewness only. Plot the decision surface (the surface separating the classes).Highlight all support vectors.

**Solution:**

```{code-cell} ipython3
# your solution
```
