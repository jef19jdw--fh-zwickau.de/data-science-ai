---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"user_expressions": []}

(projects:forged-banknotes:knn)=
# Detecting Forgery with k-NN

Banknotes have lots of security features, some well known (see [Deutsche Bundesbank](https://www.bundesbank.de/de/aufgaben/bargeld/falschgeld/falschgelderkennung/50-euro-europa-serie)) and some less known like the [EURion constellation](https://en.wikipedia.org/wiki/EURion_constellation). Machine learning methods allow to investigate features not designed for human vision or simple algorithmic evaluation.

One approach is to have a closer look at the printing quality. Banknotes are printed using a technique know as [intaglio](https://en.wikipedia.org/wiki/Intaglio_(printmaking)). That technique produces extremely sharp edges if [steel plates](https://en.wikipedia.org/wiki/Steel_engraving) are used and cannot be realized with off-the-shelf machines. Researchers from [Institut für industrielle Informationstechnik](https://www.init-owl.de/) at [Technischen Hochschule Ostwestfalen-Lippe](https://www.th-owl.de) created a data set for training banknote authentication systems based on scanned images of banknotes.

## The Data Set

The [data set](https://archive-beta.ics.uci.edu/dataset/267/banknote+authentication) is available from [UCI Machine Learning Repository](https://archive-beta.ics.uci.edu/). It comes as a simple CSV file without header.

**Task:** Download the data set and read it into a Pandas data frame. Get column names from UCI webpage of the data set. Adjust data types if necessary. Look at [this blog post by James D. McCaffrey](https://jamesmccaffrey.wordpress.com/2020/08/18/in-the-banknote-authentication-dataset-class-0-is-genuine-authentic/) to find out how to interpret class labels.

**Solution:**

```{code-cell} ipython3
:tags: []

# your solution
```

+++ {"user_expressions": []}

**Task:** Determine class sizes.

```{code-cell} ipython3
:tags: []

# your solution
```

+++ {"user_expressions": []}

The data set does not contain scanned images, but some statistical information about the histograms.

**Task:** Read sections 1 and 2 of [Banknote Authentication](https://www.researchgate.net/profile/Eugen-Gillich-2/publication/266673146_Banknote_Authentication/links/5436b8140cf2643ab9887bca/Banknote-Authentication.pdf) to get a rough idea of what the features in the data set express. Note that wavelet transforms are similar to Fourier transforms.

```{code-cell} ipython3
# your notes
```

+++ {"user_expressions": []}

## Visualization

**Task:** Create a pairplot of the 4 features with different colors for the two classes.

```{code-cell} ipython3
:tags: []

# your solution
```

+++ {"user_expressions": []}

**Task:** For each combination of 3 features create a 3d scatter plot (again different colors for classes).

```{code-cell} ipython3
:tags: []

# your solution
```

+++ {"user_expressions": []}

```{note}
From the 3d plots we see that variance, skewness, curtosis should suffice to separate forged from genuine banknotes. From this point of view the entropy feature can be neglected. Further, the data set description does not contain information on how the entropy was calculated. If we want to use a model trained on the data set to classify new samples, we do not know how to derive model inputs from scanned images. This is a second reason to drop the entropy feature.
```

## k-NN Predictions

**Task:**  Create model inputs and outputs in Scikit-Learn format. That is, create a NumPy array `X` with one row per sample and one column per feature (do not include entropy) and a one-dimensional NumPy array with outputs for all samples (use integers, no booleans).

```{code-cell} ipython3
:tags: []

# your solution
```

+++ {"user_expressions": []}

**Task:** Create a k-NN classifier with Scikit-Learn. For the moment we do not consider hyperparameter optimization. Choose $k=11$ and no weighting. Test set size should be 30 per cent. Compute accuracy on test and training sets. Get the number of missclassified samples in the test set.

**Solution:**

```{code-cell} ipython3
:tags: []

# your solution
```
