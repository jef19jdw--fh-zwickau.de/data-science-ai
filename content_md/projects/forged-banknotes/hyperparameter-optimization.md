---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:forged-banknotes:hyperparameter-optimization)=
# Hyperparameter Optimization

In this project we revisit [](projects:forged-banknotes:knn) and add hyperparameter optimization.

## Load Data Set

**Task:** Load the banknotes data set (cf. [](projects:forged-banknotes:knn)). Drop the entropy column.

```{code-cell} ipython3
# your solution
```

## Grid Search with Cross Validation

**Task:** Create a $k$-NN model with Scikit-Learn's [`KNeighborsClassifier`](https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html). Use hyperparameter optimization based on accuracy for choosing $k$ and to find appropriate weights (uniform or inverse distance). Evaluate the model on a test set. Print optimal parameters and accuracy on the test set.

**Solution:**

```{code-cell} ipython3
:tags: []

# your solution
```

## Decision Surface

**Task:** Train a second model based on variance and skewness only. Plot the decision surface (the surface separating the classes) with Matplotlib's [`contour`](https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.contour.html) or [`contourf`](https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.contourf.html).

**Solution:**

```{code-cell} ipython3
:tags: []

# your solution
```
