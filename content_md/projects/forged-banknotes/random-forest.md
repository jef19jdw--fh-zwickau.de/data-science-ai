---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:forged-banknotes:random-forest)=
# Random Forest

In this project we solve the banknote classification task with a random forest.

## Load Data Set

**Task:** Load the banknotes data set (cf. [](projects:forged-banknotes:knn)). Drop the entropy column.

```{code-cell} ipython3
# your solution
```

## Random Forest

**Task:** Train and evaluate a random forest with 50 trees. Use fixed depth to restrict tree sizes. Choose the depth by hyperparameter optimization.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Decision Surface

**Task:** Train a second forest based on variance and skewness only. Plot the decision surface (the surface separating the classes) with Matplotlib's [`contour`](https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.contour.html) or [`contourf`](https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.contourf.html).

**Solution:**

```{code-cell} ipython3
# your solution
```
