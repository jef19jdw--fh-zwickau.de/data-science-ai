---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:forged-banknotes:tree)=
# Decision Tree

In this project we solve the banknote classification task with a decision tree.

## Load Data Set

**Task:** Load the banknotes data set (cf. [](projects:forged-banknotes:knn)). Drop the entropy column.

```{code-cell} ipython3
# your solution
```

## Decision Tree

**Task:** Train and evaluate a decision tree for banknote classification. Use cost-complexity pruning with corresponding parameter chosen by hyperparameter optimization.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Decision Surface

**Task:** Train a second model based on variance and skewness only. Plot the decision surface (the surface separating the classes) with Matplotlib's [`contour`](https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.contour.html) or [`contourf`](https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.contourf.html).

**Solution:**

```{code-cell} ipython3
# your solution
```
