---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:forged-banknotes:naive-bayes)=
# Naive Bayes Classification

## Load Data Set

**Task:** Load the banknotes data set (cf. [](projects:forged-banknotes:knn)). Drop the entropy column.

```{code-cell} ipython3
# your solution
```

## Naive Bayes

**Task:** Use Scikit-Learns's [`GaussianNB`](https://scikit-learn.org/stable/modules/generated/sklearn.naive_bayes.GaussianNB.html) to create a model for banknote authentication.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Decision Surface and Distributions

**Task:** Train a second model which only uses variance and skewness. Plot the training data set and the decision curve as well as the two Gaussian densities estimated from the training samples by the naive Bayes classifier.

**Solution:**

```{code-cell} ipython3
# your solution
```
