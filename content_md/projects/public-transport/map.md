---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:public-transport:map)=
# Interactive Map

In this project we visualize the results of the [](projects:public-transport:connections) project on an interactive map. Have a look at [](visualization:folium) before you start.

## Prepare Data

**Task:** Load the CSV file created in the [](projects:public-transport:connections) project to a Pandas data frame.

**Solution:**

```{code-cell} ipython3
# your solution
```

**Task:** Create a data frame containing only one stop per group. In each group choose the stop with shortest travel time. Drop groups without any stops connected to the arrival stop.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Create Map

**Task:** Create Folium map centered at the analyzed region with a marker (with stop name in tooltip) at the arrival stop.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Add Departure Stops

**Task:** For each stop add a marker to the map showing stop name and departure time in a tooltip. Show only one stop per stop group, the one with the lastest departure.

````{hint}
Use [`folium.plugins.FastMarkerCluster`](https://python-visualization.github.io/folium/plugins.html#folium.plugins.FastMarkerCluster) instead of [`folium.plugins.MarkerCluster`](https://python-visualization.github.io/folium/plugins.html#folium.plugins.MarkerCluster), because the latter may be very slow if you have many stops. Because `FastMarkerCluster` generates markers dynamically only for the currently visible area of the map, tooltips have to be generated dynamically, too. For tooltip generation pass the following JavaScript function as string to `FastMarkerClusters` `callback` argument:
```javascript
function (row) {
    var marker;
    marker = L.marker(new L.LatLng(row[0], row[1])).bindTooltip(row[2]);
    return marker;
};
```
The `data` argument then expects a list of tuples `(latitude, longitude, tooltip_text)` describing the markers.
````

**Solution:**

```{code-cell} ipython3
# your solution
```

**Task:** Marker clusters are colored depending on their size. We want to have constant color for all clusters. Thus, inject to following HTML snipped into your map:

```html
<style>
.marker-cluster-small div, .marker-cluster-medium div, .marker-cluster-large div {
    background-color: #0000ff80;
}
.marker-cluster-small, .marker-cluster-medium, .marker-cluster-large {
    background-color: #0000ff30;
}
</style>
```

**Solution:**

```{code-cell} ipython3
# your solution
```

## Color-Coded Distances

To visualize travel times we may color each point of the map depending on the distance to the next stop and on the next stop's departure time to the arrival stop. Color scheme is as follows:
* Circular areas with radius 1 kilometer around stops with connection to the arrival stop get colored. All other regions of the map remain uncolored.
* Color around a stop depends on departure time: green for late departure, yellow for medium departure time, red for early departure (continuous color scale).

One possible path to follow is:
1. Choose a rectangular region of interest on the map and divide it into a grid of rectangles (cells). Edge length should be about 100 meters.
2. For each stop identify the cell containing the stop.
3. For each cell get the latest departure time.
4. Interpret the grid of cells as 'image' and 'draw' a 1-kilometer disc of color 'departure time' around each cell. Initialize the image with some invalid value, then draw discs with increasing departure time (thus, late departures will overwrite early departures).
5. Add an [`ImageOverlay`](https://python-visualization.github.io/folium/modules.html#folium.raster_layers.ImageOverlay) to your map color coding the image of departure times.

**Task:** Implement the above steps or follow an alternative path for color-coding departure times.

**Solution:**

```{code-cell} ipython3
# your solution
```
