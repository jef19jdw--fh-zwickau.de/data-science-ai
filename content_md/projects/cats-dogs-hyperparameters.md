---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:cats-dogs-hyperparameters)=
# Hyperparameter Optimization for Cats and Dogs

In [](supervised:ann:cnn-keras) we trained a CNN for classifying images of cats and dogs. Using the knownledge from [](supervised:ann:ann-keras) abuot hyperparameter optimization we may improve prediction quality.

**Task:** Find a CNN for classifying cats and dogs using hyperparameter optimization for Keras. Do not use data augmentation or pre-trained ANNs. Try to get at least 85 percent classification accuracy.

**Solution:**

```{code-cell} ipython3
# your answer
```
