---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:house-prices)=
# House Prices

This series of projects extends the results obtained in [](supervised:linear-regression:example1) and [](supervised:linear-regression:example2).

```{tableofcontents}
```
