---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"user_expressions": [], "tags": []}

(projects:color-perception)=
# Color Perception

Human color perception is not as trivial as it may look at first glance. The number of independent colors recognizable by humans, and thus the dimension of the human color space, is not obvious. In the project we want to use MDS to get some insights into human color perception.

The data set we want to analyze is from 1954 and can be found in a table on page 2 of [Dimensions of Color Vision](https://doi.org/10.1080/00223980.1954.9712953). Corresponding CSV file ships with this project's notebook. It contains similarity scores for pairs of colors (light of different wave lengths).

**Task:** Read the following excerpt from Ekman's 1954 paper ('Table 1' is in the CSV file). How did Ekman obtaine the similarity scores?

```{figure} ekman.png
---
alt: excerpt of Ekman's 1954 paper
---
Data table in Ekman's 1954 paper. The opposite of 'big data'.
```

**Solution:**

```{code-cell} ipython3
# your notes
```

**Task:** Load the data set. Create a list of wave lengths (for labeling plots below) and a distance (!) matrix.

**Solution:**

```{code-cell} ipython3
# your solution
```

## 1d Embedding

Wave lengths are real numbers and, thus, contained in a one dimensional linear manifold. If human perception of color differences is proportional to wave length, then the color space should have a nice embedding into 1d space.

**Task:** Use metric MDS to get a 1d embedding of the data set.

**Solution:**

```{code-cell} ipython3
# your solution
```

**Task:** Get the distance matrix of the embedded data set. Visualize (dis)similarity of both distance matrices.

**Solution:**

```{code-cell} ipython3
# your solution
```

+++ {"user_expressions": []}

## 2d Embedding

**Task:** Repeat steps from above for a 2d embedding.

**Solution:**

```{code-cell} ipython3
# your solution
```

## 3d Embedding

**Task:** Repeat steps from above for a 3d embedding.

```{code-cell} ipython3
# your solution
```

## Higher Dimensions

**Task:** Use PCA to determine the number of dimensions needed to accurately model the space of human perceivable colors.

**Solution:**

```{code-cell} ipython3
# your solution
```
