---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(projects:celadons)=
# Chinese Celadons

This series of projects investigates the relations between different types of Chinese celadons (early porcelains).

```{tableofcontents}
```
