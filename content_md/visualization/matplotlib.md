---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(visualization:matplotlib)=
# Matplotlib

The Python standard library contains no modules for visualizing functions (plotting) and other types of data. But there is a de-facto standard: [Matplotlib](https://matplotlib.org/), providing lots of plot types and additional visualization features. It integrates well with [](managing-data:numpy) and is the Swiss army knife of visualization tools.

```{tableofcontents}
```

Related exercises:
* [](exercises:visualization:matplotlib-basics)
* [](exercises:visualization:advanced-matplotlib)


Related projects:
* [](projects:chemnitz-trees)
* [](projects:weather)
  * [](projects:weather:weather-animation)
