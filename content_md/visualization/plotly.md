---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"user_expressions": []}

(visualization:plotly)=
# Plotly

[Plotly](https://plotly.com/python/) is a relatively new JavaScript based plotting library for Python and [several other languages](https://plotly.com/graphing-libraries/). It's free and open source, but developed by Plotly Technology Inc providing commercial tools and services around Plotly. Plotly started as an online-only service (rendering done on a server), but now may be used offline without any data transfer to plotly.com.

Here we only cover 3d plots to have a good alternative to Matplotlib's limited 3d capabilities. But plotly allows for 2d plotting, too. A major advantage of Plotly is, that interactive Plotly plots can easily be integrated into websites.

The Plotly python package may be installed via `conda` from Plotly's channel:
```
conda install -c plotly plotly
```

Plotly integrates well with NumPy and Pandas.

```{code-cell} ipython3
:tags: []

import numpy as np
import pandas as pd
```

+++ {"user_expressions": []}

## Plotly Express

The [Plotly express](https://plotly.com/python/plotly-express) interface to Plotly provides commands for complex standard tasks:

```{code-cell} ipython3
:tags: []

import plotly.express as px

a = np.random.rand(100)
b = np.random.rand(100)
c = a * b
df = pd.DataFrame({'some_feature': a, 'another_feature': b, 'result': c})

fig = px.scatter_3d(df, x='some_feature', y='another_feature', z='result', color='result', size='result', width=800, height=600)

fig.show()
```

+++ {"user_expressions": []}

## Graph Object Interface

For more advanced usage Plotly provides the [graph object interface](https://plotly.com/python-api-reference/plotly.graph_objects.html). Here we first have to create a [Figure](https://plotly.com/python-api-reference/generated/plotly.graph_objects.Figure.html) object, which then is filled with content step by step.

```{code-cell} ipython3
:tags: []

import plotly.graph_objects as go

fig = go.Figure()
fig.layout.width = 800
fig.layout.height = 600

x, y = np.meshgrid(np.linspace(0, 1, 30), np.linspace(0, 1, 30))
z = x * y

fig.add_trace(
    go.Surface(x=x, y=y, z=z, colorscale='Jet')
)

u = x.reshape(-1)
v = y.reshape(-1)
w = z.reshape(-1) + 0.3 * (np.random.rand(z.size) - 0.5)

fig.add_trace(
    go.Scatter3d(
        x=u, y=v, z=w,
        marker={'size': 2, 'color': 'rgba(255,0,0,0.5)'},
        line={'width': 0, 'color': 'rgba(0,0,0,0)'},
        hoverinfo = 'none'
    )
)
        
fig.show()
```

+++ {"user_expressions": []}

## Exporting Figures

Plotly plots can easily be exported to HTML with [Figure.write_html](https://plotly.com/python-api-reference/generated/plotly.graph_objects.Figure.html#plotly.graph_objects.Figure.write_html):

```{code-cell} ipython3
fig.write_html('fig.html', include_plotlyjs='directory', auto_open=False)
```

+++ {"tags": [], "user_expressions": []}

The `include_plotlyjs='directory'` tells Plotly that the exported HTML file finds the Plotly JavaScript Library in the HTML file's directory (for offline use). To make this work [download Plotly's JS bundle](https://github.com/plotly/plotly.js/blob/master/dist/plotly.min.js) from Plotly's GitHub repository.

Defautl behavior is that the exported file is opened automatically after export. To prevent this, use `auto_open=False`.
