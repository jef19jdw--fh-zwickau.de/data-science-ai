---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"user_expressions": []}

(visualization:folium)=
# Folium

[Folium](https://python-visualization.github.io/folium/) is a Python package for generating interactive maps. It's based on [OpenStreetMap](https://www.osm.org) and the [Leaflet](https://leafletjs.com/) JavaScript library.

```{code-cell} ipython3
:tags: []

import folium
```

+++ {"user_expressions": []}

Related projects:
* [](projects:public-transport)
  * [](projects:public-transport:map)

## Basic Usage

Usage is straight-forward.

```{code-cell} ipython3
:tags: []

m = folium.Map(location=[50.7161, 12.4956], zoom_start=16)

folium.Marker(
    [50.7161, 12.4956],
    popup='<b>Zwickau University of Technology</b><br />Kornmark 1',
    tooltip='Zwickau University of Technology',
    icon=folium.Icon(color="red", icon="info-sign")
).add_to(m)

display(m)
```

+++ {"user_expressions": []}

For more features have a look at [Folium's Quickstart Guide](https://python-visualization.github.io/folium/quickstart.html).

## Advanced Features

Of particular interest for data science purposes are:
* [image overlays](https://python-visualization.github.io/folium/modules.html#folium.raster_layers.ImageOverlay) (position raster image on a map)
* [marker clusters](https://python-visualization.github.io/folium/plugins.html#folium.plugins.MarkerCluster) (place many markers on a map)
* [fast marker clusters](https://python-visualization.github.io/folium/plugins.html#folium.plugins.FastMarkerCluster) (position many markers on a map very efficiently)

Data on Folium maps is organized in layers. Layer visibility may be toggled by the user, if we add [layer controls](https://python-visualization.github.io/folium/modules.html#folium.map.LayerControl):

```{code-cell} ipython3
:tags: []

import folium.plugins

m = folium.Map(location=[50.7161, 12.4956], zoom_start=17)

folium.plugins.MarkerCluster(
    [[50.7161, 12.4956], [50.7161, 12.4966], [50.7171, 12.4956]],
    name='some marker'
).add_to(m)

folium.plugins.MarkerCluster(
    [[50.7151, 12.4946], [50.7161, 12.4946], [50.7151, 12.4956]],
    name='more marker'
).add_to(m)

folium.LayerControl().add_to(m)

display(m)
```

+++ {"user_expressions": []}

## Exporting a Map

To generate an HTML file containing the interactive Folium map, call [`save`](https://python-visualization.github.io/branca/element.html). The `save` method is inherited by Foliums `Map` from `branca.elements.Element` as we see from [`Map`'s documentation](https://python-visualization.github.io/folium/modules.html#folium.folium.Map). [Branca](https://github.com/python-visualization/branca) is used by Folium for HTML and JavaScript generation.

```{code-cell} ipython3
m.save('folium.html')
```

+++ {"user_expressions": []}

The generated HTML file may be edited to add some more (HTML and JavaScript) content. Alternatively, we may inject some HTML from Python source via ['get_root'](https://python-visualization.github.io/branca/element.html#branca.element.Element.get_root):

```{code-cell} ipython3
m.get_root().html.add_child(folium.Element(
'''
<div style="position: absolute; z-index: 10000; top: 10px; left: 60px; background: white; padding:5px 10px;">
<span style="font-size: 20pt; font-weight: bold; color: black;">Some Title</span><br/>
<span style="font-size: 10pt; color: blue;">some text</span>
</div>
'''
))

display(m)
m.save('folium.html')
```
