---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(managing-data:pandas:data-frames)=
# Data Frames

A Pandas data frame (a `DataFrame` object) is a collection of Pandas series with common index. Each series can be interpreted as a column in a two-dimensional table. There is a second index object for indexing columns.

Data frames are the most important data structure provided by Pandas.

```{code-cell} ipython3
import pandas as pd
```

## Creation of `DataFrame` Objects

A `DataFrame` object can be created from lists/dictionaries of lists/dictionaries or from NumPy arrays or from lists/dictionaries of series, for instance. See [DataFrame constructor](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html) in Pandas' documentation. If necessary row labels and column labels can be provided via `index` and `columns` keyword arguments, respectively.

```{code-cell} ipython3
s1 = pd.Series({'a': 1, 'b': 2})
s2 = pd.Series({'b': 3, 'c': 4})

df = pd.DataFrame({'left': s1, 'right': s2})

df
```

```{code-cell} ipython3
df = pd.DataFrame([[1, 2, 3], [4, 5, 6], [7, 8, 9]],
                  index=['top', 'middle', 'bottom'],
                  columns=['left', 'middle', 'right'])

df
```

```{hint}
JupyterLab shows data frames as graphical table. In other words, Jupyter's `display` function yields output different from `print`. With `print` we get a text representation of the data frame.
```

The `shape` member contains a tuple with number of rows and columns in the data frame:

```{code-cell} ipython3
df.shape
```

## Data Alignment

Like for series data frames implement data alignment where possible. This applies to row indexing as well as to column indexing.

```{code-cell} ipython3
df1 = pd.DataFrame([[1, 2, 3], [4, 5, 6], [7, 8, 9]],
                   index=['a', 'b', 'c'], columns=['A', 'B', 'C'])
df2 = pd.DataFrame([[11, 12, 13], [14, 15, 16], [17, 18, 19]],
                   index=['b', 'c', 'd'], columns=['A', 'C', 'D'])

display(df1)
display(df2)
display(df1 + df2)
```

## Underlying Data Structures

The index object for row indexing is accessible via `index` member and the index object for column indexing is accessible via `columns` member.

Data frames also have a [`to_numpy`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.to_numpy.html) method returning a data frame's data as NumPy array.

## Indexing

Indexing data frames is very similar to indexing series. The major difference is that for data frames we have to specify a row and a column instead of only a row.

### Overview

There exist four widely used mechanisms (here `df` is some data frame):
* `df[...]`: Python style indexing
* `df.ix[...]`: old Pandas style indexing (removed from Pandas in January 2020)
* `df.loc[...]` and `df.iloc[...]`: new Pandas style indexing
* `df.at[...]` and `df.iat[...]`: new Pandas style indexing for more efficient access to single items

Python style indexing and old Pandas style indexing (the *`ix` indexer*) allow for position based indexing, label based indexing and mixed indexing (position for row, label for column, and vice versa).
Both `[...]` and `ix[...]` behave slightly differently. As already discussed for series, sometimes it is not clear whether positional or label based indexing shall be used. Thus, `[...]` should be used with care. The `ix` indexer has been removed from Pandas in January 2020, but may appear in old code and documents.

### Label Based Column Indexing with `[...]`

Indexing with `[...]` is mainly used for selecting columns by label. For other purposes new Pandas style indexing should be used.

```{code-cell} ipython3
df = pd.DataFrame([[1, 2, 3], [4, 5, 6], [7, 8, 9]],
                  index=['a', 'b', 'c'], columns=['A', 'B', 'C'])
display(df)

s = df['A']
df_part = df[['B', 'C']]

display(s)
display(df_part)
```

If only one label is provided, then the result is a `Series` object. If a list of labels is provided, then the result is a `DataFrame` object. In case of integer labels, label based indexing is used when selecting columns:

```{code-cell} ipython3
df = pd.DataFrame([[1, 2, 3], [4, 5, 6], [7, 8, 9]],
                  index=['a', 'b', 'c'], columns=[2, 23, 45])
display(df)
display(df[2])
```

```{hint}
Label based column indexing can be used to create new columns: `df['my_new_column'] = 0` creates a new column containing zeros, for instance.
```

### Positional Indexing

Positional indexing via `iloc[...]` or `iat[...]` works like for two-dimensional NumPy arrays.

```{code-cell} ipython3
df = pd.DataFrame([[1, 2, 3], [4, 5, 6], [7, 8, 9]],
                  index=['a', 'b', 'c'], columns=['A', 'B', 'C'])
display(df)

display(df.iloc[1:3, 0:2])             # slicing
display(df.iloc[[1, 0], [2, 1, 0]])    # list of indices
display(df.iloc[[True, False, True], [False, True, True]])    # boolean indexing
display(df.iat[2, 1])                  # efficient single element access
display(df.iloc[2, 1])                 # less efficient single element access
```

Variants (slicing, boolean and so on) may be mixed for rows and columns.

### Label Based Indexing

Label based indexing works like with dictionaries. But slicing is allowed.

```{code-cell} ipython3
df = pd.DataFrame([[1, 2, 3], [4, 5, 6], [7, 8, 9]],
                  index=['a', 'b', 'c'], columns=['A', 'B', 'C'])
display(df)

display(df.loc['c':'b':-1, 'A':'B'])       # slicing
display(df.loc[['c', 'a'], ['B', 'A']])    # list of labels
display(df.loc[[True, False, True], [False, True, True]])    # boolean indexing
display(df.at['b', 'B'])                   # efficient single element access
display(df.loc['b', 'B'])                  # less efficient single element access
```

```{important}
Like for series label based slicing includes the stop label!
```

### Indexing by Callables

Both `loc[...]` and `iloc[...]` accept a function for row index and column index. The function has to take a data frame as argument and has to return something allowed for indexing (list of indices/labels, boolean array and so on).

### Mixing Positional and Label Based Indexing

Indexing with `[...]` allows for mixing positional and label based indexing. But `[...]` should be avoided as discussed for series. The only exception is column selection. With Pandas' new indexing style mixed indexing is still possible but requires some more bytes of code:

```{code-cell} ipython3
df = pd.DataFrame([[1, 2, 3], [4, 5, 6], [7, 8, 9]],
                  index=['a', 'b', 'c'], columns=['A', 'B', 'C'])
display(df)

display(df.loc['a':'b', df.columns[0:2]])
```

The idea is to use `loc[...]` and the `columns` member variable. With `columns` we get access to the index object for column indexing. This index object allows for usual positional indexing techniques (slicing, boolean and so on). The result of indexing an index object is an index object again. But the new index object only contains the desired subset of indices. This smaller index object than is passed to `loc[...]`. Same is possible for rows via `index` member.

### Views and Copies

Like for NumPy arrays indexing Pandas data frames may return a view of the data frame. That is, modifying the extracted subset of items might modify the original data frame. If you really need a copy of the items, use the [`copy`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.copy.html) method of `DataFrame` objects.

## Some Useful Member Functions

A [full list of member functions for `DataFrame` objects](https://pandas.pydata.org/docs/reference/frame.html) is provided in Pandas' documentation. Here we only list a few.

### A First Look at a Data Frame

With [`describe`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.describe.html) we get basic statistical information about each column holding numerical values. The function returns a `DataFrame` object containing the collected information. Only columns with numerical data are considered by `describe`.

First and last rows are returned by [`head`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.head.html) and [`tail`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.tail.html). Both take an optional argument specifying the number of rows to return. Default is 5.

The [`info`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.info.html) method prints memory usage and other useful information.

```{code-cell} ipython3
df = pd.DataFrame([[1, 2, 3, 'some'], [4, 5, 6, 'string'], [7, 8, 9, 'here']],
                  index=['a', 'b', 'c'], columns=['A', 'B', 'C', 'D'])
display(df)

display(df.describe())
display(df.head(2))
display(df.tail(2))
df.info()
```

### Iterating Over a Data Frame

To iterate over columns use [`items`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.items.html), which returns tuples containing the column label and the column's data as `Series` object.

```{code-cell} ipython3
df = pd.DataFrame([[1, 2, 3], [4, 5, 6], [7, 8, 9]],
                  index=['a', 'b', 'c'], columns=['A', 'B', 'C'])
display(df)

for label, s in df.items():
    print(label)
    print(s, '\n')
```

Alternativly, iterate over the `columns` member variable:

```{code-cell} ipython3
for label in df.columns:
    print(label)
    print(df[label], '\n')
```

Iteration over rows can be implemented via [`iterrows`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.iterrows.html) method. Analogously to `items` it returns tuples containing the row label and a `Series` object with the data.

```{code-cell} ipython3
for label, s in df.iterrows():
    print(label)
    print(s, '\n')
```

```{hint}
Data in each column of a data frame has identical type, but types in a row may differ (from column to column). Thus, calling `iterrows` may involve type casting to get row data as `Series` object.
```

```{important}
Usually there is no need to iterate over rows, because Pandas provides much faster vectorized code for almost all operations needed for typical data science projects.
```

### Vectorized Operators

Mathematical operations and comparisons work in complete analogy to `Series` objects.

### Removing and Adding Items

Functions for removing and adding items:
* [`concat`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.concat.html) (concatenate data frames vertically or horizontally),
* [`drop`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.drop.html) (remove rows or columns from data frame).

### Modifying Data in a Data Frame

* [`apply`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.combine.html) (apply function rowwise or columnwise to data frame),
* [`combine`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.combine.html) (choose items from two data frames to form a new one),
* [`where`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.where.html) (replace items which do not satisfy a condition),
* [`mask`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.mask.html) (replace items which satisfy a condition).

### Data Frames from and to CSV Files

The Pandas function [`read_csv`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_csv.html) reads a CSV file and returns a data frame.

The `DataFrame` method [`to_csv`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.to_csv.html) writes data to a CSV file.

### Missing Values

Missing values are a common problem in data science. Pandas provides several functions and mechanisms for handling missing values. Important functions:

* [`isna`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.isna.html) (return boolean data frame with `True` at missing value positions),
* [`notna`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.notna.html) (return boolean data frame with `False` at missing value positions),
* [`fillna`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.fillna.html) (fill all missing values, different fill methods are provided),
* [`dropna`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.dropna.html) (remove rows or columns containing missing values or consisting completely of missing values).

Details may be found in [Pandas user guide](https://pandas.pydata.org/pandas-docs/stable/user_guide/missing_data.html).
