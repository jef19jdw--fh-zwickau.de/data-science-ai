---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(managing-data:pandas:restructuring-data)=
# Restructuring Data

Restructuring and aggregation are two basic methods for extracting statistical information from data. We start with groupwise aggregation and then discuss several forms of restructuring without and with additional aggregation.

```{code-cell} ipython3
import pandas as pd
```

## Grouping

Grouping is the first step in the so-called split-apply-combine procedure in data processing. Data is split into groups by some criterion, then some function is applied to each group, finally results get (re-)combinded. Typical functions in the apply step are sum or mean (more general: aggregation) or any type of transform or filtering functions (drop groups containing `nan` items, for instance).

This chapter follows the structure of the [Pandas user guide](https://pandas.pydata.org/docs/user_guide/groupby.html), but leaves out sections on very specific details. Feel free to have a look at those details later on.

### Splitting into Groups and Basic Usage

Grouping is done by calling the [`groupby`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.groupby.html) method of a series or data frame. It takes a column label or a list of column labels as argument and returns a `SeriesGroupBy` or `DataFrameGroupBy` object. The returned object represents a kind of list of groups, each group being a small series or data frame. All rows in a group have identical values in the columns used for grouping. 

The `...GroupBy` object offers several methods for working with the determined groups. Iterating over such objects is possible, too. 

Grouping by one column and subsequent aggregation yields an index with values from the column used for grouping:

```{code-cell} ipython3
df = pd.DataFrame({'age': [2, 3, 3, 2, 4, 5, 5, 5],
                   'score': [2.3, 4.5, 3.4, 2.0, 5.4, 7.2, 2.8, 3.9]})
display(df)

g = df.groupby('age')

for name, group in g:
    print('age:', name)
    display(group)

df_means  = g.mean()
df_means
```

Grouping by two columns and subsequent aggregation yields a multi-level index:

```{code-cell} ipython3
df = pd.DataFrame({'age': [2, 3, 3, 2, 4, 5, 5, 5],
                   'answer': ['yes', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no'],
                   'score': [2.3, 4.5, 3.4, 2.0, 5.4, 7.2, 2.8, 3.9]})
display(df)

g = df.groupby(['age', 'answer'])

for name, group in g:
    print('age:', name[0])
    print('answer:', name[1])
    display(group)

df_means  = g.mean()
display(df_means)
```

Grouping by levels of a multi-level index is possible by providing the `level` argument to `groupby`.

With `get_group` we have access to single groups:

```{code-cell} ipython3
g.get_group((5, 'yes'))
```

`DataFrameGroupBy` objects allow for column indexing:

```{code-cell} ipython3
df = pd.DataFrame({'age': [2, 3, 3, 2, 4, 5, 5, 5],
                   'answer': ['yes', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no'],
                   'score': [2.3, 4.5, 3.4, 2.0, 5.4, 7.2, 2.8, 3.9]})
display(df)

g = df.groupby('age')

g['answer'].get_group(5)
```

### Aggregation

To apply a function to each column of each group use [`aggregate`](https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.aggregate.html#pandas.core.groupby.DataFrameGroupBy.aggregate). It takes a function or a list of functions as argument. Providing a dictionary of `column: function` pairs allows for column specific functions.

```{code-cell} ipython3
import numpy as np

df = pd.DataFrame({'age': [2, 3, 3, 2, 4, 5, 5, 5],
                   'answer': ['yes', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no'],
                   'score': [2.3, 4.5, 3.4, 2.0, 5.4, 7.2, 2.8, 3.9]})
display(df)

g = df.groupby('age')

display(g.aggregate(np.min))
display(g.aggregate([np.min, np.max]))
display(g.aggregate({'answer': np.min, 'score': np.mean}))
```

With `size` we get group sizes.

```{code-cell} ipython3
g.size()
```

Many aggregation functions are directly accessible from the `...GroupBy` object. Examples are `...GroupBy.sum` and `...GroupBy.mean`. See [Computations / descriptive stats](https://pandas.pydata.org/docs/reference/groupby.html#dataframegroupby-computations-descriptive-stats) for a complete list.

### Transformation

The [`transform`](https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.transform.html#pandas.core.groupby.DataFrameGroupBy.transform) method allows to transform rows groupwise resulting in a data frame with same shape as the original one.

```{code-cell} ipython3
df = pd.DataFrame({'age': [2, 3, 3, 2, 4, 5, 5, 5],
                   'answer': ['yes', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no'],
                   'score': [2.3, 4.5, 3.4, 2.0, 5.4, 7.2, 2.8, 3.9]})
display(df)

g = df.groupby('age')

# substract the groups mean score in each age group
df['score'] = g['score'].transform(lambda score: score - score.mean())
df
```

### Filtering

To remove groups use [`filter`](https://pandas.pydata.org/docs/reference/api/pandas.core.groupby.DataFrameGroupBy.filter.html) method. It takes a function as argument and returns a data frame with rows belonging to removed groups removed. The passed function gets the group (series or data frame) and has to return `True` (keep group) or `False` (remove group).

```{code-cell} ipython3
df = pd.DataFrame({'age': [2, 3, 3, 2, 4, 5, 5, 5],
                   'answer': ['yes', 'no', 'no', 'no', 'no', 'yes', 'yes', 'no'],
                   'score': [2.3, 4.5, 3.4, 2.0, 5.4, 7.2, 2.8, 3.9]})
display(df)

g = df.groupby('age')

g.filter(lambda dfg: dfg['score'].mean() > 4)
```

## Restructuring Without Aggregation

There are three basic techniques for restructuring data in a data frame:
* [`pivot`](https://pandas.pydata.org/docs/reference/api/pandas.pivot.html) (interprets two specified columns as row and column index)
* [`stack`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.stack.html#pandas.DataFrame.stack)/[`unstack`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.stack.html#pandas.DataFrame.unstack) (move (level of) column index to (level of) row index and vice versa)
* [`melt`](https://pandas.pydata.org/docs/reference/api/pandas.melt.html) (create new column from some column labels)

Details and graphical illustrations of these technique may be found in [Pandas' user guide](https://pandas.pydata.org/docs/user_guide/reshaping.html) (first three sections).

## Restructuring With Aggregation

Pandas supports pivot tables via [`pivot_table`](https://pandas.pydata.org/docs/reference/api/pandas.pivot_table.html) function. Pivot tables are almost the same as pivoting with [`pivot`](https://pandas.pydata.org/docs/reference/api/pandas.pivot.html) but allow for multiple values per data cell, which then are aggregated to one value.

Details may be found in [Pandas' user guide](https://pandas.pydata.org/docs/user_guide/reshaping.html#pivot-tables).

Similar functionality is provided by [`crosstab`](https://pandas.pydata.org/docs/reference/api/pandas.crosstab.html). See [Pandas user guide](https://pandas.pydata.org/docs/user_guide/reshaping.html#cross-tabulations), too.
