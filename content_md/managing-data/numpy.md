---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(managing-data:numpy)=
# Efficient Computations with NumPy

Almost all data comes as tables with lots of numbers. [NumPy](https://numpy.org) is a Python package for effciently handling large tables of numbers. NumPy also provides advanced and very efficient linear algebra operations on such tables, for instance solving systems of linear equations based on the data. Most machine learning algorithms boil down to moving large amounts of data and doing some linear algebra. Thus, it's a good idea to spend some time understanding NumPy's basic principles and discovering NumPy's functionality.

```{tableofcontents}
```

Related exercises:
* [](exercises:managing-data:numpy-basics)
* [](exercises:managing-data:image-processing)
