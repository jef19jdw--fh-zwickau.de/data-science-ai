---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(managing-data:pandas)=
# High-Level Data Management with Pandas

The [Pandas](https://pandas.pydata.org/) Python package makes NumPy's efficient computing capabilities more accessible for data science purposes and adds functionality for complex data types like timestamps, time periods and categories. Pandas provides powerful data structures and functions for managing, transforming and analyzing data.

```{tableofcontents}
```

Related exercises:
* [](exercises:managing-data:pandas-basics)
* [](exercises:managing-data:pandas-indexing)
* [](exercises:managing-data:advanced-pandas)
* [](exercises:managing-data:pandas-vectorization)


Related projects:
* [](projects:public-transport)
  * [](projects:public-transport:data-setup)
  * [](projects:public-transport:connections)
* [](projects:corona-deaths)
* [](projects:weather)
  * [](projects:weather:climate-change)
