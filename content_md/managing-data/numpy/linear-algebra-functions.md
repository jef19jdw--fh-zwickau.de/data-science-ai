---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(managing-data:numpy:linear-algebra-functions)=
# Linear Algebra Functions

NumPy has a submodule `linalg` for functions related to linear algebra, but the base `numpy` module also contains some linear algebra functions. [Linear algebra](https://numpy.org/doc/stable/reference/routines.linalg.html) in NumPy's documentation provides a comprehensive list of functions. Here we only provide few examples.

```{code-cell} ipython3
import numpy as np
```

For mathematical definitions see [](math:linalg).

## Vector Products

For inner products use [`np.inner`](https://numpy.org/doc/stable/reference/generated/numpy.inner.html). For outer products use [`np.cross`](https://numpy.org/doc/stable/reference/generated/numpy.outer.html).

```{code-cell} ipython3
a = np.array([1, 2, 3])
b = np.array([1, 0, 2])

print(np.inner(a, b))
print(np.cross(a, b))
```

## Matrix Transpose

The [`np.transpose`](https://numpy.org/doc/stable/reference/generated/numpy.transpose.html) function yields the transpose of a matrix. Alternatively, a NumPy array's member variable `T` holds the transpose, too. The transpose is a view (not a copy) of the original matrix.

```{code-cell} ipython3
A = np.array([[1, 2, 3],
              [4, 5, 6]])

print(np.transpose(A))
print(A.T)
```

## Matrix Multiplication

NumPy introduces the `@` operator for matrix multiplication. It's equivalent to calling [`np.matmul`](https://numpy.org/doc/stable/reference/generated/numpy.matmul.html).

```{code-cell} ipython3
A = np.array([[1, 2, 3],
              [4, 5, 6]])
B = np.array([[1, 0],
              [2, 1],
              [1, 1]])

print(A @ B)
print(np.matmul(A, B))
```

## Determinants and Inverses

Determinants and inverses of square matrices can be computed with [`np.linalg.det`](https://numpy.org/doc/stable/reference/generated/numpy.linalg.det.html) and [`np.linalg.inv`](https://numpy.org/doc/stable/reference/generated/numpy.linalg.inv.html), respectively.

```{code-cell} ipython3
A = np.array([[2, 0],
              [1, 1]])

print(np.linalg.det(A))
print(np.linalg.inv(A))
```

## Solving Systems of Linear Equations

The [`np.solve`](https://numpy.org/doc/stable/reference/generated/numpy.linalg.solve.html) function solves a system of linear equations.

```{code-cell} ipython3
A = np.array([[2, 0],
              [1, 1]])
b = np.array([2, 3])
              
print(np.linalg.solve(A, b))
```
