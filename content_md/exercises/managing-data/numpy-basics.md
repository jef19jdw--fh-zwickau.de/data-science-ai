---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(exercises:managing-data:numpy-basics)=
# NumPy Basics

Before solving these basic NumPy exercises you should have read [](managing-data:numpy:numpy-arrays), [](managing-data:numpy:array-operations), [](managing-data:numpy:advanced-indexing), and [](managing-data:numpy:vectorization). Only use NumPy features discussed there.

```{code-cell} ipython3
import numpy as np
```

## Maximum

Given two arrays print the largest value of all elements from both arrays. Use `np.max` or `np.maximum` or both.

Test your code with

\begin{equation*}
\begin{pmatrix}
1&2&3\\4&5&6\\7&8&9
\end{pmatrix}
\qquad\text{and}\qquad
\begin{pmatrix}
1&2&3
\end{pmatrix}.
\end{equation*}

**Solution:**

```{code-cell} ipython3
# your solution
```

## Largest Difference

Print the largest elementwise absolute difference between two equally shaped arrays.

Test your code with
\begin{equation*}
\begin{pmatrix}
1.3&2.4\\3.5&6.5
\end{pmatrix}
\qquad\text{and}\qquad
\begin{pmatrix}
1.25&2.34\\3.499&6.55
\end{pmatrix}
\end{equation*}

**Solution:**

```{code-cell} ipython3
# your solution
```

## Comparisons 1

Given an array of integers, test whether there is a one in the array. Print a message showing the result.

Test your code with
\begin{equation*}
\begin{pmatrix}
1&2&3\\4&5&6\\7&8&9
\end{pmatrix}
\qquad\text{and also with}\qquad
\begin{pmatrix}
0&2&3\\4&5&6\\7&8&9
\end{pmatrix}.
\end{equation*}

**Solution:**

```{code-cell} ipython3
# your solution
```

## Comparisons 2

Test whether all elements of an array are positive. Print a message showing the result.

Test your code with

\begin{equation*}
\begin{pmatrix}
1&2&3\\4&5&6\\7&8&9
\end{pmatrix}
\qquad\text{and also with}\qquad
\begin{pmatrix}
1&-2&3\\-4&5&6\\7&8&9
\end{pmatrix}.
\end{equation*}

**Solution:**

```{code-cell} ipython3
# your solution
```

## Indexing 1

Set all elements between -1 and 1 to zero. Remember: Avoid loops wherever possible.

Test you code with
\begin{equation*}
\begin{pmatrix}
-2&-0.5&0\\0.4&5&0.9\\1&8&9.
\end{pmatrix}
\end{equation*}

**Solution:**

```{code-cell} ipython3
# your solution
```

## Indexing 2

Write a function which takes an integer $n$ and returns an $n\times n$ matrix with ones at the borders and zeros inside. Data type of the matrix should be `int8`.

Example for $n=5$:
\begin{equation*}
\begin{pmatrix}1&1&1&1&1\\1&0&0&0&1\\1&0&0&0&1\\1&0&0&0&1\\1&1&1&1&1\end{pmatrix}.
\end{equation*}

**Solution:**

```{code-cell} ipython3
# your solution
```

## Indexing 3

Write a function which takes an arbitrarily sized matrix and returns a matrix having the same elements as the original matrix, but being bordered by additional zeros. The returned matrix should have the same data type as the original matrix.

Example:
\begin{equation*}
\text{input:}\quad
\begin{pmatrix}1&2&3\\4&5&6\end{pmatrix},
\qquad\text{output:}\quad
\begin{pmatrix}0&0&0&0&0\\0&1&2&3&0\\0&4&5&6&0\\0&0&0&0&0\end{pmatrix}.
\end{equation*}

**Solution:**

```{code-cell} ipython3
# your solution
```

## Broadcasting

Take the first row of a matrix and add it to all other rows of the matrix. Print the resulting matrix.

Test your code with
\begin{equation*}
\begin{pmatrix}
1&2&3&4\\5&6&7&8\\9&10&11&12
\end{pmatrix}.
\end{equation*}

**Solution:**

```{code-cell} ipython3
# your solution
```

## Not as Easy as it Seems

Given a square matrix with odd number of rows/columns replace all but the boundary elements by zeros and write the mean of all boundary elements to the center element. Print the modified matrix.

Example:
\begin{equation*}
\text{original matrix:}\quad
\begin{pmatrix}-1&2&-1&2&-1\\2&-2&3&-2&2\\-1&3&-3&3&-1\\2&-2&3&-2&2\\-1&2&-1&2&-1\end{pmatrix},
\qquad\text{result:}\quad
\begin{pmatrix}-1&2&-1&2&-1\\2&0&0&0&2\\-1&0&0.5&0&-1\\2&0&0&0&2\\-1&2&-1&2&-1\end{pmatrix}.
\end{equation*}

**Solution:**

```{code-cell} ipython3
# your solution
```
