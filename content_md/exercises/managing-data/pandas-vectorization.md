---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(exercises:managing-data:pandas-vectorization)=
# Pandas Vectorization

Before solving these exercises you should have read [](managing-data:pandas).

```{code-cell} ipython3
import pandas as pd
```

For these exercises we use the dataset obtained in the [](projects:cafeteria) project.

```{code-cell} ipython3
data = pd.read_csv('meals.csv', names=['date', 'category', 'name', 'students', 'staff', 'guests'])

data
```

## Preprocessing

### Data Types

Convert `'date'` and `'category'` columns to `Timestamp` and `category`, respectively (see [](exercises:managing-data:advanced-pandas) exercises).

**Solution:**

```{code-cell} ipython3
# your solution
```

### Count Categories

For each category give the number of meals.

**Solution:**

```{code-cell} ipython3
# your solution
```

### Remove Categories

Remove Categories which do not contain full-fledged meals descriptions (e.g., `'Salat Bar'`).

**Solution:**

```{code-cell} ipython3
# your solution
```

## Remove Allergens and Additives

Most meals contain information on alergenes and additives (numbers in parantheses). Remove the information to get more readably meal descriptions. Implement the removal procedure twice: without and with vectorized string operations. Get and compare execution times.

**Solution:**

```{code-cell} ipython3
data['name_backup'] = data['name']
```

```{code-cell} ipython3
%%timeit
# your solution without vectorized string operations
```

```{code-cell} ipython3
data['name'] = data['name_backup']
data = data.drop(columns=['name_backup'])
```

```{code-cell} ipython3
%%timeit
# your solution with vectorized string operations
```

## Simplify Meal Descriptions

Create a new column `'simple'` from the `'name'` column by removing all lower-case words, all punctuation marks and so on. Only words starting with an upper-case letter are allowed. 

**Solution:**

```{code-cell} ipython3
# your solution
```

## All Meals with...

Given a key word (e.g., `'Kartoffel'`) get the number of meals containing the keyword and print all meal descriptions.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Meal Plot

For each day get the number of meals containing some keyword (e.g., `'Kartoffel'`). Call `Series.plot` to visualize the result.

**Solution:**

```{code-cell} ipython3
# your solution
```
