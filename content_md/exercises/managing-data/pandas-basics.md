---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(exercises:managing-data:pandas-basics)=
# Pandas Basics

Before solving these basic Pandas exercises you should have read [](managing-data:pandas:series) and [](managing-data:pandas:data-frames).

For these exercises we use a dataset describing used cars obtained from [kaggle.com](https://www.kaggle.com/nehalbirla/vehicle-dataset-from-cardekho). Licences: [Open Data Commons Database Contents License (DbCL) v1.0](http://opendatacommons.org/licenses/dbcl/1.0/) and [Open Data Commons Open Database License (ODbL) ](https://opendatacommons.org/licenses/odbl/summary/).

```{code-cell} ipython3
import pandas as pd

data = pd.read_csv('cars.csv')
```

## First Look

### Basic Information

Print the following information about the data frame `data`:
* first 10 rows,
* number of rows,
* basic statistical information,
* column labels, data types, memory usage.

**Solution:**

```{code-cell} ipython3
# your solution
```

### Missing Values

Are there missing values in `data`?

**Solution:**

```{code-cell} ipython3
# your answer
```

### Value Counts

Use [`DataFrame.nunique`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.nunique.html) to get the number of different values per column.

**Solution:**

```{code-cell} ipython3
# your solution
```

### Unique Car Models

Use [`DataFrame.value_counts`](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.value_counts.html) to get the number of unique `'name'`-`'year'` combinations.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Restructure Columns

### New Columns

Append a column `'manual_trans'` containing `True` where column `'transmission'` shows `'Manual'`, else `False`.

Append a column `'age'` showing a car's age (now minus `'year'`).

**Solution:**

```{code-cell} ipython3
# your solution
```

### Remove Columns

Remove columns `'seller_type'`, `'transmission'`, and `'owner'`.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Mean Price

### Series with String Index

Create a Pandas series `price` with column `'name'` as index and column `'selling_price'` as data.

**Solution:**

```{code-cell} ipython3
# your solution
```

### Mean

Calculate mean price for model `'Maruti Swift Dzire VDI'`.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Kilometers per Year

### Boolean Indexing

Use boolean row indexing to get a data frame `one_model` with columns `'km_driven'` and `'age'` containing only rows with `'name'` equal to `'Maruti Swift Dzire VDI'`.

**Solution:**

```{code-cell} ipython3
# your solution
```

### New Column

Add a column `'km_per_year'` to the `one_model` data frame containing kilometers per year.

**Solution:**

```{code-cell} ipython3
# your solution
```

### Mean

Get the mean of column `'km_per_year'` in `one_model`.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Oldest Car

Find the oldest car in `data` and print its name and manufacturing year. Have a look at [Pandas' documentation](https://pandas.pydata.org/docs/) for suitable functions.

**Solution:**

```{code-cell} ipython3
# your solution
```
