---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(exercises:python:memory-management)=
# Memory Management

Here you find some exercises on Python's optimization techniques for memory management discussed in [](python:variables:efficiency).

The last two tasks demonstrate effects of running out of memory and how to prevent such situations. Read [](python:variables:efficiency:garbage-collection) first.

## Similar Code, Different Results 1

Why do the following two code cells yield different results?

```{code-cell} ipython3
a = 100
b = 2 * a
c = 2 * a
print(b is c)
```

```{code-cell} ipython3
a = 100
b = 3 * a
c = 3 * a
print(b is c)
```

**Solution:**

```{code-cell} ipython3
# your answer
```

## Similar Code, Different Results 2

Copy the following code to a text file and feed the file to the Python interpreter. Why do both variants yield different results? Why running in Jupyter yields identical results in both cases?

```{code-cell} ipython3
# variant 1
a = 1234
b = 1234
print(a is b)

# variant 2
c = 34
a = 1200 + c
b = 1200 + c
print(a is b)
```

**Solution:**

```{code-cell} ipython3
# your answer
```

## Some More Code Optimization

Copy the following code to a text file and feed the file to the Python interpreter. Do you have an idea why the next code yields `True`?

```{code-cell} ipython3
a = 1200 + 34
b = 1200 + 34
print(a is b)
```

**Solution:**

```{code-cell} ipython3
# your answer
```

## Running Out of Memory

Run the following code and observe what happens to your system. Use some system tool to monitor memory consumption. You may interrupt the Python kernel in Jupyter if necessary.

```{warning}
Save all your data you are currently editing. Depending on your system you may have to reboot your machine due to hanging the system.
```

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

stop = False
data = 'x'

while not stop:

    print('Hit Return to stop. Type an int to multiply memory consumption with.')
    fac = input()
    
    if fac == '':
        stop = True
    else:
        fac = int(fac)
        print('increasing memory usage to', fac * len(data), 'bytes...')
        new_data = ''
        for i in range(0, fac):
            new_data += data
        data = new_data
        del new_data
        print('...done')

del data
```

Note the `del data` line. This line is not required if the program is run as a stand-alone program (text file fed to the interpreter), because at exit the interpreter will free all memory used by the program. But in Jupyter the interpreter does not stop at the end of a code cell. All objects created by the cell remain in memory until the kernel dies or memory is explicitly freed via `del`.

**Solution:**

```{code-cell} ipython3
# your answer
```

## Managing Memory

Write a function which returns a string with approximately 1 billion characters. Then write a program which can manage three data sets. Repeatedly ask the user whether he or she wants the exit the program or whether he or she wants to load/remove data set 1/2/3. Load and remove data sets according to the user's choice. Monitor memory consumption while running the program to see whether the program works as intended.

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your solution
```
