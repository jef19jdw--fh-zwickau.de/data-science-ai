---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(exercises:python:variables-operators)=
# Variables and Operators

Python's approach to variables and operators is simple and beautiful, although beginners need some time to see both simplicity and beauty. In each exercise below keep track of what's happening in detail behind the scenes. That is, track the creation of objects and which name is tied to which object. Before solving the exercises read the chapter on [](python:variables) (sections [](python:variables:operators:operators-member-functions) and [](python:variables:efficiency) are not required here).

## Global vs. Local Variables 1

The following code contains an error. Find it (by running the code) and correct it.

```python
def do_something():
    n = n + 1
    print('something')
    
n = 0    # counter for function calls

for i in range(0, 100):
    if i % 11 == 0:
        do_something()

print('function called', n, 'times')
```

**Solution:**

```{code-cell} ipython3
:tags: [remove-output, raises-exception]

# your modifications

def do_something():
    n = n + 1
    print('something')
    
n = 0    # counter for function calls

for i in range(0, 100):
    if i % 11 == 0:
        do_something()

print('function called', n, 'times')
```

## Global vs. Local Variables 2

The following code shall print 10 lines each containing 5 numbers. Make it work correctly.

```python
def print_n_times_5_numbers(m, n, o, p, q):
    for k in range(0, n):
        print(m, n, o, p, q)

n = 10    # number of rows to print

print('printing', n, 'times 5 numbers:')
print_n_times_5_numbers(42, 23, 32, 24, 111)
```

**Solution:**

```{code-cell} ipython3
# your modifications

def print_n_times_5_numbers(m, n, o, p, q):
    for k in range(0, n):
        print(m, n, o, p, q)

n = 10    # number of rows to print

print('printing', n, 'times 5 numbers:')
print_n_times_5_numbers(42, 23, 32, 24, 111)
```

## List of Squares

Make the following code work correctly.

```python
a = [1, 2, 3, 4, 5]

squares = a
for i in range(0, len(squares)):
    squares[i] **= 2

print('squares of', a, 'are:')
print(squares)
```

**Solution:**

```{code-cell} ipython3
# your modifications

a = [1, 2, 3, 4, 5]

squares = a
for i in range(0, len(squares)):
    squares[i] **= 2

print('squares of', a, 'are:')
print(squares)
```

## Similar Code, Different Results

Why do the following two code cells yield different results? Explain in detail what's happening!

```{code-cell} ipython3
a = 2
b = a
a = 5
print(b)
```

```{code-cell} ipython3
a = [2]
b = a
a[0] = 5
print(b[0])
```

**Solution:**

```{code-cell} ipython3
# your answer
```

## 2 > 3?

Guess why the condition `2 <= 3 == True` evaluates to `False`. How to repair?

```python
if 2 <= 3 == True:
    print('2 <= 3')
else:
    print('2 > 3, really?')
```

**Solution:**

```{code-cell} ipython3
# your modifications

if 2 <= 3 == True:
    print('2 <= 3')
else:
    print('2 > 3, really?')
```

## Minus Minus

Why do the following two code cells yield different results?

```{code-cell} ipython3
a = 5
b = 2
c = 3
print(a - b - c)
```

```{code-cell} ipython3
a = 5
b = 2
c = 3
a -= b - c
print(a)
```

**Solution:**

```{code-cell} ipython3
# your answer
```
