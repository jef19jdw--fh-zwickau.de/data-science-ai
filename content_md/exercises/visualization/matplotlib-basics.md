---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(exercises:visualization:matplotlib-basics)=
# Matplotlib Basics

Before solving these exercises you should have read [](visualization:matplotlib:basics).

```{code-cell} ipython3
import numpy as np
import matplotlib.pyplot as plt
```

## Simple Function Plot

Plot the sine function for angles from $0$ to $2\pi$. Label the axes and show a title. Use a red line without markers.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Synchronized Axes

Create a figure containing $\sin(x)$ and $2\,\cos(x)$ for $x\in[0,2\,\pi]$ next to each other and having identical vertical axes.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Plotting Poles

Plot $f(x)=\tan(x)$ for $x\in[-\pi,\pi]$ and $f(x)\in[-10, 10]$. Add dashed vertical lines at the poles.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Parametric Curves

Plot a curve $(x(t), y(t))$ in the plane. Align plots of $x(t)$ and $y(t)$ properly below and beside the curve plot, respectively. As an example you could use $x(t)=\cos t+2\,\sin(2\,t)$ and $y(t)=\sin(t)+2\,\cos(3\,t)$ for $t\in[0,2\,\pi]$.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Ticks and Grids

Plot $f(x)=\sin(x)$ for $x\in[-1,7]$. Place major tick labels at multiples of $\pi$ and minor tick labels at multiples of $\pi/2$. The y axis should have major ticks at $-1,0,1$ and minor ticks in between. Activate grid lines for minor and major ticks on x axis and for major ticks on y axis. Further, use increased line width for grid lines at major ticks.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Circular Colorbar

Visualize the function $f(x,y)=\arcsin(x)+\arccos(y)+\pi/2$ for $(x,y)\in[-1, 1]\times[-1, 1]$ with a scatter plot of uniformly distributed points with color given by the function values. Function values are angles in $[0,2\cdot\pi]$. Thus, colors at both ends of the colorbar should coincide. Create a circular colorbar next to the scatter plot.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Bent Arrows

Plot $f(x)=\cos x$ for $x\in[-2\,\pi,2\,\pi]$. Add the text 'two local minima' to the plot and connect it with two arrows to the minima. Take care that the arrows do not intersect with the graph.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Color Channels

Load the image `tux.png` and plot it four times: original, red channel, green channel, blue channel (hint: a simple way to create custom colormaps is [`matplotlib.colors.LinearSegmentedColormap.from_list`](https://matplotlib.org/stable/api/_as_gen/matplotlib.colors.LinearSegmentedColormap.html#matplotlib.colors.LinearSegmentedColormap.from_list)).

**Solution:**

```{code-cell} ipython3
# your solution
```

## Pixelbased Postprocessing

Plot $f(x)=\sin x$ for $x\in[-2\,\pi,2\,\pi]$. Fade out line ends with GIMP.

**Solution:**

```{code-cell} ipython3
# your solution
```

## Vectorbased Postprocessing

Plot $f(x)=\sin x$ for $x\in[-1,7]$ and plot the sine approximations $g(x)=-\frac{4}{\pi^2}\,(x-\frac{\pi}{2})^2+1$ and $h(x)=\frac{4}{\pi^2}\,(x-\frac{3\,\pi}{2})^2-1$. Plot the region $x\in[\pi-\frac{1}{2},\pi+\frac{1}{2}]$ in a separate figure. Save both figures as vector graphics and add the detail view to the sine plot with Inkscape.

**Solution:**

```{code-cell} ipython3
# your solution
```
