---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(exercises:visualization:advanced-matplotlib)=
# Advanced Matplotlib

Before solving these exercises you should have read [](visualization:matplotlib).

## Cycloids

Create a never ending animation which plots a curve in the following way: the pen is rotating around a center, which itself rotates around the origin. Use different radiuses and rotation speeds for both rotations.

**Solution:**

```{code-cell} ipython3
# your solution
```
