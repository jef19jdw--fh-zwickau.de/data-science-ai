---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(exercises:reinforcement:modeling)=
# Modeling

Before solving these exercises you should have read [](reinforcement:overview-examples).

## Tic-Tac-Toe

**Task:** How many actions has [Tic-tac-toe](https://en.wikipedia.org/wiki/Tic-tac-toe) seen from an agent's perspective?

**Solution:**

```{code-cell} ipython3
# your answer
```

+++ {"tags": [], "user_expressions": []}

**Task:** How many states has Tic-tac-toe seen from an agent's perspective, that is, if the next move is the agent's move? Include end states, too (although the agent can't choose any valid action based on an end state). Give a rough estimate (upper bound), then try to find better estimates. Bonus: write some Python code to get the exact value.

**Solution:**

```{code-cell} ipython3
:tags: []

# your answer
```

+++ {"user_expressions": [], "tags": []}

## Connect Four

**Task:** How many actions has [Connect four](https://en.wikipedia.org/wiki/Connect_Four) seen from an agent's perspective?

**Solution:**

```{code-cell} ipython3
# your answer
```

+++ {"user_expressions": []}

**Task:** Give a simple and maybe also a more precise upper bounds for the number of states in Connect four.

**Solution:**

```{code-cell} ipython3
# your answer
```
