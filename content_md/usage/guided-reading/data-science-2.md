---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Data Science II (course at WHZ)

The second semester of the data science lecture series starts with visualization techniques. Then supervised machine learning for generating predictions from data is introduced. Linear regression and artificial neural networks are discussed in depth.

## Data Visualization

### Week 1 (Matplotlib Basics)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](visualization:matplotlib)
  * [](visualization:matplotlib:basics)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](exercises:visualization:matplotlib-basics) (exercises; *Circular Colorbar* is bonus)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:chemnitz-trees): download, cleaning, short names (project)
:::
::::

### Week 2 (Advanced Matplotlib)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](visualization:matplotlib), continued
  * [](visualization:matplotlib:plots-3d)
  * [](visualization:matplotlib:animations)
  * [](visualization:matplotlib:seaborn)
  * [](visualization:matplotlib:maps)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](exercises:visualization:advanced-matplotlib) (exercises)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:chemnitz-trees): information extraction, presentation (project)
:::
::::

### Week 3 (Ploty and Folium)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](visualization:plotly)
* [](visualization:folium)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* revisit [](projects:public-transport:data-setup) (project)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:public-transport)
  * [](projects:public-transport:connections) (project)
:::
::::

## Supervised Learning

### Week 4 (Introduction)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](supervised:ml-overview)
* [](supervised:general)
  * [](supervised:general:problem-workflow)
  * [](supervised:general:example-knn)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:public-transport:map) (project, *Color-Coded Distances* are bonus)
:::
::::

### Week 5 (Quality Measures)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](supervised:general), continued
  * [](supervised:general:quality-measures) (proof of AUC interpretation is bonus)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* revisit [](projects:weather:climate-change) (project)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:weather:weather-animation) (project)
:::
::::

### Week 6 (Feature Reduction)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](supervised:general), continued
  * [](supervised:general:feature-reduction)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:forged-banknotes)
  * [](projects:forged-banknotes:knn) (project)
  * [](projects:forged-banknotes:quality) (project, AUC section is bonus)
:::
::::

### Week 7 (Hyperparameter Optimization)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](supervised:general), continued
  * [](supervised:general:hyperparameter-tuning)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](projects:mnist)
  * revisit/complete [](projects:mnist:load-qmnist) (project)
* revisit/complete [](exercises:managing-data:image-processing) (exercises)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:mnist)
  * [](projects:mnist:qmnist-feature-reduction) (project)
:::
::::

### Week 8 (Linear Regression)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](supervised:linear-regression)
  * [](supervised:linear-regression:approach)
  * [](supervised:linear-regression:regularization)
  * [](supervised:linear-regression:example1)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](supervised:linear-regression), continued
  * [](supervised:linear-regression:example2)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:forged-banknotes)
  * [](projects:forged-banknotes:hyperparameter-optimization) (project)
* [](projects:house-prices)
  * [](projects:house-prices:gui) (bonus project)
:::
::::

### Week 9 (More on Regression)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](supervised:linear-regression), continued
  * [](supervised:linear-regression:outliers)
* [](supervised:logistic-regression)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* finish previous projects
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* finish previous projects
:::
::::

### Week 10 (ANN Basics)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](supervised:ann)
  * [](supervised:ann:basics)
  * [](supervised:ann:training) (mathematical derivation of ANN gradients and ANN implementation from scratch are bonus)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:house-prices)
  * [](projects:house-prices:ann)
:::
::::

### Week 11 (ANNs with Keras)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](computer-science:cloud-computing)
* [](supervised:ann), continued
  * [](supervised:ann:ann-keras)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:mnist:qmnist-pca-ann)
* [](projects:install:long-running)
:::
::::

### Week 12 (CNNs, part I)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](supervised:ann), continued
  * [](supervised:ann:cnn)
  * [](supervised:ann:cnn-keras)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:mnist:qmnist-cnn)
:::
::::

### Week 13 (CNNs, part II)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](supervised:ann), continued
  * [](supervised:ann:cnn-learn)
  * [](supervised:ann:performance)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:mnist:qmnist-cnn-analysis)
* [](projects:cats-dogs-hyperparameters) (bonus)
:::
::::
