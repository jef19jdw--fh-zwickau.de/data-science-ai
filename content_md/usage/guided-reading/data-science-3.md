---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Data Science III (course at WHZ)

Part three of the data science lecture series continues discussion of supervised machine learning. Further methods like decision trees and support vector machines are introduced. Then we move on to unsupervised machine learning covering clustering methods and techniques for dimensionality reduction.

## Supervised Learning

### Week 1 (Decision Trees)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](supervised:trees)
  * [](supervised:trees:basics)
  * [](supervised:trees:regression-trees)
  * [](supervised:trees:classification-trees)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](projects:mnist:iban-recognition) (project)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:forged-banknotes)
  * [](projects:forged-banknotes:tree) (project)
:::
::::

### Week 2 (Ensemble Methods)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](supervised:ensemble-methods)
  * [](supervised:ensemble-methods:idea)
  * [](supervised:ensemble-methods:stacking)
  * [](supervised:ensemble-methods:bagging)
  * [](supervised:ensemble-methods:boosting)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* revisit [](supervised:linear-regression:example2)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:forged-banknotes)
  * [](projects:forged-banknotes:random-forest) (project)
* [](projects:house-prices)
  * [](projects:house-prices:random-forest) (project)
:::
::::

### Week 3 (Support-Vector Machines)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](supervised:svm)
  * [](supervised:svm:hard-margin)
  * [](supervised:svm:soft-margin)
  * [](supervised:svm:kernel-svm)
  * [](supervised:svm:scikit-learn)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](supervised:svm)
  * [](supervised:svm:svr) (bonus)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:forged-banknotes)
  * [](projects:forged-banknotes:svm) (project)
* [](projects:mnist)
  * [](projects:mnist:qmnist-svm) (project)
:::
::::

### Week 4 (Naive Bayes)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](supervised:naive-bayes) (without kernel density estimates)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](supervised:naive-bayes) (kernel density estimates, bonus)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:forged-banknotes)
  * [](projects:forged-banknotes:naive-bayes) (project)
:::
::::

### Week 5 (Text Classification)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](supervised:text-classification)
  * [](supervised:text-classification:preprocessing)
  * [](supervised:text-classification:training)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
:::
::::

### Week 6 (Text Classification)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:blogs)
  * [](projects:blogs:author-classification-training) (project)
  * [](projects:blogs:author-classification-test) (project)
:::
::::

## Unsupervised Learning

### Week 7 (Introduction, Centroid-based Clustering)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](unsupervised:typical-tasks)
* [](unsupervised:quality-measures-clustering)
* [](unsupervised:centroid-clustering)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:mnist)
  * [](projects:mnist:semisupervised-classification) (project)
:::
::::

### Week 8 (Hierarchical Clustering)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](unsupervised:hierarchical-clustering)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:supermarket-customers) (project)
* [](projects:celadons)
  * [](projects:celadons:hierarchical) (project)
:::
::::

### Week 9 (Density-based Distribution-based Clustering)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](unsupervised:density-clustering)
  * [](unsupervised:density-clustering:dbscan)
  * [](unsupervised:density-clustering:optics)
* [](unsupervised:distribution-clustering)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:celadons)
  * [](projects:celadons:density) (project)
* [](projects:mnist)
  * [](projects:mnist:generate-digits) (project)
:::
::::

### Week 10 (Autoencoders)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](unsupervised:autoencoders)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:mnist)
  * [](projects:mnist:autoencoder) (project)
:::
::::

### Week 11 (Nonlinear Dimensionality Reduction, Kernel PCA)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](unsupervised:dim-reduction-overview)
* [](unsupervised:kernel-pca)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](supervised:general:feature-reduction) (reread PCA section)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
:::
::::

### Week 12 (Multidimensional Scaling)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](unsupervised:mds)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
* [](supervised:general:feature-reduction) (reread PCA section)
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:color-perception) (project)
* [](projects:forest-fires) (project)
:::
::::

### Week 13 (LLE, SNE, SOM)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Lectures**
^^^
* [](unsupervised:lle)
* [](unsupervised:sne)
* [](unsupervised:som)
:::

:::{grid-item-card}
:columns: 6
**Self-study**
^^^
:::

:::{grid-item-card}
:columns: 12
**Practice session**
^^^
* [](projects:mnist)
  * [](projects:mnist:qmnist-tsne) (project)
* [](projects:house-prices)
  * [](projects:house-prices:som) (project)
:::
::::
