---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Data Science I (course at WHZ)

The first part of the data science lecture series introduces the Python programming language and some Python libraries required for data processing. Next to Python the focus is on working with big data, obtaining, understanding and restructuring data, as well as extracting basic statistical information from data.

## Warm-Up

### Week 1

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Prepare**
^^^
* [](warm-up:data-science)
* [](warm-up:python)
* [](warm-up:computers-programming)
:::

:::{grid-item-card}
:columns: 6
**Discuss and apply**
^^^
* [](exercises:computer-basics) (exercises)
* [](projects:install)
  * [](projects:install:jupyterlab) (project)
:::

:::{grid-item-card}
:columns: 12
**Practical exercise session**
^^^
* [](projects:install)
  * [](projects:install:jupyter) (project)
:::
::::

## Python for Data Science

### Week 2 (Crash Course I)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Prepare**
^^^
* [](python:crash)
  * [](python:crash:first-program)
  * [](python:crash:blocks)
:::

:::{grid-item-card}
:columns: 6
**Discuss and apply**
^^^
* [](exercises:python:finding-errors) (exercises)
* [](exercises:python:basics) (exercises)
:::

:::{grid-item-card}
:columns: 12
**Practical exercise session**
^^^
* [](projects:install)
  * [](projects:install:python-without-jupyter) (project)
* [](projects:python)
  * [](projects:python:simple-list) (project)
:::
::::

### Week 3 (Crash Course II)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Prepare**
^^^
* [](python:crash), continued
  * [](python:crash:screen-io)
  * [](python:crash:library-code)
  * [](python:crash:everything-object)
:::

:::{grid-item-card}
:columns: 6
**Discuss and apply**
^^^
* [](exercises:python:more-basics) (exercises, last task is bonus)
:::

:::{grid-item-card}
:columns: 12
**Practical exercise session**
^^^
* [](projects:python)
  * [](projects:python:geometric-objects) (project, last section is bonus)
:::
::::

### Week 4 (Variables and Operators)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Prepare**
^^^
* [](python:variables)
  * [](python:variables:names-objects)
  * [](python:variables:types)
  * [](python:variables:operators) (section [](python:variables:operators:operators-member-functions) is bonus)
  * [](python:variables:efficiency) (all but section [](python:variables:efficiency:garbage-collection) is bonus)
:::

:::{grid-item-card}
:columns: 6
**Discuss and apply**
^^^
* [](exercises:python:variables-operators) (exercises)
* [](exercises:python:memory-management) (exercises, all but the last two tasks are considered bonus)
:::

:::{grid-item-card}
:columns: 12
**Practical exercise session**
^^^
* [](projects:weather)
  * [](projects:weather:dwd-open-data) (project)
* [](projects:python)
  * bonus: [](projects:python:vector-multiplication) (project)
:::
::::

### Week 5 (Lists and Friends, Strings)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Prepare**
^^^
* [](python:lists-friends)
  * [](python:lists-friends:tuples)
  * [](python:lists-friends:lists)
  * [](python:lists-friends:dicts)
  * [](python:lists-friends:iterable-objects)

* [](python:strings)
  * [](python:strings:basics)
  * [](python:strings:special-characters)
  * [](python:strings:formatting)
:::

:::{grid-item-card}
:columns: 6
**Discuss and apply**
^^^
* [](exercises:python:lists-friends) (exercises)
* [](exercises:python:strings) (exercises, last one is bonus)
:::

:::{grid-item-card}
:columns: 12
**Practical exercise session**
^^^
* [](projects:mnist)
  * [](projects:mnist:xmnist) (project)
:::
::::

### Week 6 (Accessing Data)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Prepare**
^^^
* [](python:accessing-data)
  * [](python:accessing-data:file-io)
  * [](python:accessing-data:text-files)
  * [](python:accessing-data:zip-files)
  * [](python:accessing-data:csv-files)
  * [](python:accessing-data:html-files)
  * [](python:accessing-data:xml-files)
  * [](python:accessing-data:web-access)
:::

:::{grid-item-card}
:columns: 6
**Discuss and apply**
^^^
* [](exercises:python:file-access) (exercises)
:::

:::{grid-item-card}
:columns: 12
**Practical exercise session**
^^^
* [](projects:cafeteria), download part (project)
:::
::::

### Week 7 (Functions, Modules, Packages)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Prepare**
^^^
* [](python:functions)
  * [](python:functions:basics)
  * [](python:functions:passing-arguments)
  * [](python:functions:anonymous-functions)
  * [](python:functions:function-method-objects)
  * [](python:functions:recursion)
* [](python:modules-packages)
:::

:::{grid-item-card}
:columns: 6
**Discuss and apply**
^^^
* [](exercises:python:functions) (exercises)
:::

:::{grid-item-card}
:columns: 12
**Practical exercise session**
^^^
* [](projects:cafeteria), parsing part (project)
:::
::::

### Week 8 (Errors, Debugging, Inheritance)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Prepare**
^^^
* [](python:error-handling-debugging-overview)
* [](python:inheritance) (last section [](python:inheritance:exceptions) is bonus)
* [](computer-science:uml) (bonus)
:::

:::{grid-item-card}
:columns: 6
**Discuss and apply**
^^^
* [](exercises:python:oop) (exercises)
* [](python:further-python-features) (bonus reading)
:::

:::{grid-item-card}
:columns: 12
**Practical exercise session**
^^^
* [](projects:weather)
  * [](projects:weather:getting-forecasts), download part (project)
:::
::::

## Managing Data with Python

### Week 9 (NumPy Basics)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Prepare**
^^^
* [](managing-data:numpy)
  * [](managing-data:numpy:numpy-arrays)
  * [](managing-data:numpy:array-operations)
  * [](managing-data:numpy:advanced-indexing)
  * [](managing-data:numpy:vectorization)
:::

:::{grid-item-card}
:columns: 6
**Discuss and apply**
^^^
* [](exercises:managing-data:numpy-basics) (exercises)
:::

:::{grid-item-card}
:columns: 12
**Practical exercise session**
^^^
* [](projects:weather)
  * [](projects:weather:getting-forecasts), parsing part (project, automatic download is bonus)
:::
::::

### Week 10 (Advanced NumPy)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Prepare**
^^^
* [](managing-data:numpy), continued
  * [](managing-data:numpy:array-manipulation-functions)
  * [](managing-data:numpy:copies-views)
  * [](managing-data:numpy:efficiency-considerations)
  * [](managing-data:numpy:special-floats)
  * [](managing-data:numpy:linear-algebra-functions)
  * [](managing-data:numpy:random-numbers)
* [](managing-data:saving-loading-data)
:::

:::{grid-item-card}
:columns: 6
**Discuss and apply**
^^^
* [](exercises:managing-data:image-processing) (exercises, last one is bonus)
:::

:::{grid-item-card}
:columns: 12
**Practical exercise session**
^^^
* [](projects:mnist)
  * [](projects:mnist:load-qmnist) (project)
:::
::::

### Week 11 (Pandas Basics)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Prepare**
^^^
* [](managing-data:pandas)
  * [](managing-data:pandas:series)
  * [](managing-data:pandas:data-frames)
:::

:::{grid-item-card}
:columns: 6
**Discuss and apply**
^^^
* [](exercises:managing-data:pandas-basics) (exercises)
:::

:::{grid-item-card}
:columns: 12
**Practical exercise session**
^^^
* [](projects:public-transport)
  * [](projects:public-transport:data-setup) (project)
:::
::::

### Week 12 (Advanced Indexing, Dates and Times)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Prepare**
^^^
* [](managing-data:pandas), continued
  * [](managing-data:pandas:advanced-indexing)
  * [](managing-data:pandas:dates-times)
:::

:::{grid-item-card}
:columns: 6
**Discuss and apply**
^^^
* [](exercises:managing-data:pandas-indexing) (exercises)
:::

:::{grid-item-card}
:columns: 12
**Practical exercise session**
^^^
* [](projects:corona-deaths) (project)
:::
::::

### Week 13 (Categories, Restructuring)

::::{grid}
:gutter: 4

:::{grid-item-card}
:columns: 6
**Prepare**
^^^
* [](managing-data:pandas), continued
  * [](managing-data:pandas:categorical-data)
  * [](managing-data:pandas:restructuring-data)
  * [](managing-data:pandas:performance-issues)
:::

:::{grid-item-card}
:columns: 6
**Discuss and apply**
^^^
* [](exercises:managing-data:advanced-pandas) (exercises)
* [](exercises:managing-data:pandas-vectorization) (exercises)
:::

:::{grid-item-card}
:columns: 12
**Practical exercise session**
^^^
* [](projects:weather)
  * [](projects:weather:climate-change) (project)
:::
::::
