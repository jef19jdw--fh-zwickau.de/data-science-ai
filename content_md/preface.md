---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.16.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Preface

This book covers a wide range of topics in data science and artificial intelligence. It's an attempt to provide self-contained learning material for first-year students in data science related courses. Most, not all, of the material is taught in the undergradute [course on data science](https://whz.de/data) at [Zwickau University of Applied Sciences](https://whz.de).

Starting teaching data science in 2019 [the author](https://whz.de/~jef19jdw) faced the problem that there seems to be no text book covering math, computer science, statistical data science, artificial intelligence and related topics in a well structured, accessible, thorough way. Basic [Python](https://python.org) programming should be covered as well as state of the art deep reinforcement learning for controlling autonomous robots. All this with hands-on experience for students, interesting real-world data sets, and sufficiently rich theoretical background.

Classical paper books or PDF ebooks do not suit the needs for this project. Working with data requires lots of source code, interactive visualizations, data listings, and easy to follow pointers to online resources. [Jupyter Book](https://jupyterbook.org) is an awesome software tool for publishing book-like interactive content. For the author writing this book was also a journey of discovery to the possible future of publishing. Having authored two paper books the author knows the tight limits of paper books and publishing companies. The greater his enthusiasm is for the freedom in writing and publishing provided by Jupyter Book and its community, [The Executable Books Project](https://executablebooks.org).

The book, at the moment of writing this preface, contains more than 20 exercise series and almost 60 complex data projects dealing with real-world data. The print version of the book has more than 1000 pages.

The author expresses its gratitude towards all the more or less anonymous people developing the wonderful open source tools used in this book and for writing the book. There are too many tools to list them here. The author also thanks his students and colleagues at Zwickau University, especially [Hendrik Weiß](https://whz.de/pti/organisation/fachgruppe-mathematik/personen/hendrik-weiss), who constantly find typos and make suggestions for improving the book.

[Jens Flemming](https://whz.de/~jef19jdw), Zwickau, August 2024
