---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(math:linalg:vectors)=
# Vectors

The term *vector* is used in several different contexts, each coming with a slightly different definition. In basic linear algebra a vector often is considered a finite column of numbers. That's the approach we follow here. 

## Definition

For $d\in\mathbb{N}$ a *$d$-tuple* is an ordered list of real numbers, typically written as $(x_1,x_2,\ldots,x_d)$ with $x_1,\ldots,x_d$ denoting the numbers. Here 'ordered' means that swapping two unequal numbers in the list yields a different $d$-tuple. Example: $(1,2,3)\neq(1,3,2)$. By $\mathbb{R}^d$ we denote the set of $d$-tuples.

*Vector* is another term for $d$-tuple. In linear algebra vectors may be interpreted as points in space or as difference between two points (that is, describing a translation). Vectors often are written as columns:
\begin{equation*}
x=\begin{bmatrix}x_1\\x_2\\\vdots\\x_d\end{bmatrix}.
\end{equation*}

## Length of a Vector

The (Euclidean) length of a vector $x$ is defined as
\begin{equation*}
|x|:=\sqrt{x_1^2+\cdots+x_d^2}.
\end{equation*}

## Sum of Vectors

Sums of vectors are defined componentwise:
\begin{equation*}
x+y:=\begin{bmatrix}x_1+y_1\\\vdots\\x_d+y_d\end{bmatrix}.
\end{equation*}

## Multiples of Vectors

Products of real numbers and vectors are defined componentwise. For $a\in\mathbb{R}$ and $x\in\mathbb{R}^d$ we have
\begin{equation*}
a\,x:=\begin{bmatrix}a\,x_1\\\vdots\\a\,x_d\end{bmatrix}.
\end{equation*}

## Inner Products

The inner product of two vectors $x,y\in\mathbb{R}^d$ is
\begin{equation*}
\langle x,y\rangle:=x_1\,y_1+\cdots+x_d\,y_d.
\end{equation*}
Inner products are closely related to angles between vectors.

## Outer Products

The outer product of two vectors $x,y\in\mathbb{R}^3$ is
\begin{equation*}
x\times y:=\begin{bmatrix}x_2\,y_3-x_3\,y_2\\x_3\,y_1-x_1\,y_3\\x_1\,y_2-x_2\,y_1\end{bmatrix}.
\end{equation*}
The outer product yields a vector orthogonal to both factors.
