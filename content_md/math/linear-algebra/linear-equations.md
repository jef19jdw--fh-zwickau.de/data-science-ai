---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(math:linalg:linear-equations)=
# Systems of Linear Equations

A system of linear equations with $m$ equations and $n$ unknowns $x_1,\ldots,x_n$ has the form
\begin{align*}
a_{1,1}\,x_1+\cdots+a_{1,n}\,x_n&=b_1\\
\vdots\\
a_{m,1}\,x_1+\cdots+a_{m,n}\,x_n&=b_m
\end{align*}
with coefficients $a_{ij}$ and right-hand sides $b_1,\ldots,b_m$.

Setting
\begin{equation*}
A:=\begin{pmatrix}a_{1,1}&\cdots&a_{1,n}\\\vdots&&\vdots\\a_{m,1}&\cdots&a_{m,n}\end{pmatrix},
\qquad x:=\begin{pmatrix}x_1\\\vdots\\x_n\end{pmatrix}
\qquad\text{and}\qquad b:=\begin{pmatrix}b_1\\\vdots\\b_m\end{pmatrix}
\end{equation*}
we may write a system of linear equations in its *matrix form*
\begin{equation*}
A\,x=b.
\end{equation*}

Depending on coeefficient matrix $A$ and right-hand side $b$ a system of linear equations either has no solution or exactly one solution or infinitely many solutions.
