---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(math:linalg:matrices)=
# Matrices

A *matrix* is a rectangular scheme of numbers. Matrices frequently appear in almost all fields of mathematics because they can be used to represent abstract concepts like linear mappings numerically. Many abstract operations in mathematics boil down to matrix computations as soon as concrete numerical examples are considered.

## Definition

For $m\in\mathbb{N}$ and $n\in\mathbb{N}$ a *matrix* is an $m$-tuple of $n$-tuples (see [](math:linalg:vectors) for definition of tuples). Example:
\begin{equation*}
\bigl((1, 4, 5, -2), (3, 2, -7, 10), (-2, 4, 5, 8)\bigr)\qquad\text{here $\;m=3\;$ and $\;n=4$.}
\end{equation*}

To simplify notation matrices are written as rectangular schemes:
\begin{equation*}
\begin{pmatrix}1&4&5&-2\\3&2&7&10\\-2&4&5&8\end{pmatrix}\qquad\text{or}\qquad
\begin{bmatrix}1&4&5&-2\\3&2&7&10\\-2&4&5&8\end{bmatrix}
\end{equation*}

The set of all realvalued matrices with $m$ rows and $n$ columns is denoted as $\mathbb{R}^{m\times n}$.

Matrices usually are denoted by uppercase letters and a matrix' elements by corresponding lowercase letters with double index. The first index denotes the row, the second the column of the element. Example:

\begin{equation*}
A=\begin{pmatrix}a_{11}&a_{12}&a_{13}&a_{14}\\a_{21}&a_{22}&a_{23}&a_{24}\\a_{31}&a_{32}&a_{33}&a_{34}\end{pmatrix}
=\bigl(a_{ij}\bigr)_{i=1,\ldots,3}^{j=1,\ldots,4}.
\end{equation*}

Row $i$ is denoted by $a_{i\bullet}$, column $j$ by $a_{\bullet,j}$. Rows and columns can be regarded as vectors.

## Special Matrices

A matrix with identical number of rows and columns ($m=n$) is called *square matrix*. The tuple $(a_{11},a_{22},\ldots,a_{mm})$ is called *main diagonal* of the matrix $A$.

A square matrix of the form
\begin{equation*}
\begin{pmatrix}
1&0&\cdots&0&0\\
0&1&\ddots&&0\\
\vdots&\ddots&\ddots&\ddots&\vdots\\
0&&\ddots&1&0\\
0&0&\cdots&0&1\\
\end{pmatrix}
\end{equation*}
is called *identity matrix*.

Matrices of the form
\begin{equation*}
\begin{pmatrix}
*&\cdots&\cdots&*\\
0&\ddots&&\vdots\\
\vdots&\ddots&\ddots&\vdots\\
0&\cdots&0&*\\
\end{pmatrix}
\qquad\text{and}\qquad
\begin{pmatrix}
*&0&\cdots&0\\
\vdots&\ddots&\ddots&\vdots\\
\vdots&&\ddots&0\\
*&\cdots&\cdots&*\\
\end{pmatrix}
\end{equation*}
are called *upper-triangular* and *lower-triangular*.

## Transpose

Given a matrix $A\in\mathbb{R}^{m\times n}$ its *transpose* is the matrix
\begin{equation*}
A^{\mathrm{T}}:=\bigl(a_{ji}\bigr)_{i=1,\ldots,m}^{j=1,\ldots,n}\in\mathbb{R}^{n\times m},
\end{equation*}
that is, the same matrix as $A$, but with rows and columns interchanged.

The double transpose is equivalent to the original matrix:
\begin{equation*}
\bigl(A^{\mathrm{T}}\bigr)^{\mathrm{T}}=A.
\end{equation*}

## Matrix Multiplication

The product of two matrices $A\in\mathbb{R}^{m\times n}$ and $B\in\mathbb{R}^{n\times p}$ is the matrix $C:=A\,B\in\mathbb{R}^{m\times p}$ with entries
\begin{equation*}
c_{ik}:=\sum_{j=1}^n a_{ij}\,b_{jk}.
\end{equation*}

## Inverse Matrix

A square matrix $A\in\mathbb{R}^{n\times n}$ is called *invertible* if there is a square matrix $B\in\mathbb{R}^{n\times n}$ such that
\begin{equation*}
A\,B=I\qquad\text{and}\qquad B\,A=I,
\end{equation*}
where $I$ is the identity matrix.
The matrix $B$ is called the *inverse* of $A$.

## Determinants

The determinant $\det A$ of a square matrix $A\in\mathbb{R}^{n\times n}$ is the real number computed from the following iterative rule:
\begin{equation*}
\det A:=\begin{cases}
a_{1,1},&\text{if $n=1$},\\
\displaystyle\sum_{j=1}^n(-1)^{1+j}\,a_{1,j}\,\det A_{1,j},&\text{if $n>1$},
\end{cases}
\end{equation*}
where $A_{1,j}$ is the submatrix of $A$ originating from removing row 1 and column $j$.

The determinant is non-zero if and only if the matrix is invertible. It is positive if the matrix columns constitute a right-handed coordinate system and negative if columns constitute a left-handed coordinate system.

For each fixed $i\in\{1,\ldots,n\}$ we get an equivalent iterative definition:
\begin{equation*}
\det A=\sum_{j=1}^n(-1)^{i+j}\,a_{i,j}\,\det A_{i,j}\qquad\text{for $n>1$},
\end{equation*}
where $A_{i,j}$ is $A$ without row $i$ and without column $j$.

One can show that
\begin{equation*}
\det A=\det A^{\mathrm{T}},
\end{equation*}
which implies
\begin{equation*}
\det A=\sum_{i=1}^n(-1)^{i+j}\,a_{i,j}\,\det A_{i,j}\qquad\text{for $n>1$}
\end{equation*}
for all $j\in\{1,\ldots,n\}$.
