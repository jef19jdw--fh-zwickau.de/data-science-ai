---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(math:combinatorics)=
# Combinatorics

Combinatorics is the mathematical field of counting. An application are discrete distributions in simple probability theory.

## Factorial

The factorial of a non-negative integer $n$ is the integer
\begin{equation*}
n!:=1\cdot 2\cdot\cdots\cdot(n-1)\cdot n,
\end{equation*}
where $0!:=1$.

Obviously, $n!=(n-1)!\,n$.
