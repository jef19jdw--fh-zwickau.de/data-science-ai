---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.8
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

(math:logic)=
# Logic

The field of logic consideres truth values of mathematical expressions.

## Logical Operators

Truth values can be transformed or combined by *logical operators*.

### Not

The $\mathrm{not}$ operator inverts the truth values of an expression.

| $a$ | $\mathrm{not}\,a$ |
|------|------|
| true | false |
| false | true |

### And

The $\mathrm{and}$ operator yields true if and only if both operands are true.

| $a$ | $b$ | $a\,\mathrm{and}\,b$ |
|------|------|------|
| true | true | true |
| true | false | false |
| false | true | false |
| false | false | false |

### Or (inclusive)

The $\mathrm{or}$ operator yields true if and only if at least one of both operands is true. This is sometimes called *inclusive or* because the and-case (both operands true) is included (that is, yields true).

| $a$ | $b$ | $a\,\mathrm{or}\,b$ |
|------|------|------|
| true | true | true |
| true | false | true |
| false | true | true |
| false | false | false |

### Or (exclusive)

The $\mathrm{xor}$ operator yields true if and only if exactly one of both operands is true. This called *exclusive or* because the and-case is excluded.

| $a$ | $b$ | $a\,\mathrm{xor}\,b$ |
|------|------|------|
| true | true | false |
| true | false | true |
| false | true | true |
| false | false | false |
