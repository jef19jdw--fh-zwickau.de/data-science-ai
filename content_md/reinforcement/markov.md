---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"tags": [], "user_expressions": []}

(reinforcement:markov)=
# Markov Decision Processes

Finite Markov decision processes are the standard model for reinforcement learning tasks. Although their simple structure almost all tasks fit into this framework. Having a rigorous mathematical model at hand, we are able to solve all relevant task in a unified manner.

```{tableofcontents}
```

Related exercises:
* [](exercises:reinforcement:markov)
