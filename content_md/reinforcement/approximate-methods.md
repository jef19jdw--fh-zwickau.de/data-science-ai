---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"tags": [], "user_expressions": []}

(reinforcement:approximate-methods)=
# Approximate Value Function Methods

Instead of using tables (tabular methods) to represent value functions, for task with large state and action spaces approximate representations of value functions based on parametrizations should be used, especially if tabular methods render infeasible regarding memory consumption and computation times.

```{tableofcontents}
```

Related projects:
* [](projects:cart-pole)
  * [](projects:cart-pole:environment)
  * [](projects:cart-pole:sarsa)
  * [](projects:cart-pole:deep-q-learning)
