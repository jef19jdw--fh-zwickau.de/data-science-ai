---
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.14.4
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"tags": [], "user_expressions": []}

(reinforcement:overview-examples)=
# Overview and Examples

Reinforcement learning is the third fundamental concept in machine learning next to supervised and unsupervised learning. Although the basic idea (learning by trail and error) is simple, most methods are quite involved and require a sound understanding of underlying theoretical concepts. In this chapter we provide a first overview of relevant notions and ideas. Details will be discussed in in subsequent chapters.

Most of the material in this part of the book is loosely based on or inspired by the book [Reinforcement Learning: An Introduction](https://webspace.fh-zwickau.de/jef19jdw/teaching/pti01840/sutton_barto.pdf) by Richard S. Sutton and Andrew G. Barto ([CC BY-NC-ND 2.0](https://creativecommons.org/licenses/by-nc-nd/2.0/)).

```{tableofcontents}
```

Related exercises:
* [](exercises:reinforcement:modeling)
