# Data Science and Artificial Intelligence for Undergraduates

This project is an executable book on data science and artificial intelligence built with [Jupyter Book](https://jupyterbook.org). It aims at undergraduates and provides material for a two-year data science course with emphasis on machine learning algorithms.

## Usage

The [official HTML rendering of the book](https://whz.de/~jef19jdw/data-science-ai) is hosted at the project maintainer's personal website.

This repository contains the source code from which the book is built. For general information on rendering and output formats see [Jupyter Book website](https://jupyterbook.org).

To generate HTML rendering from source run
```
jb build .
```
in the repository's root directory. For PDF rendering via LaTeX run
```
jb build . --builder pdflatex
```

See also [Hosting on a GitLab instance](#hosting-on-a-gitlab-instance) and [Non-Standard Configuration of Jupyter Book](#non-standard-configuration-of-jupyter-book) below.

## Support

Support is provided via GitLab issues for members of Saxon universities. People without suitable GitLab account may write an email to `jens.flemming@fh-zwickau.de` to get support.

## Contributing

At the moment the book is more or less a one-man project. Feel free to send bug reports, typos and so on to the author, see [Support](#support) above.

## License

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License (CC BY-SA 4.0)](http://creativecommons.org/licenses/by-sa/4.0/).

## Project Status

The project started in May 2022. A first complete version is available since March 2024. Material will be extended and revised continuously.

## Hosting on a GitLab Instance

Jupyter Book still (in August 2024, version 1.0.2) does not come with full support for Git repositories other than on [github.com](https://github.com). To use a self-hosted GitLab instance some tweaking may be required depending on which features you want to use. Two Python packages have to be modified:
* `sphinx-book-theme` (version 1.1.3)
* `sphinx-thebe` (version 0.3.1)

You find these packages in your Python environment's install path. Most often something like `your_python_env_path/lib/Python3.12/site-package/package_name`. To get your env's path check output of `conda env list` or `mamba env list` (if you are using conda/mamba envs).

### Binder Button

Launching in Binder does not work due to incorrect URLs (see [Jupyter Book issue 2025](https://github.com/jupyter-book/jupyter-book/issues/2025)). Open `sphinx-book-theme/src/sphinx_book_theme/header_buttons/launch.py` (see above where to find `sphinx_book_theme` directory) and replace
```python
url = f"{binderhub_url}/v2/git/{quote(repo_url)}/{branch}"
```
near line 125 by
```python
url = f"{binderhub_url}/v2/git/{quote(repo_url, safe="")}/{branch}"
```

### Thebe Button

Thebe button does not work because the `sphinx_thebe` package does not support repositories not hosted at [github.com](https://github.com). Open `sphinx_thebe/__init__.py` (see above where to find `sphinx_thebe` directory) and remove or comment line 132:
```python
    #org, repo = _split_repo_url(repo_url)
```
This suppresses the warning about the unsupported Git repository. Then replace
```python
            repo: "{org}/{repo}",
```
by
```python
            repo: "{repo_url}",
```
(near line 140) and insert the line
```python
            repoProvider: "git",
```
below line 140.

## Non-Standard Configuration of Jupyter Book

The [official HTML rendering of the book](https://whz.de/~jef19jdw/data-science-ai) shows some features not directly supported by Jupyter Book and deserving some explanation.

### Notebooks for Launching, MyST Markdown for Editing

Launch and edit buttons use the same Git repository. There is no config option to provide different repositories for launching with Binder/JupyterHub and editing in GitLab. Notebooks are more common for launching, but MyST markdown in combination with [Jupytext](https://github.com/mwouts/jupytext) is much more comfortable for editing than directly working with notebooks. Jupytext allows to keep notebooks in one directory and corresponding markdown files in another (with synchronization in both directions). Thus, it would be nice to launch from the notebook directory, but edit the markdown files.

To get this working we use the `edit_page_url_template` config option of the underlying [`pydata_sphinx_theme`](https://pydata-sphinx-theme.readthedocs.io/en/stable/) in combination with some code for the [Jinja template engine](https://jinja.palletsprojects.com):
```yaml
sphinx:
  config:
    html_context: {
        edit_page_url_template: 'https://gitlab.hrz.tu-chemnitz.de/jef19jdw--fh-zwickau.de/data-science-ai/-/edit/main/{{ file_name | replace(".ipynb", ".md") | replace("content/", "content_md/") }}',
    }
```
On the one hand, here we replace the name the directory `content` containing the `ipynb` files by `content_md` (contains the `md` files). On the other hand, we set the proper filename extension.

Then header of each markdown file should contain
```
jupytext:
  formats: content_md///md:myst,content///ipynb
  text_representation:
    extension: .md
    format_name: myst
```
to tell Jupytext to pair notebooks with markdown files in different directories.

### PDF Rendering
In standard configuration Jupyter Book won't render the PDF version of the book. For some reason `pdflatex` fails. Forcing Jupyter Book to use `xelatex` makes things work:
```yaml
sphinx:
  config:
    latex_engine: xelatex
```
